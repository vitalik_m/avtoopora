<?php
// HTTP
//define('HTTP_SERVER', 'http://avtoopora.scenario-projects.com/admin/');
//define('HTTP_CATALOG', 'http://avtoopora.scenario-projects.com/');

// HTTPS
//define('HTTPS_SERVER', 'http://avtoopora.scenario-projects.com/admin/');
//define('HTTPS_CATALOG', 'http://avtoopora.scenario-projects.com/');

$host = $_SERVER['HTTP_HOST'];
// HTTP
define('HTTP_SERVER', 'http://'.$host.'/admin/');
define('HTTP_CATALOG', 'http://'.$host.'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.$host.'/admin/');
define('HTTPS_CATALOG', 'https://'.$host.'/');
 
$dir = dirname(dirname(__FILE__));


// DIR
define('DIR_APPLICATION', $dir.'/html/admin/');
define('DIR_SYSTEM', $dir.'/html/system/');
define('DIR_LANGUAGE', $dir.'/html/admin/language/');
define('DIR_TEMPLATE', $dir.'/html/admin/view/template/');
define('DIR_CONFIG', $dir.'/html/system/config/');
define('DIR_IMAGE', $dir.'/html/image/');
define('DIR_CACHE', $dir.'/html/system/cache/');
define('DIR_DOWNLOAD', $dir.'/html/system/download/');
define('DIR_UPLOAD', $dir.'/html/system/upload/');
define('DIR_LOGS', $dir.'/html/system/logs/');
define('DIR_MODIFICATION', $dir.'/html/system/modification/');
define('DIR_CATALOG', $dir.'/html/catalog/');

// DIR
//define('DIR_APPLICATION', '/var/www/html/admin/');
//define('DIR_SYSTEM', '/var/www/html/system/');
//define('DIR_LANGUAGE', '/var/www/html/admin/language/');
//define('DIR_TEMPLATE', '/var/www/html/admin/view/template/');
//define('DIR_CONFIG', '/var/www/html/system/config/');
//define('DIR_IMAGE', '/var/www/html/image/');
//define('DIR_CACHE', '/var/www/html/system/cache/');
//define('DIR_DOWNLOAD', '/var/www/html/system/download/');
//define('DIR_UPLOAD', '/var/www/html/system/upload/');
//define('DIR_LOGS', '/var/www/html/system/logs/');
//define('DIR_MODIFICATION', '/var/www/html/system/modification/');
//define('DIR_CATALOG', '/var/www/html/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cmanager');
define('DB_PASSWORD', 'Pfr0dskbcnsq');
define('DB_DATABASE', 'avtoporadb');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
