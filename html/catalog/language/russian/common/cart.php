<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_items']     = '%s товаров';
$_['text_empty']     = 'Ваша корзина пуста!';
$_['text_cart']      = 'Корзина';
$_['text_checkout']  = 'Оформление заказа';
$_['text_recurring'] = 'Платежный профиль';