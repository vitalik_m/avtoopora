<!--<h3 class="module-title"><span><?php echo $heading_title; ?></span></h3>-->
<div class="row main_categories imgcategory">
    <?php foreach ($categories as $category) { ?>
    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
        <a href="<?php echo $category['href']; ?>" class="maincat">
            <div class="image col-md-6">
                <img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>"
                     alt="<?php echo $category['name']; ?>"/>
            </div>
            <div class="col-md-6">
                <h3>
                    <?php echo $category['name']; ?>
                </h3>
            </div>
        </a>
    </div>
    <?php } ?>
</div>