<div class="swiper-container top_slider">
    <div class="swiper-wrapper">
        <?php foreach ($slides as $slide) { ?>
        <div class="swiper-slide top_greenslide">
            <div class="col-md-6 topbanner-descr">
                <h2>
                    <?php if ($slide['use_html']) { ?>
                        <?php echo $slide['html']; ?>
                    <?php } else { ?>
                        <h1><?php echo $slide['header']; ?></h1>
                        <span><?php echo $slide['description']; ?></span>
                    <?php } ?>
                </h2>
                <a class="button_greenslide" href="<?php echo $slide['link']; ?>">перейти</a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="topslide-swiper-pagination"></div>
</div>