<?php if (isset($products)): ?>
<section class="special_order">
  <div class="container">
    <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
    <div class="block-wrapper">
      <div class="blockslider-next specialorder_blockslider-next"></div>
      <div class="blockslider-prev specialorder_blockslider-prev"></div>
        <div class="swiper-container block_slider specialorder_slider">
          <div class="swiper-wrapper blockslider_flex ">
            <?php foreach ($products as $product) { ?>
            <div class="swiper-slide blockslider-height proditem" data-id="<?=$product['product_id'];?>">
              <div class="col-md-12 blockslider_bordered">
                <div class="blockslider_imgwrapper">
                  <?php if($product['inWish'] == true) { ?>
                  <a class="item_infavorites" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');$(this).removeClass('item_infavorites').addClass('item_tofavorites');"></a>
                  <?php } else { ?>
                  <a class="item_tofavorites" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');$(this).removeClass('item_tofavorites').addClass('item_infavorites');"></a>
                  <?php } ?>
                  <div class="badgeswrapper">
                    <?php if (!empty($product['sticker'])): ?>
                    <span class="oneitem_action">Хит продаж</span>
                    <?php endif; ?>
                    <?php if ($product['special']) { ?>
                    <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                    <?php } ?>
                  </div>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                </div>
                <p class="part_manufacturer">Оригинал</p>
                <h4 class="part_header"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p class="item_partnumber">
                  <?php if (!empty($product['description'])): echo $product['description']; endif; ?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['ean'])): echo $product['ean']; endif;?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['jan'])): echo $product['jan']; endif; ?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['isbn'])): echo $product['isbn']; endif; ?>
                </p>
                <p class="item_partnumber">
                  <?php if (!empty($product['mpn'])): echo $product['mpn']; endif; ?>
                </p>
                <?php if ($product['price']) { ?>
                <p class="partprice">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="oldprice"><?php echo $product['price']; ?></span>
                  <span class="current_price"><?php echo $product['special']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                <div class="quantity">
                  <input type="number" min="<?=$product['minimum'];?>" max="<?=$product['quantity'];?>" step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
                </div>
                <button class="item_tocart pull-right" type="button"><?php echo $button_cart; ?></button>
              </div>
            </div>
            <?php } ?>
          </div>
          <div class="swiper-pagination specialorder_swiper-pagination"></div>
    </div>
  </div>
</section>
<?php endif; ?>