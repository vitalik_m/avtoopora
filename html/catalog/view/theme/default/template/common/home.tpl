<?php echo $header; ?>
<div class="container">
    <div class="row">
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="topsect_marg">
            <?php echo $column_left; ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
            </div>
        </div>
    </div>
    <?php /* ?>
    <!--<div class="row">
        <?php $class = 'col-sm-12'; ?>
        <?php if ( $column_right ) { $class = 'col-sm-9'; } ?>
        <div id="content_bottom" class="<?php echo $class; ?>">
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div> -->
    <?php */ ?>
    <?php echo $content_bottom; ?>
    <?php echo $column_right; ?>
</div>
<?php echo $footer; ?>