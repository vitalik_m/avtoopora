<div id="openmodal_callback" class="modalDialog">
    <div>
        <a href="#close" title="Закрыть" class="close_modal">
        </a>
        <h2 class="modal_header">Заказать обратный звонок</h2>
        <form action="">
            <label for="callback_name">Имя</label>
            <input id="callback_name" type="text" placeholder="Укажите ваше имя" required>
            <label for="callback_phone">Телефон</label>
            <input id="callback_phone" type="text" placeholder="Укажите номер вашего телефона" required>
            <input id="callback_submit" type="submit" value="Отправить" class="pull-right">
        </form>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <?php if ($logo) { ?>
                <a href="<?php echo $home; ?>">
                    <img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" class="img-responsive header_logo">
                </a>
                <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
                <p class="service_top_pay">Мы принимаем к оплате</p>
                <img src="image/catalog/demo_avtoopora/liqpay-logo.png" alt="liqpay" class="img-responsive pull-left">
                <img src="image/catalog/demo_avtoopora/visa-mc-logo.png" alt="visa-mc"
                     class="img-responsive pull-right">
            </div>
            <?php if ($informations): ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2><?php echo $name; ?></h2>
                <ul>
                    <?php foreach ($informations as $information): ?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo $contact; ?>"><?php echo $questions; ?></a></li>
                </ul>
            </div>
            <?php endif; ?>
            <?php if ($categories) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2>Каталог</h2>
                <ul>
                    <?php foreach ($categories as $category) : ?>
                    <li>
                        <a href="<?php echo $category['href']; ?>">
                            <?php echo $category['name']; ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo $contact; ?>"><?php echo $help_online; ?></a></li>
                    <li>
                        <a href="<?php echo $contact; ?>" class="btn_footer_questions">
                            <?php echo $questions; ?>
                        </a>
                    </li>
                </ul>
            </div>
            <? } ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2><?php echo $text_extra; ?></h2>
                <ul>
                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone2; ?></a></li>
                    <li><a href="tel:<?php echo $fax; ?>"><?php echo $fax; ?></a></li>
                </ul>
                <p class="work_graphic"><?php echo $config_open; ?></p>
                <p class="footer_adress"><?php echo $address; ?></p>
                <?php if ($geocode): ?>
                <?php $vowels = array('lat: ','lng: ');?>
                <a href="https://maps.google.com/maps?q=<?php echo urlencode(str_replace($vowels, '', $geocode)); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                   target="_blank" class="see_map">
                    <?php echo $button_maps; ?>
                </a>
                <?php endif; ?>
            </div>
            <?php /* ?>
            <div class="col-sm-3">
                <h5><?php echo $text_service; ?></h5>
                <ul class="list-unstyled">
                    <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                    <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                    <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5><?php echo $text_extra; ?></h5>
                <ul class="list-unstyled">
                    <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                    <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                    <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
                    <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                </ul>
            </div>

            <div class="col-sm-3">
                <h5><?php echo $text_account; ?></h5>
                <ul class="list-unstyled">
                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                    <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                </ul>
            </div>
            <?php ?>
        </div>
        <hr>
        <p><?php echo $powered; */ ?></p>
    </div>
    <div class="scripts">
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript" defer></script>
        <?php } ?>
    </div>
</footer>
</body></html>