<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<?php if ($robots) { ?>
<meta name="robots" content="<?php echo $robots; ?>" />
<?php } ?>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/new/custom.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/new/fonts.min.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/new/header.min.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/new/main.min.css" rel="stylesheet">
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript" defer></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
	<?php // echo $menu; ?>
    <nav class="navbar customnav_marg">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#top_nav">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="top_nav">
          <ul class="nav navbar-nav top_m">
            <li><a href="#">О компании</a></li>
            <li><a href="#">Гарантия и возврат</a></li>
            <li><a href="#">Оплата</a></li>
            <li><a href="#">Доставка</a></li>
            <li><a href="#">Вопросы и ответы</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>">
            <img src="<?php echo $logo; ?>" alt="logotype" class="img-responsive header_logo"></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="open_phones">
          <p class="shop_open">в будние дни с 08:00 – 19:00, в субботу и воскресенье с 09:00 – 16:00</p>
          <ul class="shop_phones">
            <li><a href="tel:<?=$telephone?>"><?=$telephone?></a></li>
            <li><a href="tel:<?=$telephone2?>"><?=$telephone2?></a></li>
            <li><a href="tel:<?=$fax?>"><?=$fax?></a></li>
          </ul>
        </div>
        <a href="#openmodal_callback" class="callme">перезвоните мне</a>
      </div>
      <div class="col-md-3">
        <?php if ($logged) { ?>
        <ul>
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
          <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
          <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
        </ul>
        <?php } else { ?>
        <a class="enter_link" href="<?php echo $login; ?>"><?php echo $text_login; ?></a>
        <a class="reg_link" href="<?php echo $register; ?>"> / <?php echo $text_register; ?></a>
        <?php } ?>
      </div>
      <!--<ul class="dropdown-menu dropdown-menu-right">
        <?php if ($logged) { ?>
        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
        <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
        <?php } else { ?>
        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
        <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
        <?php } ?>
      </ul>-->
      <div class="col-sm-3"></div>
    </div>
  </div>
  <?php if ($categories) { ?>
  <section class="filtermenu">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="dropdown">
            <a href="#" id="dLabel" class="cat_dropdown" role="button" data-toggle="dropdown" data-target="#">Каталог товаров</a>
            <ul class="dropdown-menu multi-level">
              <?php foreach ($categories as $category) : ?>
              <?php if ($category['children']) : ?>
              <li class="dropdown-submenu">
                <a href="<?php echo $category['href']; ?>" tabindex="-1"><?php echo $category['name']; ?></a>
                <ul class="dropdown-menu">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                  <?php foreach ($children as $child) : ?>
                  <li><a tabindex="-1" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php endforeach; ?>
                  <li><a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></li>
                <?php } ?>
                </ul>
              </li>
              <?php else : ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-6"> <?php echo $search; ?> </div>
        <div class="col-sm-3">
          <div id="cart" class="btn-group btn-block">
            <?php if ($true == true): ?>
            <a class="favorite_header in_wish" href="/index.php?route=account/wishlist">
              <span>
                <?php if ($text_wishlist != 0) : echo $text_wishlist; endif;?>
              </span>
              <?=$text_wish?>
            </a>
            <?php else: ?>
            <a class="favorite_header" href="/index.php?route=account/wishlist">
              <?=$text_wish?>
            </a>
            <?php endif; ?>
            <?php echo $cart; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php } ?>
</header>

