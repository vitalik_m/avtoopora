<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
        $('#button-confirm').on('click', function () {
            $.ajax({
                type: 'get',
                url: 'index.php?route=payment/cod/confirm',
                cache: false,
                beforeSend: function () {
                    $('#button-confirm').button('loading');
                },
                complete: function () {
                    $('#button-confirm').button('reset');
                },
                success: function () {
                    //$('#cart > ul').load('index.php?route=common/cart/info ul li');
                    //location = '<?php echo $continue; ?>';
                    $('#checkout_text').load('<?php echo $continue; ?>');
                }
            });
        });
//--></script>
