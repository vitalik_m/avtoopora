<h3 class="delivery_headblock"><?php echo $text_instruction; ?></h3>
<p class="textPay"><?php echo $text_description; ?></p>
<div class="well well-sm">
  <p class="textPay"><?php echo $bank; ?></p>
  <p class="textPay"><?php echo $text_payment; ?></p>
</div>
<div class="buttons">
  <div class="pull-right hidden">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
        $('#button-confirm').on('click', function () {
            $.ajax({
                type: 'get',
                url: 'index.php?route=payment/bank_transfer/confirm',
                cache: false,
                beforeSend: function () {
                    $('#button-confirm').button('loading');
                },
                complete: function () {
                    $('#button-confirm').button('reset');
                },
                success: function () {
                    //location = '<?php echo $continue; ?>';
                    $('#checkout_text').load('<?php echo $continue; ?>');
                }
            });
        });
//--></script>
