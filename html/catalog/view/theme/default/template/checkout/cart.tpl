<?php echo $header; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-sm-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php if ($attention) { ?>
<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="container">
    <h2 class="cart_topphead pull-left"><?php echo $heading_title; ?></h2>
    <a class="savecart pull-right" href="#">Сохранить корзину в избранном</a>
    <div class="row">
        <?php echo $column_left; ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" onsubmit="return false;">
                <?php foreach ($products as $product): ?>
                <div class="cart_item">
                    <a class="cartitem_close" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"></a>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="blockslider_imgwrapper">
                                <?php if ($product['thumb']): ?>
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="img img-responsive"/>
                                </a>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p class="manufacturer_cart">Castrol</p>
                            <h4 class="part_header"><a
                                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <div class="quantity cart">
                                <input min="<?php echo $product['minimum']; ?>"
                                       step="<?php echo $product['minimum']; ?>"
                                       max="<?php echo $product['quantity'] + 1; ?>"
                                       value="<?php echo $product['quantity']; ?>"
                                       name="quantity[<?php echo $product['cart_id']; ?>]" type="number"
                                       class="quantity_col"
                                data-cart-id="<?php echo $product['cart_id']; ?>">
                            </div>
                            <p class="cartitem_price"><?php echo $product['price']; ?></p>
                        </div>
                        <div class="col-md-2">
                            <p class="cartitem_totalprice"><?php echo $product['total']; ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </form>
            <div class="cart_allprice pull-right">
                <?php if (!isset($coupon_set)): ?>
                <?php foreach ($totals as $total): ?>
                <?php if ( $total['code'] == 'sub_total' ): ?>
                <p><?php echo $total['title']; ?>: <?php echo $total['text']; ?></p>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php if ($coupon || $voucher || $reward || $shipping): ?>
                <a id="cart_promocode" class="adft">Есть промо-код?</a>
                <span id="cart_entercode" class="hidden">
                        <?php echo $coupon; /* ?>
                    <?php echo $voucher; ?>
                    <?php echo $reward; ?>
                    <?php echo $shipping; */ ?>
                    </span>
                <?php endif;?>
                <div id="cart_salecount" class="hidden">
                    <?php foreach ($totals as $total) { ?>
                    <p><?php echo $total['title']; ?>: <?php echo $total['text']; ?></p>
                    <?php } ?>
                </div>
                <a href="<?php echo $checkout; ?>" class="btn cartordersubmit">
                    <?php echo $button_checkout; ?>
                </a>
                <?php elseif (!empty($coupon_set)): ?>
                <?php foreach ($totals as $total) { ?>
                <p><?php echo $total['title']; ?>: <?php echo $total['text']; ?></p>
                <?php } ?>
                <a href="<?php echo $checkout; ?>" class="btn cartordersubmit">
                    <?php echo $button_checkout; ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<div class="container">
<?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
