<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="error"></div>
    <div class="row">
        <div id="checkout_progressbar">
            <p id="checkout_delivery" class="step step_active">
                <span class="step_text">Доставка</span>
            </p>
            <hr class="checkout_line">
            <p id="checkout_payment" class="step">
                <span class="step_text">Оплата</span>
            </p>
            <hr class="checkout_line">
            <p id="checkout_processing" class="step">
                <span class="step_text">Заказ в обработке</span>
            </p>
        </div>
    </div>
</div>
<section id="delivery_section" class="checkout_form">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2 class="section_header"><?php echo $heading_title; ?></h2>
                <?php /* if (!isset($address)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <p class="well">
                            <?php echo $text_returning_customer; ?>
                            &nbsp;<a href="#" onclick="jQuery('.login-form').toggle();return false;">
                                <?php echo $text_i_am_returning_customer; ?>
                            </a>
                        </p>
                        <div class="login-form registerbox clearfix" style="display:none">
                            <div class="row">
                                <div class="col-md-12 message"></div>
                                <form class="form-inline" role="form">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label"
                                                   for="input-email"><?php echo $entry_email; ?></label>
                                            <input type="text" name="email" value=""
                                                   placeholder="<?php echo str_replace(':','',$entry_email); ?>"
                                                   id="input-email" class="form-control"/>
                                        </div>&nbsp;&nbsp;
                                        <div class="form-group">
                                            <label class="control-label"
                                                   for="input-password"><?php echo $entry_password; ?></label>
                                            <input type="password" name="password" value=""
                                                   placeholder="<?php echo str_replace(':','',$entry_password); ?>"
                                                   id="input-password" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="button" value="<?php echo $button_login; ?>" id="button-login"
                                               data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"
                                               class="btn btn-primary"/>
                                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } */ // Если зарегистрирован ?>
                <?php /* if ($addresses) { //Если адрес уже был введен (зарегистрированный пользователь) ?>
                <?php if (isset($customer_id)) { ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="payment_address" value="existing" checked="checked"
                               onclick="jQuery('#payment-address-new').hide()"/>
                        <?php echo $text_address_existing; ?></label>
                </div>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required" id="payment-existing">
                    <select name="payment_address_id" class="select_styled checkout_select form-control">
                        <?php foreach ($addresses as $address) { ?>
                        <?php if (isset($payment_address_id) && $address['address_id'] == $payment_address_id) { ?>
                        <option value="<?php echo $address['address_id']; ?>"
                                selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>
                            , <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
                            , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>
                            , <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
                            , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </fieldset>
                <?php } ?>
                <?php if (isset($customer_id)) { ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="payment_address" value="new"
                               onclick="jQuery('#payment-address-new').show();"/>
                        <?php echo $text_address_new; ?></label>
                </div>
                <?php } ?>
                <?php } */ ?>
                <div id="payment-address-new">
                    <input type="hidden" name="customer_group_id" value="1"/>
                    <input type="hidden" name="company" value=""/>
                    <input type="hidden" name="company_id" value=""/>
                    <input type="hidden" name="tax_id" value=""/>
                    <input type="hidden" name="address_2" value=""/>
                    <input type="hidden" name="postcode" value=""/>
                    <input type="hidden" name="comment" value=""/>
                    <input type="hidden" name="fax" value=""/>
                    <input type="hidden" name="shipping_firstname" value=""/>
                    <input type="hidden" name="shipping_lastname" value=""/>
                    <input type="hidden" name="shipping_company" value=""/>
                    <input type="hidden" name="shipping_address_1" value=""/>
                    <input type="hidden" name="shipping_address_2" value=""/>
                    <input type="hidden" name="shipping_city" value=""/>
                    <input type="hidden" name="shipping_postcode" value=""/>
                    <input type="hidden" name="shipping_country_id" value=""/>
                    <fieldset class="checkout_required required form-group">
                        <label class="control-label" for="input-payment-firstname">
                            <?php echo $entry_firstname; ?>
                        </label>
                        <input type="text" name="firstname"
                               value="<?php if (isset($address['firstname'])) echo $address['firstname']; elseif (isset($firstname)) echo $firstname; ?>"
                               placeholder="<?php echo str_replace(':','',$entry_firstname); ?>"
                               id="input-payment-firstname"
                               class="form-control" <?php if (isset($customer_id)) { ?> readonly <?php } ?>/>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <label class="control-label" for="input-payment-lastname">
                            <?php echo $entry_lastname; ?>
                        </label>
                        <input type="text" name="lastname" value="<?php if (isset($lastname)) echo $lastname;?>"
                               placeholder="<?php echo str_replace(':','',$entry_lastname); ?>"
                               id="input-payment-lastname"
                               class="form-control" <?php if (isset($customer_id)) { ?> readonly<?php }?>/>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <label class="control-label" for="input-payment-email"><?php echo $entry_email; ?></label>
                        <input type="text" name="email" value="<?php if (isset($email)) echo $email;?>"
                               placeholder="<?php echo str_replace(':','',$entry_email); ?>" id="input-payment-email"
                               class="form-control" <?php if (isset($customer_id)) { ?> readonly<?php }?>/>
                    </fieldset>

                    <h2 class="section_subheader"><?php echo $text_your_address; ?></h2>

                    <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                        <?php if ($error_warning) { ?>
                        <div class="alert alert-warning"><i
                                    class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                        <?php if ($shipping_methods) { ?>

                        <label class="control-label"
                               for="delivery_checkout"><?php echo $text_shipping_method; ?></label>
                        <select id="delivery_checkout" class="select_styled checkout_select shipping_method" name="shipping_method">
                            <option selected disabled>Выберите способ доставки</option>
                            <?php foreach ($shipping_methods as $shipping_method): ?>
                            <?php if (!$shipping_method['error']) { ?>
                            <?php foreach ($shipping_method['quote'] as $quote): ?>
                            <?php if ($quote['code'] == $code || !$code) { ?>
                            <?php $code = $quote['code']; ?>
                            <option value="<?php echo $quote['code']; ?>"
                                    title="<?php echo $quote['title']; ?>">
                                <?php echo $quote['title']; ?>
                            </option>
                            <?php } else { ?>
                            <option value="<?php echo $quote['code']; ?>"
                                    title="<?php echo $quote['title']; ?>">
                                <?php echo $quote['title']; ?>
                            </option>
                            <?php } ?>
                            <?php /* echo $quote['title']; ?> - <?php echo $quote['text']; */ // название - стоимость ?>
                            <?php endforeach; ?>
                            <?php } else { ?>
                            <div class="alert alert-danger">
                                <?php echo $shipping_method['error']; ?>
                            </div>
                            <?php } ?>
                            <?php endforeach; ?>
                        </select>
                        <?php } ?>
                        <input id="checkout-input" type="hidden">
                    </fieldset>

                    <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                        <label class="control-label" for="input-payment-country"><?php echo $entry_country; ?></label>
                        <select name="country_id" id="input-payment-country"
                                class="select_styled checkout_select form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                            <option value="<?php echo $country['country_id']; ?>"
                                    selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                        <label class="control-label" for="input-payment-zone"><?php echo $entry_zone;; ?></label>
                        <select name="zone_id" id="input-payment-zone"
                                class="select_styled checkout_select form-control">
                        </select>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <label class="control-label" for="input-payment-city"><?php echo $entry_city; ?></label>
                        <input type="text" name="city" value="<?php if (isset($city)) echo $city;?>"
                               placeholder="<?php echo str_replace(':','',$entry_city); ?>" id="input-payment-city"
                               class="form-control"/>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <label class="control-label"
                               for="input-payment-address-1"><?php echo $entry_address_1; ?></label>
                        <input type="text" name="address_1" value="<?php if (isset($address_1)) echo $address_1;?>"
                               placeholder="<?php echo str_replace(':','',$entry_address_1); ?>"
                               id="input-payment-address-1"
                               class="form-control"/>
                    </fieldset>

                    <fieldset class="checkout_required form-group required">
                        <label class="control-label"
                               for="input-payment-telephone"><?php echo $entry_telephone; ?></label>
                        <input type="text" name="telephone" value="<?php if (isset($telephone)) echo $telephone;?>"
                               placeholder="<?php echo str_replace(':','',$entry_telephone); ?>"
                               id="input-payment-telephone"
                               class="form-control" <?php if (isset($customer_id)) { ?> readonly<?php } ?>/>
                    </fieldset>
                </div>
                <?php if (!isset($customer_id)) { // Регистрировать при покупке или нет ?>
                <div class="form-group col-md-12">
                    <label>
                        <input type="checkbox" name="register"
                               onclick="jQuery('.register-form').toggle()">&nbsp;<?php echo $text_register; ?>
                    </label>
                </div>
                <?php } ?>
                <div class="register-form" style="display:none">
                    <div class="form-group required col-md-6">
                        <label class="control-label" for="input-payment-password"><?php echo $entry_password; ?></label>
                        <input type="password" name="password" value=""
                               placeholder="<?php echo str_replace(':','',$entry_password); ?>"
                               id="input-payment-password"
                               class="form-control"/>
                    </div>
                    <div class="form-group required col-md-6">
                        <label class="control-label" for="input-payment-confirm"><?php echo $entry_confirm; ?></label>
                        <input type="password" name="confirm" value=""
                               placeholder="<?php echo str_replace(':','',$entry_confirm); ?>"
                               id="input-payment-confirm"
                               class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <span>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <span class="divider pull-right"></span>
                    <p class="checkout_cartinfolink">
                            <span><a class="checkout_cartinfolink" href="<?= $cart ?>"><?= $edit_cart; ?></a></span>
                        <span>
                            <?php foreach ($totals as $total): ?>
                            <?php echo $total['title']; ?>: <?php echo $total['text']; ?><br>
                            <?php endforeach; ?>
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-md-6">
                    <input id="delivery_submit" class="pull-right btn_checkout" type="submit"
                           value="сохранить и перейти к оплате">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="payment_section" class="ishidden_checkout checkout_form">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <h2 class="section_header">Оформление заказа</h2>
                <h2 class="section_subheader"><?=$text_payment_method; ?></h2>
                <?php if ($error_warning) { ?>
                <div class="alert alert-warning">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                </div>
                <?php } ?>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                    <label class="control-label" for="payment_method">Способ оплаты</label>
                    <?php if ($payment_methods): ?>
                    <select id="payment_method" class="select_styled checkout_select" name="payment_method">
                        <option value="" selected="selected">
                            Выберите способ оплаты
                        </option>
                        <?php foreach ($payment_methods as $payment_method): ?>
                        <?php if ($payment_method['code'] == $code || !$code): ?>
                        <?php $code = $payment_method['code']; ?>
                        <option value="<?php echo $payment_method['code']; ?>" title="<?php echo $payment_method['title']; ?>">
                            <?php echo $payment_method['title']; ?>
                        </option>
                        <?php else: ?>
                        <option value="<?php echo $payment_method['code']; ?>" title="<?php echo $payment_method['title']; ?>">
                            <?php echo $payment_method['title']; ?>
                        </option>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                    <?php endif; ?>
                    <input type="hidden" id="payment-input" >
                </fieldset>
                <p class="paydescr_text">

                </p>
                <?php if ($text_agree) { ?>
                <div class="buttons clearfix">
                    <div class="pull-right"><?php echo $text_agree; ?>
                        <?php if ($agree) { ?>
                        <input type="checkbox" name="agree" value="1" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="agree" value="1" />
                        <?php } ?>
                        &nbsp;
                    </div>
                </div>
                <?php } else { ?>
                <div class="buttons">
                    <div class="pull-right">
                        <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary" />
                    </div>
                </div>
                <?php } ?>
                <input type="submit" class="pull-right btn_checkout"
                       data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"
                       id="button-register" value="<?php echo $heading_title;?>">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <span>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <span class="divider pull-right"></span>
                    <p class="checkout_cartinfolink">
                        <span><a class="checkout_cartinfolink" href="<?= $cart ?>"><?= $edit_cart; ?></a></span>
                        <span>
                            <?php foreach ($totals as $total): ?>
                            <?php echo $total['title']; ?>: <?php echo $total['text']; ?><br>
                            <?php endforeach; ?>
                        </span>
                    </p>
                </div>
                <div class="checkout_cartinfo">
                    <h3 class="delivery_headblock delivery"></h3><p class="delivery_block name"></p><p class="delivery_block city"></p><p class="delivery_block street"></p>
                    <p class="delivery_block checkout_cartinfolink"><a id="delivery_edit" href="#">Редактировать доставку</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="checkout_section" class="ishidden_checkout">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="checkout_text"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <span>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <p class="checkout_cartinfolink">
                         <span>
                            <?php foreach ($totals as $total): ?>
                             <?php echo $total['title']; ?>: <?php echo $total['text']; ?><br>
                             <?php endforeach; ?>
                        </span>
                    </p>
                </div>
                <div class="checkout_cartinfo"><h3 class="delivery_headblock delivery"></h3><p class="delivery_block name"></p><p class="delivery_block city"></p><p class="delivery_block street"></p></div>
                <div class="checkout_cartinfo pay"></div>
            </div>
        </div>
    </div>
</section>
<?php echo $column_right; ?>
<div class="container">
    <?php echo $content_bottom; ?>
</div>


<script type="text/javascript"><!--
    document.addEventListener("DOMContentLoaded", function (event) {
        var error = true;

// Login
        $(document).delegate('#button-login', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/checkout/login_validate',
                type: 'post',
                data: $('.login-form :input'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-login').button('loading');
                },
                complete: function () {
                    $('#button-login').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('.login-form .message').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

// Register
        $(document).delegate('#delivery_submit', 'click', function () {

            var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
            data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title');
            $.ajax({
                url: 'index.php?route=checkout/checkout/pre_validate',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                complete: function () {
                    $('#button-register').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        error = true;
                        if (json['error']['warning']) {
                            $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    } else {
                        error = false;
                        $( "#checkout_delivery" ).addClass("step_checked");
                        $( "#delivery_section" ) .toggleClass( "ishidden_checkout " );
                        $( "#payment_section" ) .toggleClass( "ishidden_checkout " );
                        $( "#checkout_payment" ).addClass("step_active");
                        jQuery('#payment_method :selected').click();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        $(document).delegate('#button-register', 'click', function () {

            var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
            data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title') + '&_payment_method=' + jQuery('#payment_method :selected').prop('title');

            $.ajax({
                url: 'index.php?route=checkout/checkout/validate',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                complete: function () {
                    $('#button-register').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        error = true;
                        if (json['error']['warning']) {
                            $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    } else {
                        error = false;
                        $( "#checkout_payment" ).addClass("step_checked");
                        $( "#payment_section" ) .toggleClass( "ishidden_checkout " );
                        $( "#checkout_section" ) .toggleClass( "ishidden_checkout " );
                        $( "#checkout_processing" ).addClass("step_active");
                        jQuery('#payment_method :selected').click();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $('select[name=\'country_id\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
                dataType: 'json',
                beforeSend: function () {
                    $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
                },
                complete: function () {
                    $('.fa-spinner').remove();
                },
                success: function (json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'zone_id\']').html(html).val("");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });


        $('select[name=\'shipping_country_id\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
                dataType: 'json',
                beforeSend: function () {
                    $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
                },
                complete: function () {
                    $('.fa-spinner').remove();
                },
                success: function (json) {
                    if (json['postcode_required'] == '1') {
                        $('input[name=\'postcode\']').parent().parent().addClass('required');
                    } else {
                        $('input[name=\'postcode\']').parent().parent().removeClass('required');
                    }

                    html = '<option value=""><?php echo $text_select; ?></option>';

                    if (json['zone'] && json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                            if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                                html += ' selected="selected"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                    }

                    $('select[name=\'shipping_zone_id\']').html(html).val("");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });


        $('select[name=\'country_id\'], select[name=\'zone_id\'], select[name=\'shipping_country_id\'], select[name=\'shipping_zone_id\'], input[type=\'radio\'][name=\'payment_address\'], input[type=\'radio\'][name=\'shipping_address\']').on('change', function () {
            if (this.name == 'contry_id') jQuery("select[name=\'zone_id\']").val("");
            if (this.name == 'shipping_country_id') jQuery("select[name=\'shipping_zone_id\']").val("");

            jQuery(".shipping-method").load('index.php?route=checkout/checkout/shipping_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function () {
                if (jQuery("input[name=\'shipping_method\']:first").length) {
                    jQuery("input[name=\'shipping_method\']:first").attr('checked', 'checked').prop('checked', true).click();
                } else {
                    jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
                }
            });

            jQuery(".payment_method").load('index.php?route=checkout/checkout/payment_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function () {
                jQuery("#payment_method").removeAttr("selected").prop('selected', false);
            });
        });


        $(document).delegate('#delivery_checkout', 'click', function () {
            jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
        });

        $('body').delegate('#payment_method', 'click', function () {

            var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
            data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title') + '&_payment_method=' + jQuery('#payment_method :selected').prop('title');

            if (!error)
                $.ajax({
                    url: 'index.php?route=checkout/checkout/confirm',
                    type: 'post',
                    data: data,
                    success: function (html) {
                        jQuery(".pay").html(html);

                        jQuery("#button-confirm").click();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
        });

        $('select[name=\'country_id\']').trigger('change');
        jQuery(document).ready(function () {
            jQuery('input:radio[name=\'payment_method\']:first').attr('checked', true).prop('checked', true);
        });
    });
    //--></script>

<?php echo $footer;?>
