<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
    </div>
</div>
<section id="register_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php include "catalog/view/theme/default/template/account/register_singup.tpl";?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2><?php echo $text_returning_customer; ?></h2>
                <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
                <form action="<?php // echo $action; ?>" method="post" enctype="multipart/form-data"
                      onsubmit="return false;">
                    <div class="form-group">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="text" name="email" value="<?php echo $email; ?>"
                               placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                        <input type="password" name="password" value="<?php echo $password; ?>"
                               placeholder="<?php echo $entry_password; ?>" id="input-password"
                               class="form-control"/>
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
                    <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary"/>
                    <?php if ($redirect) { ?>
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                    <?php } ?>
                </form>
            </div>
        </div>
        <div class="row"><?php echo $content_bottom; ?></div>
    </div>
</section>
<?php echo $footer; ?>