<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<section class="item_subcat">
    <div class="container">
        <div class="row">
            <?php echo $content_top; ?>
            <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
            <?php if ($categories) { /*?>
            <h3><?php echo $text_refine; ?></h3>
            <?php */ if (count($categories) <= 5) { ?>
            <div class="row">
                <div class="col-md-2">
                    <?php foreach ($categories as $category) { ?>
                    <div class="carsearch_bordered">
                        <a href="<?php echo $category['href']; ?>">
                            <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"
                                 class="center-block img-responsive">
                            <span class="carmark_descr"><?php echo $category['name']; ?></span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php } /*  else { ?>
            <div class="row">
                <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                <div class="col-sm-3">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <?php } */ ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php if ($products) { ?>
<section>
    <div id="content" class="container">
        <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
        <div class="row">
            <div class="col-md-5">
                <p class="search_ocitems pull-left">Найдено <?php echo $count; ?> товара в категории</p>
            </div>
            <!--<p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
            <div class="col-md-7 col-xs-6">
                <div class="group_custom">
                    <div class="btn-group btn-group-sm pull-right grid_switch">
                        <button type="button" id="grid-view" class="btn btn-default grid-active" data-toggle="tooltip"
                                title="<?php echo $button_grid; ?>" data-original-title="Сетка"></button>
                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip"
                                title="<?php echo $button_list; ?>" data-original-title="Список"></button>
                    </div>
                    <div class="select_arr select_pricesort">
                        <select id="input-sort" class="form-control select_styled" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>"
                                    selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row"><div class="col-md-1 text-right"><label class="control-label" for="input-limit"><?php echo $text_limit; ?></label></div><div class="col-md-2 text-right"><select id="input-limit" class="form-control" onchange="location = this.value;"><?php foreach ($limits as $limits) { ?><?php if ($limits['value'] == $limit) { ?><option value="<?php echo $limits['href']; ?>"selected="selected"><?php echo $limits['text']; ?></option><?php } else { ?><option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option><?php } ?><?php } ?></select></div></div><br/> -->
        <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"
                 data-id="<?php echo $product['product_id']; ?>">
                <div class="blockslider_bordered">
                    <?php if($product['inWish'] == true) { ?>
                    <a class="item_infavorites" type="button" data-toggle="tooltip"
                       title="<?php echo $button_wishlist; ?>"
                       onclick="wishlist.remove('<?php echo $product['product_id']; ?>');$(this).removeClass('item_infavorites').addClass('item_tofavorites');"></a>
                    <?php } else { ?>
                    <a class="item_tofavorites" type="button" data-toggle="tooltip"
                       title="<?php echo $button_wishlist; ?>"
                       onclick="wishlist.add('<?php echo $product['product_id']; ?>');$(this).removeClass('item_tofavorites').addClass('item_infavorites');"></a>
                    <?php } ?>
                    <div class="blockslider_imgwrapper">
                        <div class="blockgrid_badges">
                            <?php if (!empty($product['sticker'])): ?>
                            <span class="oneitem_action">Хит продаж</span>
                            <?php endif; ?>
                            <?php if ($product['special']) { ?>
                            <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                            <?php } ?>
                        </div>
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                 title="<?php echo $product['name']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="product_layout_listheader">
                        <p class="part_manufacturer"></p>
                        <h4 class="part_header">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        </h4>
                    </div>
                    <div class="product_layout_listblock">
                        <p class="item_partnumber">
                            <?php if (!empty($product['description'])): echo $product['description']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['ean'])): echo $product['ean']; endif;?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['jan'])): echo $product['jan']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['isbn'])): echo $product['isbn']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['mpn'])): echo $product['mpn']; endif; ?>
                        </p>
                    </div>
                    <?php /* if ($product['rating']) { ?>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($product['rating'] < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                    class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } */ ?>
                    <div class="product_layout_listtocart">
                        <?php if ($product['price']) { ?>
                        <?php if (!$product['special']) { ?>
                        <p class="partprice"><?php echo $product['price']; ?></p>
                        <?php } else { ?>
                        <p class="partprice partprice_notavailable">
                            <span class="oldprice"><?php echo $product['price']; ?></span>
                            <span class="current_price"><?php echo $product['special']; ?></span>
                        </p>
                        <?php } ?>
                        <?php /* if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                        <?php } */ ?>
                        <?php } ?>
                        <div class="quantity">
                            <input min="<?php echo $product['minimum']; ?>"
                                   max="<?=$product['quantity'] != 0 ? $product['quantity'] : $product['minimum']+1; ?>"
                                   step="<?php echo $product['minimum']; ?>"
                                   value="<?php echo $product['minimum']; ?>" type="number">
                        </div>
                        <button class="item_tocart pull-right" type="button">
                            <?php echo $button_cart; ?>
                        </button>
                    </div>
                </div>
                <?php /* if need added compare ?>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                        onclick="compare.add('<?php echo $product['product_id']; ?>');">
                    <i class="fa fa-exchange"></i>
                </button>
                <?php */ ?>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<div class="pagination_wrapper">
    <div class="text-center">
        <?php echo $pagination; ?>
        <!--<div class="col-sm-6 text-right"><?php echo $results; ?></div>-->
    </div>
</div>
</div>
<?php } ?>
<?php echo $column_right; ?>
<?php if (!$categories && !$products) { ?>
<p><?php echo $text_empty; ?></p>
<div class="buttons">
    <div class="pull-right">
        <a href="<?php echo $continue; ?>" class="btn btn-primary">
            <?php echo $button_continue; ?>
        </a>
    </div>
</div>
<?php } ?>
<?php if ($thumb && $description) { ?>
<section class="grids_textblock">
    <div class="container">
        <div class="row">
            <div class="col-md-9"><?php echo $description; ?></div>
            <div class="col-md-3">
                <img src="<?php echo $thumb_inner; ?>" alt="<?php echo $heading_title; ?>"
                     title="<?php echo $heading_title; ?>" class="gridsbanner_testclass"/>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<div class="container">
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
