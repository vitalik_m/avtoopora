<?php echo $header; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-sm-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>

<section>
  <div class="container">
    <?php echo $column_left; ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <?php echo $heading_title; ?>
      <?php if (isset($product_total)): ?>
      <div class="row">
        <div class="col-md-5">
          <p class="search_ocitems pull-left"><?=$product_total; ?></p>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <?php if ($products): ?>
        <?php foreach ($products as $product): ?>
        <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"
             data-id="<?php echo $product['product_id']; ?>">
          <div class="blockslider_bordered">
            <?php if($product['inWish'] == true) { ?>
            <a class="item_infavorites" type="button" data-toggle="tooltip"
               title="<?php echo $button_wishlist; ?>"
               onclick="wishlist.remove('<?php echo $product['product_id']; ?>');$(this).removeClass('item_infavorites').addClass('item_tofavorites');"></a>
            <?php } else { ?>
            <a class="item_tofavorites" type="button" data-toggle="tooltip"
               title="<?php echo $button_wishlist; ?>"
               onclick="wishlist.add('<?php echo $product['product_id']; ?>');$(this).removeClass('item_tofavorites').addClass('item_infavorites');"></a>
            <?php } ?>
            <div class="blockslider_imgwrapper">
              <div class="blockgrid_badges">
                <?php if (!empty($product['sticker'])): ?>
                <span class="oneitem_action">Хит продаж</span>
                <?php endif; ?>
                <?php if ($product['special']) { ?>
                <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                <?php } ?>
              </div>
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                     title="<?php echo $product['name']; ?>" class="img-responsive"/>
              </a>
            </div>
            <div class="product_layout_listheader">
              <p class="part_manufacturer"></p>
              <h4 class="part_header">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              </h4>
            </div>
            <div class="product_layout_listblock">
              <p class="item_partnumber">
                <?php if (!empty($product['description'])): echo $product['description']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['ean'])): echo $product['ean']; endif;?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['jan'])): echo $product['jan']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['isbn'])): echo $product['isbn']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['mpn'])): echo $product['mpn']; endif; ?>
              </p>
            </div>
            <?php /* if ($product['rating']) { ?>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                        class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            <?php } */ ?>
            <div class="product_layout_listtocart">
              <?php if ($product['price']) { ?>
              <?php if (!$product['special']) { ?>
              <p class="partprice"><?php echo $product['price']; ?></p>
              <?php } else { ?>
              <p class="partprice partprice_notavailable">
                <span class="oldprice"><?php echo $product['price']; ?></span>
                <span class="current_price"><?php echo $product['special']; ?></span>
              </p>
              <?php } ?>
              <?php /* if ($product['tax']) { ?>
              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } */ ?>
              <?php } ?>
              <div class="quantity">
                <input min="<?php echo $product['minimum']; ?>"
                       max="<?php echo $product['quantity']; ?>"
                       step="<?php echo $product['minimum']; ?>"
                       value="<?php echo $product['minimum']; ?>" type="number">
              </div>
              <button class="item_tocart pull-right" type="button">
                <?php echo $button_cart; ?>
              </button>
            </div>
          </div>
          <?php /* if need added compare ?>
          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                  onclick="compare.add('<?php echo $product['product_id']; ?>');">
            <i class="fa fa-exchange"></i>
          </button>
          <?php */ ?>
        </div>
        <?php endforeach; ?>
        <?php else: ?>
        <h4 class="notfound_result"><?php echo $text_empty; ?></h4>
        <?php endif; ?>
      </div>
    </div>
    <?php echo $column_right; ?>
  </div>
</section>
<?php if (isset($pagination)): ?>
<div class="pagination_wrapper">
  <div class="text-center">
    <?php echo $pagination; ?>
  </div>
</div>
<?php endif; ?>
<div class="container">
<?php echo $content_bottom; ?>
</div>
<?php /* ?>
<div class="container">
  <div class="row">
    <div id="content" class="<?php echo $class; ?>">
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-4">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-3">
          <label class="checkbox-inline">
            <?php if ($sub_category) { ?>
            <input type="checkbox" name="sub_category" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="sub_category" value="1" />
            <?php } ?>
            <?php echo $text_sub_category; ?></label>
        </div>
      </div>
      <p>
        <label class="checkbox-inline">
          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" id="description" />
          <?php } ?>
          <?php echo $entry_description; ?></label>
      </p>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
      <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>
      <div class="row">
        <div class="col-sm-3 hidden-xs">
          <div class="btn-group">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
        <div class="col-sm-1 col-sm-offset-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-sm-3 text-right">
          <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-sm-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div> <?php */ ?>
<script type="text/javascript"><!--
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#button-search').bind('click', function () {
            url = 'index.php?route=product/search';

            var search = $('#content input[name=\'search\']').prop('value');
            if (search) {
                url += '&search=' + encodeURIComponent(search) + '&sub_category=true' + '&description=true';
            }
            /*var category_id = $('#content select[name=\'category_id\']').prop('value');
            if (category_id > 0) {
                url += '&category_id=' + encodeURIComponent(category_id);
            } */
            /*var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');
            if (sub_category) {
                url += '&sub_category=true';
             }
            var filter_description = $('#content input[name=\'description\']:checked').prop('value');
            if (filter_description) {
                url += '&description=true';
             } */
            location = url;
        });
        $('input[name=\'search\']').bind('keydown', function (e) {
            if (e.keyCode == 13) {
                $('#button-search').trigger('click');
            }
        });
        /*
        $('select[name=\'category_id\']').on('change', function () {
            if (this.value == '0') {
                $('input[name=\'sub_category\']').prop('disabled', true);
            } else {
                $('input[name=\'sub_category\']').prop('disabled', false);
            }
        });

        $('select[name=\'category_id\']').trigger('change'); */
    });
--></script>
<?php echo $footer; ?>