//QUANTITY-SLIDER---------------------------------------------------------------------------

$(document).ready(function(){
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('.quantity input');

    (function( d ){
        var $inputs = d.querySelectorAll('.quantity');

        (function( inputs ){
            for (var i = 0; i < inputs.length; i++) {
                inputs[i] && inputs[i].addEventListener('click', function( e ){
                    e.preventDefault();
                    var target = e.target,
                        input = this.getElementsByTagName('INPUT')[0],
                        min = input.getAttribute('min'),
                        max = input.getAttribute('max');

                    if ( target.classList.contains('quantity-up') ) {
                        var oldValue = parseInt(input.value);
                        if (oldValue >= max) {
                            var newVal = oldValue;
                        } else {
                            var newVal = oldValue + 1;
                        }
                        input.value = newVal;
                        if ( this.classList.contains('cart') ) {
                            var cartId = input.getAttribute('data-cart-id');
                            cart.update(cartId, newVal);
                        }
                    }
                    if ( target.classList.contains('quantity-down') ) {
                        var oldValue = parseInt(input.value);
                        if (oldValue <= min) {
                            var newVal = oldValue;
                        } else {
                            var newVal = oldValue - 1;
                        }
                        input.value = newVal;
                        if ( this.classList.contains('cart') ) {
                            var cartId = input.getAttribute('data-cart-id');
                            cart.update(cartId, newVal);
                        }
                    }
                    return false;
                });
            }
        })($inputs);
    })(document)
});



(function (d) {
    var $elements = document.querySelectorAll('.product-layout, .proditem, #product, .relatedproducts');

    for (var i = 0; i < $elements.length; i++) {
        $elements[i] && $elements[i].addEventListener('click', function (e) {
            var target = e.target;

            if ( target.classList.contains('item_tocart') || target.classList.contains('oneitem_tocart') || target.classList.contains('relatedproducts_tocart') ) {
                e.preventDefault();
                var id = this.getAttribute('data-id'),
                    quantity = this.querySelector('.quantity input:first-child').value;

                $.ajax({
                    url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: {
                        quantity: quantity,
                        product_id: id
                    },
                    dataType: 'json',
                    success: function (json) {
                        $('.alert, .text-danger').remove();
                        $('.form-group').removeClass('has-error');

                        $('#cart > .cart_header').addClass('in_cart');

                        if (json['error']) {
                            if (json['error']['option']) {
                                for (i in json['error']['option']) {
                                    var element = $('#input-option' + i.replace('_', '-'));

                                    if (element.parent().hasClass('input-group')) {
                                        element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                    } else {
                                        element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                    }
                                }
                            }
                            if (json['error']['recurring']) {
                                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                            }

                            // Highlight any found errors
                            $('.text-danger').parent().addClass('has-error');
                        }
                        if (json['redirect']) {
                            location = json['redirect'];
                        }
                        if (json['success']) {
                            $('#content').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                            $('#cart > .cart_header').html('<span class="on_cart">' + json['total'] + '</span>' + ' ' + json['text_cart'] );
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
                return false;
            }
        });
    }

})(document);

//------------------------------------------------------------------------------------------


//--->/SELECT-TOP---------------------------------------------------------------------------------

$(document).ready(function () {

    var mediagridSliderCarSearch =  {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 4,
            spaceBetween: 30
        }
    };

    var mediagridSlider =  {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 2,
            spaceBetween: 30
        }
    };

//TOP-SLIDER-INIT
    var swiper = new Swiper('.top_slider', {
        pagination: '.topslide-swiper-pagination',
        paginationClickable: true
    });

    var swiper2 = new Swiper('.search_slider', {
        //pagination: '.swiper-pagination2',
        //  paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 6,
        breakpoints: mediagridSliderCarSearch

    });

    var blockSlider= new Swiper('.specialorder_slider', {
        pagination: '.specialorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.specialorder_blockslider-next',
        prevButton: '.specialorder_blockslider-prev',
        breakpoints: mediagridSlider
        // autoHeight: true
    });

    var viewedSlider = new Swiper('.viewed_slider', {
        pagination: '.viewed_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.viewed_blockslider-next',
        prevButton: '.viewed_blockslider-prev',
        breakpoints: mediagridSlider
        // autoHeight: true
    });


    var popularorderSlider = new Swiper('.popularorder_slider', {
        pagination: '.popularorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.popularorder_blockslider-next',
        prevButton: '.popularorder_blockslider-prev',
        breakpoints: mediagridSlider
    });


});



