$(document).ready(function () {

    // $(document).on('click', function (e) {
//     var target = $(e.target).closest(".btn-select");
//     if (!target.length) {
//         $(".btn-select").removeClass("active").find("ul").hide();
//     }
// });

   /* $( "#delivery_submit" ).click(function() {
        $( "#checkout_delivery" ).addClass("step_checked");
        $( "#delivery_section" ) .toggleClass( "ishidden_checkout " );
        $( "#payment_section" ) .toggleClass( "ishidden_checkout " );
        $( "#checkout_payment" ).addClass("step_active");
    });

    $( "#button-register" ).click(function() {
        $( "#checkout_payment" ).addClass("step_checked");
        $( "#payment_section" ) .toggleClass( "ishidden_checkout " );
        $( "#checkout_section" ) .toggleClass( "ishidden_checkout " );
        $( "#checkout_processing" ).addClass("step_active");
    });
    */

    $( "#delivery_edit" ).click(function() {
        $( "#delivery_section" ) .toggleClass( "ishidden_checkout " );
        $( "#payment_section" ) .toggleClass( "ishidden_checkout " );
        $( "#checkout_delivery" ).addClass("step_active");
        $( "#checkout_delivery" ).toggleClass("step_checked");
        $( "#checkout_payment" ) .toggleClass( " step_active " );
    });

    document.querySelector('#delivery_submit').addEventListener('click', function( e ){
        var userName = $('#input-payment-firstname').val(),
            userLastName = $('#input-payment-lastname').val(),
            deliveryName = $('#delivery_checkout :selected').html(),
            cityName = $('#input-payment-city :selected').html(),
            addressClient = $('#input-payment-address-1').val(),
            payMetod = $('#payment_method :selected').html(),
            countryValue = $('#input-payment-country :selected').val(),
            zoneValue = $('#input-payment-zone :selected').val()
            //payDesc = $('#').html();
        ;
        //console.log(deliveryName + ' ' + userLastName + ' ' + userName);

        $.ajax({
            success: function ( res ) {
                $('.name').html(userName + ' ' + userLastName);
                $('.city').html(cityName);
                $('.delivery').html(deliveryName);
                $('.street').html(addressClient);
                $('.pay').html(payMetod);
                //   $('.textPay').html(payDesc);
                $("input[name='shipping_firstname']").val(userName);
                $("input[name='shipping_lastname']").val(userLastName);
                $('input[name=\'shipping_address_1\']').val(addressClient);
                $('input[name=\'shipping_city\']').val(cityName);
                $('input[name=\'shipping_zone_id\']').val(zoneValue);
                $('input[name=\'shipping_country_id\']').val(countryValue);
            }
        });
    });

   // document.addEventListener("DOMContentLoaded", function (event) {
        $('#delivery_checkout').delegate('option','click', function () {
            var delivery_name = $('#delivery_checkout :selected').html();
            var delivery_value = $('#delivery_checkout :selected').val();
            $('#checkout-input').attr({'title': delivery_name, 'name':'shipping_method'});
            $('#checkout-input').val(delivery_value);
            console.log('Сменили метод доставки');
        });

        $('#payment_method').change(function () {
            var payment_name = $('#payment_method :selected').html();
            var payment_value = $('#payment_method :selected').val();
            $('#payment-input').attr('title', payment_name);
            $('#payment-input').val(payment_value);
            console.log('Сменили метод оплаты');
        });
  //  });
    $( document ).ready(function() {
        $( 'a[href="#"]' ).click( function(e) {
            e.preventDefault();
        } );
    });

});

