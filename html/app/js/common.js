$(document).ready(function(){
    // // Инициализирует карусель
    // $(".start-slide").click(function(){
    //     $("#myCarousel2").carousel('cycle');
    // });
    // // Останавливает процесс автоматической смены слайдов карусели
    // $(".pause-slide").click(function(){
    //     $("#myCarousel2").carousel('pause');
    // });
    // // Осуществляет переход на предыдущий слайд
    // $(".prev-slide").click(function(){
    //     $("#myCarousel2").carousel('prev');
    // });
    // // Осуществляет переход на следующий слайд
    // $(".next-slide").click(function(){
    //     $("#myCarousel2").carousel('next');
    // });
    // // Осуществляет переход на 0 слайд карусели
    // $(".slide-one").click(function(){
    //     $("#myCarousel2").carousel(0);
    // });
    // // Осуществляет переход на 1 слайд карусели
    // $(".slide-two").click(function(){
    //     $("#myCarousel2").carousel(1);
    // });
    // // Осуществляет переход на 2 слайд карусели
    // $(".slide-three").click(function(){
    //     $("#myCarousel2").carousel(2);

    // });


    // var max_col_height = 0; // максимальная высота, первоначально 0
    // $('.blockslider-height').each(function(){ // цикл "для каждой из колонок"
    //     if ($(this).height() > max_col_height) { // если высота колонки больше значения максимальной высоты,
    //         max_col_height = $(this).height(); // то она сама становится новой максимальной высотой
    //     }
    // });
    // $('.blockslider-height').height(max_col_height); // устанавливаем высоту каждой колонки равной значению максимальной высоты
});



// $(".carousel").swipe({
//
//     swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
//
//         if (direction == 'left') $(this).carousel('next');
//         if (direction == 'right') $(this).carousel('prev');
//
//     },
//     allowPageScroll:"vertical"
//
// });



//QUANTITY-SLIDER---------------------------------------------------------------------------

jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
    var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

});

//------------------------------------------------------------------------------------------



//SELECT-TOP---------------------------------------------------------------------------------

$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});


//--->/SELECT-TOP---------------------------------------------------------------------------------


$(document).ready(function () {



//TOP-SLIDER-INIT
    var swiper = new Swiper('.top_slider', {
        pagination: '.topslide-swiper-pagination',
        paginationClickable: true
    });

    var swiper2 = new Swiper('.search_slider', {
        //pagination: '.swiper-pagination2',
        //  paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 6

    });

    var blockSlider= new Swiper('.specialorder_slider', {
        pagination: '.specialorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.specialorder_blockslider-next',
        prevButton: '.specialorder_blockslider-prev'
        // autoHeight: true
    });

    var viewedSlider = new Swiper('.viewed_slider', {
        pagination: '.viewed_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.viewed_blockslider-next',
        prevButton: '.viewed_blockslider-prev'
        // autoHeight: true
    });


    var popularorderSlider = new Swiper('.popularorder_slider', {
        pagination: '.popularorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.popularorder_blockslider-next',
        prevButton: '.popularorder_blockslider-prev',
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            }
        }
    });


});



