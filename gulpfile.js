let gulp = require('gulp');
let browserify = require('gulp-browserify');
let concat = require('gulp-concat');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');
// let importCss = require('gulp-import-css');

var cssimport = require("gulp-cssimport");
var options = {};


gulp.task('default', function() {
    gulp.watch('catalog/view/javascript/**', gulp.series('scripts'));
    gulp.watch('catalog/view/javascript/**', gulp.series('modules_scripts'));

    gulp.watch('catalog/view/theme/default/stylesheet/**',  gulp.series('styles'));
});

gulp.task('scripts', function() {
    gulp.src([
        'catalog/view/javascript/jquery/jquery-2.1.1.min.js',
        'catalog/view/javascript/bootstrap/js/bootstrap.min.js',
        'catalog/view/javascript/common.js',
        ])
        .pipe(sourcemaps.init())
        // .pipe(browserify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('catalog/view/javascript/public'))
});

gulp.task('modules_scripts', function() {
    gulp.src([
            'catalog/view/javascript/new/swiper.min.js',
            'catalog/view/javascript/new/swiper.settings.js',
            'catalog/view/javascript/new/common.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('modules.js'))
        .pipe(gulp.dest('catalog/view/javascript/public'))
});

gulp.task('styles', function() {
    gulp.src([
        'catalog/view/javascript/font-awesome/css/font-awesome.min.css',
        'catalog/view/theme/default/stylesheet/new/fonts.min.css',
        'catalog/view/theme/default/stylesheet/new/main.min.css',
        'catalog/view/javascript/bootstrap/css/bootstrap.min.css',
        'catalog/view/javascript/bootstrap/css/bootstrap-theme.min.css',
        'catalog/view/theme/default/stylesheet/new/swiper.min.css'
    ])
        .pipe(sourcemaps.init())
        // .pipe(importCss())
        .pipe(cssimport(options))
        // .pipe(cleanCSS())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('catalog/view/theme/default/stylesheet/public'))
});