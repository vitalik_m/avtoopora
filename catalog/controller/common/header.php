<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonHeader extends Controller
{
    public function index()
    {

        $this->load->model('account/wishlist');
        $jek_wish = $this->model_account_wishlist->getAllWishlist();
        if (isset($jek_wish)) {
            foreach ($jek_wish as $in_wish) {
                if (!empty($in_wish)) {
                    $true = true;
                } else {
                    $true = false;
                }
            }
        }
        if (isset($true)) {
            $data['true'] = $true;
        } else {
            $data['true'] = false;
        }
        // Analytics
        $this->load->model('extension/extension');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');
        $this->document->addScript('catalog/view/javascript/public/main.js', 'header');

        $this->document->addStyle('catalog/view/theme/default/stylesheet/public/main.css');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['robots'] = $this->document->getRobots();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts('header');
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            //$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
            $image = $this->config->get('config_logo');
            if (strpos($image, '.svg') !== false) {
                $data['logo'] = $server . 'image/' . $image;
                //$data['logo'] = renderSVG($image);
            } else {
                $data['logo'] = $server . 'image/' . $image;
            }
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        $data['text_home'] = $this->language->get('text_home');

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }
        $data['text_wish'] = $this->language->get('text_wish');

        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/edit', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['questions'] = $this->language->get('questions');

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/edit', '', 'SSL');
        $data['register'] = $this->url->link('account/register', '', 'SSL');
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['telephone2'] = $this->config->get('config_telephone2');
        $data['fax'] = $this->config->get('config_fax');
        $data['config_open'] = mb_substr($this->config->get('config_open'), mb_strpos($this->config->get('config_open'), ' '));

        $status = true;

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

            foreach ($robots as $robot) {
                if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
                    $status = false;

                    break;
                }
            }
        }
        //top nav
        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }
        // Menu
        $this->load->model('design/menu');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $data['categories'] = array();

        if ($this->config->get('configmenu_menu')) {
            $menus = $this->model_design_menu->getMenus();
            $menu_child = $this->model_design_menu->getChildMenus();

            foreach ($menus as $id => $menu) {
                $children_data = array();

                foreach ($menu_child as $child_id => $child_menu) {
                    if (($menu['menu_id'] != $child_menu['menu_id']) or !is_numeric($child_id)) {
                        continue;
                    }

                    $child_name = '';

                    if (($menu['menu_type'] == 'category') and ($child_menu['menu_type'] == 'category')) {
                        $filter_data = array(
                            'filter_category_id' => $child_menu['link'],
                            'filter_sub_category' => true
                        );

                        $child_name = ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '');
                    }

                    $children_data[] = array(
                        'name' => $child_menu['name'] . $child_name,
                        'href' => $this->getMenuLink($menu, $child_menu)
                    );
                }

                $data['categories'][] = array(
                    'name' => $menu['name'],
                    'children' => $children_data,
                    'column' => $menu['columns'] ? $menu['columns'] : 1,
                    'href' => $this->getMenuLink($menu)
                );
            }

        } else {

            $categories = $this->model_catalog_category->getCategories(0);
            $categories_share = array();
            foreach ($categories as $category) {
                if ($category['top']) {
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);
                    $share = array();
                    $share_child = array();
                    foreach ($children as $key => $child) {
                        $filter_data = array(
                            'filter_category_id' => $child['category_id'],
                            'filter_sub_category' => true
                        );
                        if (isset ($children) && !empty($children)) {
							$em = $this->model_catalog_category->getTree($child['category_id']);
                            if ($this->model_catalog_category->getTree($child['category_id']) && !empty($em)) {
                                $share_child = $this->model_catalog_category->getTree($child['category_id']);
                                if (isset($share_child)) {
                                    array_push($categories_share, $share_child);
                                }
                            }
                        }
                        $children_data[] = array(
                            'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );


                        // Level 3
                        $children_1 = $this->model_catalog_category->getCategories($child['category_id']);
                        $share_child_1 = array();
                        foreach ($children_1 as $key_1 => $child_1) {
                            $filter_data_1 = array(
                                'filter_category_id' => $child_1['category_id'],
                                'filter_sub_category' => true
                            );

                            if (isset($children_1) && !empty($children_1)) {
								$em = $this->model_catalog_category->getTree($child_1['category_id']);
                                if ($this->model_catalog_category->getTree($child_1['category_id']) && !empty($em)) {
                                    $share_child_1 = $this->model_catalog_category->getTree($child_1['category_id']);
                                    if (isset($share_child_1)) {
                                        array_push($categories_share, $share_child_1);
                                    }
                                }
                            }

                            $children_data[$key]['children_1'][] = array(
                                'name' => $child_1['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data_1) . ')' : ''),
                                'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_1['category_id'])
                            );
                        }
                    }
                    /*
                    if ($children_1 && !empty($children_1)) {
                        for ($i = 0; $i < count($children_1); $i++) {
                            if ($this->model_catalog_category->getTree($children_1[$i]['category_id']) && !empty($this->model_catalog_category->getTree($children_1[$i]['category_id']))) {
                                echo($children_1[$i]['category_id']);
                                print('<pre>');
                                print_r($this->model_catalog_category->getTree($children_1[$i]['category_id']));
                                print('</pre>');
                            }
                        }
                    }
                    if (isset ($children) && !empty($children)) {
                        for ($i = 0; $i < count($children); $i++) {
                            if ($this->model_catalog_category->getTree($children[$i]['category_id']) && !empty($this->model_catalog_category->getTree($children[$i]['category_id']))) {
                                echo($children[$i]['category_id']);
                                print('<pre>');
                                print_r($this->model_catalog_category->getTree($children[$i]['category_id']));
                                print('</pre>');
                            }
                        }
                    }
                    */
                    if (isset ($categories) && !empty($categories)) {
						$em = $this->model_catalog_category->getTree($category['category_id']);
                        if ($this->model_catalog_category->getTree($category['category_id']) && !empty($em)) {
                            $share = $this->model_catalog_category->getTree($category['category_id']);
                            if (isset($share)) {
                                array_push($categories_share, $share);
                            }
                        }
                    }

                    // Level 1
                    $data['categories'][] = array(
                        'name' => $category['name'],
                        'children' => $children_data,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }
        }
/*
        print('<pre>');
        print_r ($categories_share);
        print('</pre>');*/
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        foreach ($categories_share as $products) {
            foreach ($products as $product){
                $product_info = $this->model_catalog_product->getProduct($product['product_id']);
                $results = $this->model_catalog_product->getBestSellerProducts(5);
                if ($product_info) {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                    }
                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }
                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }
                    $flag = false;
                    foreach ($results as $result) {
                        if ($result['product_id'] == $product_info['product_id']) {
                            $flag = $result['product_id'];
                        }
                    }
                    $this->load->model('account/wishlist');
                    $jek_wish = $this->model_account_wishlist->getAllWishlist();
                    $true = false;
                    if (isset($jek_wish)) {
                        foreach ($jek_wish as $in_wish) {
                            if ($product_info['product_id'] == $in_wish) {
                                $true = true;
                            }
                        }
                    }
                    $data['shared'][] = array(
                        'category_id' => $category['category_id'],
                        'product_id' => $product_info['product_id'],
                        'manufacturer' => $product_info['manufacturer'],
                        'inWish' => $true,
                        'sticker' => $flag,
                        'saving' => $product_info['price'] == 0 ? 100 : round((($product_info['price'] - $product_info['special']) / $product_info['price']) * 100, 2),
                        'thumb' => $image,
                        'name' => $product_info['name'],
                        'model' => $product_info['model'],
                        'sku' => $product_info['sku'],
                        'upc' => $product_info['upc'],
                        'price' => $price,
                        'special' => $special,
                        //'rating'      => $rating,
                        'minimum' => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                        'quantity' => $product_info['quantity'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                    );
                }
            }
        }
        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        if ($this->config->get('configblog_blog_menu')) {
            $data['menu'] = $this->load->controller('blog/menu');
        } else {
            $data['menu'] = '';
        }
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');

// For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
        } else {
            return $this->load->view('default/template/common/header.tpl', $data);
        }
    }

    public
    function getMenuLink($parent, $child = null)
    {
        if ($this->config->get('configmenu_menu')) {
            $item = empty($child) ? $parent : $child;

            switch ($item['menu_type']) {
                case 'category':
                    $route = 'product/category';

                    if (!empty($child)) {
                        $args = 'path=' . $parent['link'] . '_' . $item['link'];
                    } else {
                        $args = 'path=' . $item['link'];
                    }
                    break;
                case 'product':
                    $route = 'product/product';
                    $args = 'product_id=' . $item['link'];
                    break;
                case 'manufacturer':
                    $route = 'product/manufacturer/info';
                    $args = 'manufacturer_id=' . $item['link'];
                    break;
                case 'information':
                    $route = 'information/information';
                    $args = 'information_id=' . $item['link'];
                    break;
                default:
                    $tmp = explode('&', str_replace('index.php?route=', '', $item['link']));

                    if (!empty($tmp)) {
                        $route = $tmp[0];
                        unset($tmp[0]);
                        $args = (!empty($tmp)) ? implode('&', $tmp) : '';
                    } else {
                        $route = $item['link'];
                        $args = '';
                    }

                    break;
            }

            $check = stripos($item['link'], 'http');
            $checkbase = strpos($item['link'], '/');
            if ($check === 0 || $checkbase === 0) {
                $link = $item['link'];
            } else {
                $link = $this->url->link($route, $args);
            }

            return $link;
        }
    }
}
