<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerAccountCars extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cars', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
        $this->document->addScript('catalog/view/javascript/bootstrap/js/bootstrap-select.min.js','footer');
        $this->document->addStyle('catalog/view/javascript/bootstrap/css/bootstrap-select.min.css');
		$this->load->language('account/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/cars');

		$this->getList();
	}

	public function add() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cars', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js','footer');
		$this->document->addScript('catalog/view/javascript/bootstrap/js/bootstrap-select.min.js','footer');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js','footer');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
		$this->document->addStyle('catalog/view/javascript/bootstrap/css/bootstrap-select.min.css');

		$this->load->model('account/cars');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_cars->addCars($this->request->post);

			$this->session->data['success'] = $this->language->get('text_add');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('cars_add', $activity_data);

			$this->response->redirect($this->url->link('account/cars', '', 'SSL'));
		}

		$this->getForm();
	}

	public function edit() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cars', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js','footer');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js','footer');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/cars');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_cars->editCars($this->request->get['cars_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_edit');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('cars_edit', $activity_data);

			$this->response->redirect($this->url->link('account/cars', '', 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/cars', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/cars');

		if (isset($this->request->get['cars_id']) && $this->validateDelete()) {
			$this->model_account_cars->deleteCars($this->request->get['cars_id']);

			$this->session->data['success'] = $this->language->get('text_delete');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('cars_delete', $activity_data);

			$this->response->redirect($this->url->link('account/cars', '', 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/edit', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			//'href' => $this->url->link('account/cars', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_cars_book'] = $this->language->get('text_cars_book');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_new_cars'] = $this->language->get('button_new_cars');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_back'] = $this->language->get('button_back');
		$data['cars_all'] = $this->language->get('cars_all');
        $data['geocode'] = $this->config->get('config_geocode');
        $data['geocode_hl'] = $this->config->get('config_language');
        $data['button_map'] = $this->language->get('button_map');
        $data['button_maps'] = $this->language->get('button_maps');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['carses'] = array();

		$results = $this->model_account_cars->getCarses();

		foreach ($results as $result) {
			if (isset($result['cars_format'])) {
				$format = $result['cars_format'];
			} else {
				$format = '{brand} {model}' . "\n" . '{year}' . "\n" . '{engine}' . "\n" . '{vin_code}';
			}

			$find = array(
				'{brand}',
				'{model}',
				'{year}',
				'{engine}',
				'{vin_code}'
			);

			$replace = array(
				'brand' => $result['brand'],
				'model'  => $result['model'],
				'year'   => $result['year'],
				'engine' => $result['engine'],
				'vin_code' => $result['vin_code']
			);

			$data['carses'][] = array(
				'cars_id' => $result['cars_id'],
				'cars'    => str_replace(array("\r\n", "\r", "\n "), '<br />', preg_replace(array("/ \s \s+/", "/ \r \r+/", "/\n \n+/"), '<br />', trim(str_replace($find, $replace, $format)))),
				'update'     => $this->url->link('account/cars/edit', 'cars_id=' . $result['cars_id'], 'SSL'),
				'delete'     => $this->url->link('account/cars/delete', 'cars_id=' . $result['cars_id'], 'SSL')
			);
		}

		$data['add'] = $this->url->link('account/cars/add', '', 'SSL');
		$data['back'] = $this->url->link('account/edit', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/cars_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/cars_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/cars_list.tpl', $data));
		}
	}

	protected function getForm() {
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/edit', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/cars', '', 'SSL')
		);

		if (!isset($this->request->get['cars_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_edit_cars'),
				//'href' => $this->url->link('account/cars/add', '', 'SSL')
			);
            $data['text_edit_cars'] = $this->language->get('text_added_cars');
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_cars_book'),
				//'href' => $this->url->link('account/cars/edit', 'cars_id=' . $this->request->get['cars_id'], 'SSL')
			);
            $data['text_edit_cars'] = $this->language->get('text_edit_cars');
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_brand'] = $this->language->get('entry_brand');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_year'] = $this->language->get('entry_year');
		$data['entry_engine'] = $this->language->get('entry_engine');
		$data['entry_vin_code'] = $this->language->get('entry_vin_code');
		$data['entry_default'] = $this->language->get('entry_default');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['brand'])) {
			$data['error_brand'] = $this->error['brand'];
		} else {
			$data['error_brand'] = '';
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}

		if (isset($this->error['year'])) {
			$data['error_year'] = $this->error['year'];
		} else {
			$data['error_year'] = '';
		}

		if (isset($this->error['engine'])) {
			$data['error_engine'] = $this->error['engine'];
		} else {
			$data['error_engine'] = '';
		}

		if (isset($this->error['vin_code'])) {
			$data['error_vin_code'] = $this->error['vin_code'];
		} else {
			$data['error_vin_code'] = '';
		}
		
		if (!isset($this->request->get['cars_id'])) {
			$data['action'] = $this->url->link('account/cars/add', '', 'SSL');
		} else {
			$data['action'] = $this->url->link('account/cars/edit', 'cars_id=' . $this->request->get['cars_id'], 'SSL');
		}

		if (isset($this->request->get['cars_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$cars_info = $this->model_account_cars->getCars($this->request->get['cars_id']);
		}

		if (isset($this->request->post['brand'])) {
			$data['brand'] = $this->request->post['brand'];
		} elseif (!empty($cars_info)) {
			$data['brand'] = $cars_info['brand'];
		} else {
			$data['brand'] = '';
		}

		if (isset($this->request->post['model'])) {
			$data['model'] = $this->request->post['model'];
		} elseif (!empty($cars_info)) {
			$data['model'] = $cars_info['model'];
		} else {
			$data['model'] = '';
		}

		if (isset($this->request->post['year'])) {
			$data['year'] = $this->request->post['year'];
		} elseif (!empty($cars_info)) {
			$data['year'] = $cars_info['year'];
		} else {
			$data['year'] = '';
		}

		if (isset($this->request->post['engine'])) {
			$data['engine'] = $this->request->post['engine'];
		} elseif (!empty($cars_info)) {
			$data['engine'] = $cars_info['engine'];
		} else {
			$data['engine'] = '';
		}

		if (isset($this->request->post['vin_code'])) {
			$data['vin_code'] = $this->request->post['vin_code'];
		} elseif (!empty($cars_info)) {
			$data['vin_code'] = $cars_info['vin_code'];
		} else {
			$data['vin_code'] = '';
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		$data['back'] = $this->url->link('account/cars', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/cars_form.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/cars_form.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/cars_form.tpl', $data));
		}
	}

	protected function validateForm() {
		if ((utf8_strlen(trim($this->request->post['brand'])) < 1) || (utf8_strlen(trim($this->request->post['brand'])) > 48)) {
			$this->error['brand'] = $this->language->get('error_brand');
		}

		if ((utf8_strlen(trim($this->request->post['model'])) < 1) || (utf8_strlen(trim($this->request->post['model'])) > 48)) {
			$this->error['model'] = $this->language->get('error_model');
		}

		if (utf8_strlen(trim($this->request->post['year'])) != 4 || !is_numeric($this->request->post['year'])) {
			$this->error['year'] = $this->language->get('error_year');
		}

		if ((utf8_strlen(trim($this->request->post['engine'])) < 1) || (utf8_strlen(trim($this->request->post['engine'])) > 10)) {
			$this->error['engine'] = $this->language->get('error_engine');
		}

        if ((utf8_strlen(trim($this->request->post['vin_code'])) != 17)) {
            $this->error['vin_code'] = $this->language->get('error_vin_code');
        }

		return !$this->error;
	}

	protected function validateDelete() {
		if ($this->model_account_cars->getTotalCarses() == 1) {
			$this->error['warning'] = $this->language->get('error_delete');
		}
		return !$this->error;
	}
}
