<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerAccountPassword extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/password');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);

			//$this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('password', $activity_data);
            if (!$this->error){
                $json['success'] = $this->language->get('text_success');
            }
			$json['redirect'] = ($this->url->link('account/edit', '', 'SSL'));
            //$this->response->redirect($this->url->link('account/edit', '', 'SSL'));

		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/edit', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			//'href' => $this->url->link('account/password', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_password'] = $this->language->get('text_password');

		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['password'])) {
            $json['error']['password'] = $this->error['password'];
		} else {
            $json['error']['password'] = '';
		}

		if (isset($this->error['old_password'])) {
            $json['error']['old_password'] = $this->error['old_password'];
		} else {
            $json['error']['old_password'] = '';
		}

		$data['action'] = $this->url->link('account/edit', '', 'SSL');

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['old_password'])) {
			$data['old_password'] = $this->request->post['old_password'];
		} else {
			$data['old_password'] = '';
		}

		$data['back'] = $this->url->link('account/edit', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/password.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/password.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/password.tpl', $data));
		}
		if(isset($json['error']) && !empty($json['error'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } elseif ($json['error']['password'] == '' && $json['error']['old_password'] == '' ) {
            //$this->session->data['success'] = $this->language->get('text_success');
            $json['success'] = $this->language->get('text_success');
            //$this->response->redirect($this->url->link('account/edit', '', 'SSL'));
            $json['redirect'] = ($this->url->link('account/edit', '', 'SSL'));
        }
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
            $this->error['password'] = $this->language->get('error_password');
		}

        if (empty(utf8_strlen($this->request->post['old_password']))) {
            $this->error['old_password'] = $this->language->get('enter_old_password');
        }

        if (empty(utf8_strlen($this->request->post['password']))) {
            $this->error['password'] = $this->language->get('enter_password');
        }

        if(!$this->customer->login($this->request->post['email'], $this->request->post['old_password'],false, true)){
            $this->error['old_password'] = $this->language->get('error_old_password');
        }

		return !$this->error;
	}
}