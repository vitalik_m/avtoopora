<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerModulePopular extends Controller {
	public function index($setting) {
		$this->load->language('module/popular');
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_tax'] = $this->language->get('text_tax');
		
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
        $data['button_cart_zero'] = $this->language->get('button_cart_zero');
        $this->document->addScript('catalog/view/javascript/public/modules.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/swiper.min.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/swiper.settings.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/common.js','footer');
//        $this->document->addStyle('catalog/view/theme/default/stylesheet/new/swiper.min.css');

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
		
		$data['products'] = array();		

		$product_data = array();
		
		if(isset($setting['limit']) && $setting['limit']!=''){
		   $setting['limit'] = $setting['limit'];
		}
		else{
  		   $setting['limit'] = 4;
		}
		
		
		$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.viewed DESC LIMIT " . (int)$setting['limit']);
		
		
		
		foreach ($query->rows as $result) { 		
			$product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
		}
					 	 		
		$results = $product_data;

		if ($results) {
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
			}
			
			
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}	
			
			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
			} else {
				$tax = false;
			}
			
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
            $bests = $this->model_catalog_product->getBestSellerProducts(5);
            $flag = false;
            foreach ($bests as $best) {
                if($result['product_id'] == $best['product_id']){
                    $flag = $result['product_id'];
                }
            }

            $this->load->model('account/wishlist');
            $jek_wish = $this->model_account_wishlist->getAllWishlist();
			$true = false;
			if (isset($jek_wish)) {
                foreach ($jek_wish as $in_wish) {
                    if ($result['product_id'] == $in_wish) {
                        $true = true;
                    }
                }
            }

			$data['products'][] = array(
				'product_id'   => $result['product_id'],
                'manufacturer' => $result['manufacturer'],
				'thumb'   	   => $image,
				'inWish'       => $true,
                'sticker'      => $flag,
                'saving'       => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 2),
				'name'         => $result['name'],
				'description'  => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'model'        => $result['model'],
                'sku'          => $result['sku'],
//                'upc'          => $result['upc'],
                'ean'          => $result['ean'],
                'jan'          => $result['jan'],
                'isbn'         => $result['isbn'],
                'mpn'          => $result['mpn'],
				'quantity'     => $result['quantity'],
				'minimum'      => $result['minimum'],
				'price'   	   => $price,
				'special' 	   => $special,
				'tax'          => $tax,
				'rating'       => $rating,
				'href'         => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/popular.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/popular.tpl', $data);
			} else {
			return $this->load->view('default/template/module/popular.tpl', $data);
		   }
	    }
	}
}