<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerModuleMAnufacturer extends Controller
{
    public function index($setting)
    {
        $this->load->language('module/manufacturer');

        $data['heading_title'] = $this->language->get('heading_title');

        $this->document->addScript('catalog/view/javascript/public/modules.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/swiper.min.js', 'footer');
        //$this->document->addScript('catalog/view/javascript/new/swiper.settings.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/common.js','footer');

        if (isset($this->request->get['manufacturer_id'])) {
            $data['manufacturer_id'] = (string)$this->request->get['manufacturer_id'];
        } else {
            $data['manufacturer_id'] = array();
        }

        $this->load->model('catalog/manufacturer');

        $this->load->model('catalog/product');

        $data['manufacturer'] = array();

		$filter_data = array(
			'sort' => 'sort_order',
		);
		
        $manufacturers = $this->model_catalog_manufacturer->getManufacturers($filter_data);

        foreach ($manufacturers as $manufacturer) {
//            if (!isset($manufacturer['image'])){
//                $manufacturer['image'] = 'cache/no_image.png';
//            }
            if ($manufacturer['image']) {
                $image = $this->model_tool_image->resize($manufacturer['image'], 118, 82);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 118, 82);
            }

            $data['manufacturers'][] = array(
                'manufacturer_id' => $manufacturer['manufacturer_id'],
                'name' => $manufacturer['name'],
                'thumb' => $image,
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer.tpl', $data);
        } else {
            return $this->load->view('default/template/module/manufacturer.tpl', $data);
        }
    }
}