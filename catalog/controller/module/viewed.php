<?php
class ControllerModuleViewed extends Controller {
	public function index($setting) {
		$this->load->language('module/viewed');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
        $data['button_cart_zero'] = $this->language->get('button_cart_zero');
        $this->document->addScript('catalog/view/javascript/public/modules.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/swiper.min.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/swiper.settings.js','footer');
//        $this->document->addScript('catalog/view/javascript/new/common.js','footer');
//        $this->document->addStyle('catalog/view/theme/default/stylesheet/new/swiper.min.css');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

        $products = array();

        if (isset($this->request->cookie['viewed'])) {
            $products = explode(',', $this->request->cookie['viewed']);
        } else if (isset($this->session->data['viewed'])) {
            $products = $this->session->data['viewed'];
        }

        if (isset($this->request->get['route']) && $this->request->get['route'] == 'product/product') {
            if(isset($this->request->get['product_id'])) {
                $product_id = $this->request->get['product_id'];
                $products = array_diff($products, array($product_id));
                array_unshift($products, $product_id);
                setcookie('viewed', implode(',', $products), time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
            }
        }

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$products = array_slice($products, 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			//if ($product_info) {
            $this->request->get['product_id'] = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : 0;
            if ($product_info and $product_id != $this->request->get['product_id']) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

                $bests = $this->model_catalog_product->getBestSellerProducts(5);
                $flag = false;
                foreach ($bests as $best) {
                    if($product_info['product_id'] == $best['product_id']){
                        $flag = $product_info['product_id'];
                    }
                }

                $this->load->model('account/wishlist');
                $jek_wish = $this->model_account_wishlist->getAllWishlist();
                $true = false;
                if (isset($jek_wish)) {
                    foreach ($jek_wish as $in_wish) {
                        if ($product_info['product_id'] == $in_wish) {
                            $true = true;
                        }
                    }
                }

				$data['products'][] = array(
					'product_id'  => $product_info['product_id'],
					'manufacturer'=> $product_info['manufacturer'],
                    'inWish'      => $true,
                    'sticker'     => $flag,
                    'saving'      => $product_info['price'] == 0 ? 100 : round((($product_info['price'] - $product_info['special'])/$product_info['price'])*100, 2),
					'thumb'       => $image,
					'name'        => $product_info['name'],
                    'sku'         => (empty($product_info['sku'])) ? '' : $this->language->get('text_sku') .' '. $product_info['sku'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'model'       => $product_info['model'],
                    'sku'         => $product_info['sku'],
                    'upc'         => $product_info['upc'],
                    'ean'         => $product_info['ean'],
                    'jan'         => $product_info['jan'],
                    'isbn'        => $product_info['isbn'],
                    'mpn'         => $product_info['mpn'],
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
                    'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                    'quantity'    => $product_info['quantity'],
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/viewed.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/viewed.tpl', $data);
			} else {
				return $this->load->view('default/template/module/viewed.tpl', $data);
			}
		}
	}
}