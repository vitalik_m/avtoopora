/**
 * Created by Rj_Jeki4 on 07.11.2017.
 */
$(document).ready(function () {
    /*$(".item_tocart_notavailable").click(function () {
        $("#order").find("h2.modal-title").text($(this).parent().parent().find('h4 a').text());
        $("#order").find('input[name="product"]').val("<a href='" + $(this).parent().parent().find('h4 a').attr('href') + "'>" + $(this).parent().parent().find('h4 a').text() + "</a>");
    });*/
});
(function (d) {
    var $elements = document.querySelectorAll('.product-layout, .proditem, #product, .relatedproducts, .ads');

    for (var i = 0; i < $elements.length; i++) {
        $elements[i] && $elements[i].addEventListener('click', function (e) {
            var target = e.target;
            if (target.classList.contains('item_tocart_notavailable')) {
                e.preventDefault();
                if (this.id === 'product'){
                    $("#order").find("h2.modal-title").text($(this).find('h1').text());
                    $("#order").find('input[name="product"]').val("<a href='" + location.href + "'>" + $(this).find('h1').text() + "</a>");
                } else {
                    $("#order").find("h2.modal-title").text($(this).find('h4 a').text());
                    $("#order").find('input[name="product"]').val("<a href='" + $(this).find('h4 a').attr('href') + "'>" + $(this).find('h4 a').text() + "</a>");
                }
                //console.log($(this));

                return false;
            }
        });
    }

})(document);