$(document).ready(function () {
    $("#delivery_edit").click(function () {
        $("#delivery_section").toggleClass("ishidden_checkout ");
        $("#payment_section").toggleClass("ishidden_checkout ");
        $("#checkout_delivery").addClass("step_active");
        $("#checkout_delivery").toggleClass("step_checked");
        $("#checkout_payment").toggleClass(" step_active ");
    });
    document.querySelector('#delivery_submit').addEventListener('click', function (e) {
        var userName = $('#input-payment-firstname').val(),
            userLastName = $('#input-payment-lastname').val(),
            deliveryName = $('#delivery_checkout :selected').html(),
            cityName = $('#input-payment-city :selected').html(),
            addressClient = $('#input-payment-address-1').val(),
            payMetod = $('#payment_method :selected').html(),
            countryValue = $('#input-payment-country :selected').val(),
            zoneValue = $('#input-payment-zone :selected').val();
        $.ajax({
            success: function (res) {
                $('.name').html(userName + ' ' + userLastName);
                $('.city').html(cityName);
                $('.delivery').html(deliveryName);
                $('.street').html(addressClient);
                $('.pay').html(payMetod);
                //   $('.textPay').html(payDesc);
                $("input[name='shipping_firstname']").val(userName);
                $("input[name='shipping_lastname']").val(userLastName);
                $('input[name=\'shipping_address_1\']').val(addressClient);
                $('input[name=\'shipping_city\']').val(cityName);
                $('input[name=\'shipping_zone_id\']').val(zoneValue);
                $('input[name=\'shipping_country_id\']').val(countryValue);
            }
        });
    });
    $(document).ready(function () {
        $('a[href="#"]').click(function (e) {
            e.preventDefault();
        });
    });
});
//$(document).ready(function () {
var error = true;
// Login
$(document).delegate('#button-login', 'click', function () {
    $.ajax({
        url: 'index.php?route=checkout/checkout/login_validate',
        type: 'post',
        data: $('.login-form :input'),
        dataType: 'json',
        beforeSend: function () {
            $('#button-login').button('loading');
        },
        complete: function () {
            $('#button-login').button('reset');
        },
        success: function (json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('.login-form .message').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

// Register
$(document).delegate('#delivery_submit', 'click', function () {
    if (jQuery('#delivery_checkout :selected').prop('title') === "") {
        //$('.alert, .text-danger').remove();
        $('#delivery_checkout').after('<div class="text-danger">Выберите способ доставки!</div>');
        $('#delivery_checkout').parent().parent().addClass('err_alert');
        var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'tel\'], .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
        data += '&_shipping_method=' + $('#delivery_checkout :selected').prop('title');
        $.ajax({
            url: 'index.php?route=checkout/checkout/pre_validate',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('#button-register').button('loading');
            },
            complete: function () {
                $('#button-register').button('reset');
            },
            success: function (json) {
                //$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    error = true;
                    if (json['error']['warning']) {
                        $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                    $('.alert, .text-danger').remove();
                    $('#payment-address-new').find('.err_alert').removeClass('err_alert');
                    $('#delivery_checkout').after('<div class="text-danger">Выберите способ доставки!</div>');
                    $('#delivery_checkout').parent().parent().addClass('err_alert');
                    for (i in json['error']) {
                        $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        $('[name="' + i + '"]').parent().parent().addClass('err_alert');
                    }

                } else {
                    error = false;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    else {
        $('#delivery_checkout').parent().parent().removeClass('err_alert');
        var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'tel\'], .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
        data += '&_shipping_method=' + $('#delivery_checkout :selected').prop('title');
        $.ajax({
            url: 'index.php?route=checkout/checkout/pre_validate',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('#button-register').button('loading');
            },
            complete: function () {
                $('#button-register').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    error = true;
                    if (json['error']['warning']) {
                        $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    $('#payment-address-new').find('.err_alert').removeClass('err_alert');
                    for (i in json['error']) {
                        $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        $('[name="' + i + '"]').parent().parent().addClass('err_alert');
                    }

                } else {
                    error = false;
                    $("#checkout_delivery").addClass("step_checked");
                    $("#delivery_section").toggleClass("ishidden_checkout ");
                    $("#payment_section").toggleClass("ishidden_checkout ");
                    $("#checkout_payment").addClass("step_active");
                    //jQuery('#payment_method :selected').click();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
});
$(document).delegate('#button-register', 'click', function () {
    if (jQuery('#payment_method :selected').prop('title') === "") {
        //$('.alert, .text-danger').remove();
        $('#payment_section').after('<div class="text-danger">Выберите способ оплаты!</div>');
        $('#payment_section').parent().parent().removeClass('err_alert');
        var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form input[type=\'tel\'], .checkout_form textarea, .checkout_form select').serialize();
        data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title') + '&_payment_method=' + jQuery('#payment_method :selected').prop('title');

        $.ajax({
            url: 'index.php?route=checkout/checkout/validate',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('#button-register').button('loading');
            },
            complete: function () {
                $('#button-register').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    error = true;
                    //$('#payment_section').find('.err_alert').removeClass('err_alert');
                    if (json['error']['warning']) {
                        $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                    for (i in json['error']) {
                        $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        $('[name="' + i + '"]').parent().parent().addClass('err_alert');
                    }
                } else {
                    $('#payment_section').find('.err_alert').removeClass('err_alert');
                    error = false;
                    $("#checkout_payment").addClass("step_checked");
                    $("#payment_section").toggleClass("ishidden_checkout ");
                    $("#checkout_section").toggleClass("ishidden_checkout ");
                    $("#checkout_processing").addClass("step_active");
                    jQuery('#payment_method :selected').click();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    else {
        $('#payment_section').parent().parent().removeClass('err_alert');
        var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form input[type=\'tel\'], .checkout_form textarea, .checkout_form select').serialize();
        data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title') + '&_payment_method=' + jQuery('#payment_method :selected').prop('title');

        $.ajax({
            url: 'index.php?route=checkout/checkout/validate',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('#button-register').button('loading');
            },
            complete: function () {
                $('#button-register').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    error = true;
                    //$('#payment_section').find('.err_alert').removeClass('err_alert');
                    if (json['error']['warning']) {
                        $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                    for (i in json['error']) {
                        $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        $('[name="' + i + '"]').parent().addClass('err_alert');
                    }
                } else {
                    $('#payment_section').find('.err_alert').removeClass('err_alert');
                    error = false;
                    $("#checkout_payment").addClass("step_checked");
                    $("#payment_section").toggleClass("ishidden_checkout ");
                    $("#checkout_section").toggleClass("ishidden_checkout ");
                    $("#checkout_processing").addClass("step_active");
                    jQuery('#payment_method :selected').click();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
});

//$('select[name=\'country_id\']').on('change', function () {
/*$('input[name="payment_address"]').on('click', function () {
 var value = $('input[name=\'country_id\']').attr('data-value');
 $.ajax({
 url: 'index.php?route=checkout/checkout/country&country_id=' + value,
 dataType: 'json',
 beforeSend: function () {
 $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
 },
 complete: function () {
 $('.fa-spinner').remove();
 },
 success: function (json) {
 if (json['postcode_required'] == '1') {
 $('input[name=\'postcode\']').parent().parent().addClass('required');
 } else {
 $('input[name=\'postcode\']').parent().parent().removeClass('required');
 }

 html = '<option value=""> --- Выберите --- </option>';

 if (json['zone'] && json['zone'] != '') {
 for (i = 0; i < json['zone'].length; i++) {
 html += '<option value="' + json['zone'][i]['zone_id'] + '"';

 if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
 html += ' selected="selected"';
 }

 html += '>' + json['zone'][i]['name'] + '</option>';
 }
 } else {
 html += '<option value="0" selected="selected"> --- Не выбрано --- </option>';
 }

 $('select[name=\'zone_id\']').html(html).val("");
 },
 error: function (xhr, ajaxOptions, thrownError) {
 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
 }
 });
 });*/
var value = $('input[name=\'country_id\']').attr('data-value');

$.ajax({
    url: 'index.php?route=checkout/checkout/country&country_id=' + value,
    dataType: 'json',
    beforeSend: function () {
        $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
    },
    complete: function () {
        $('.fa-spinner').remove();
    },
    success: function (json) {
        if (json['postcode_required'] == '1') {
            $('input[name=\'postcode\']').parent().parent().addClass('required');
        } else {
            $('input[name=\'postcode\']').parent().parent().removeClass('required');
        }

        html = '<option value=""> --- Выберите --- </option>';

        if (json['zone'] && json['zone'] != '') {
            for (i = 0; i < json['zone'].length; i++) {
                html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                    html += ' selected="selected"';
                }

                html += '>' + json['zone'][i]['name'] + '</option>';
            }
        } else {
            html += '<option value="0" selected="selected"> --- Не выбрано --- </option>';
        }

        $('select[name=\'zone_id\']').html(html).val("");
        $('.selectpicker').selectpicker('refresh');
    },
    error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
});
/*
 if (undefined !== window.isLogged && window.isLogged) {
 //console.log('isLogged');
 $('input[name="payment_address"]').on('click', function () {
 //console.log('click at pey new');
 var value = $('input[name=\'country_id\']').attr('data-value');
 $.ajax({
 url: 'index.php?route=checkout/checkout/country&country_id=' + value,
 dataType: 'json',
 beforeSend: function () {
 $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
 },
 complete: function () {
 $('.fa-spinner').remove();
 },
 success: function (json) {
 if (json['postcode_required'] == '1') {
 $('input[name=\'postcode\']').parent().parent().addClass('required');
 } else {
 $('input[name=\'postcode\']').parent().parent().removeClass('required');
 }

 html = '<option value=""> --- Выберите --- </option>';

 if (json['zone'] && json['zone'] != '') {
 for (i = 0; i < json['zone'].length; i++) {
 html += '<option value="' + json['zone'][i]['zone_id'] + '"';

 if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
 html += ' selected="selected"';
 }

 html += '>' + json['zone'][i]['name'] + '</option>';
 }
 } else {
 html += '<option value="0" selected="selected"> --- Не выбрано --- </option>';
 }
 $('select[name=\'zone_id\']').html(html).val("");
 $('#input-payment-zone').selectpicker();
 },
 error: function (xhr, ajaxOptions, thrownError) {
 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
 }
 });
 });
 } else {
 var value = $('input[name=\'country_id\']').attr('data-value');

 $.ajax({
 url: 'index.php?route=checkout/checkout/country&country_id=' + value,
 dataType: 'json',
 beforeSend: function () {
 $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
 },
 complete: function () {
 $('.fa-spinner').remove();
 },
 success: function (json) {
 if (json['postcode_required'] == '1') {
 $('input[name=\'postcode\']').parent().parent().addClass('required');
 } else {
 $('input[name=\'postcode\']').parent().parent().removeClass('required');
 }

 html = '<option value=""> --- Выберите --- </option>';

 if (json['zone'] && json['zone'] != '') {
 for (i = 0; i < json['zone'].length; i++) {
 html += '<option value="' + json['zone'][i]['zone_id'] + '"';

 if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
 html += ' selected="selected"';
 }

 html += '>' + json['zone'][i]['name'] + '</option>';
 }
 } else {
 html += '<option value="0" selected="selected"> --- Не выбрано --- </option>';
 }
 $('select[name=\'zone_id\']').html(html).val("");

 },
 error: function (xhr, ajaxOptions, thrownError) {
 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
 }
 });
 }
 */
//});


$('select[name=\'shipping_country_id\']').on('change', function () {
    $.ajax({
        url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function () {
            $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function () {
            $('.fa-spinner').remove();
        },
        success: function (json) {
            if (json['postcode_required'] == '1') {
                $('input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                $('input[name=\'postcode\']').parent().parent().removeClass('required');
            }

            html = '<option value=""><?php echo $text_select; ?></option>';

            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }

                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }

            $('select[name=\'shipping_zone_id\']').html(html).val("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});


$('select[name=\'country_id\'], select[name=\'zone_id\'], select[name=\'shipping_country_id\'], select[name=\'shipping_zone_id\'], input[type=\'radio\'][name=\'payment_address\'], input[type=\'radio\'][name=\'shipping_address\']').on('change', function () {
    if (this.name == 'contry_id') jQuery("select[name=\'zone_id\']").val("");
    if (this.name == 'shipping_country_id') jQuery("select[name=\'shipping_zone_id\']").val("");

    jQuery(".shipping-method").load('index.php?route=checkout/checkout/shipping_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function () {
        if (jQuery("input[name=\'shipping_method\']:first").length) {
            jQuery("input[name=\'shipping_method\']:first").attr('checked', 'checked').prop('checked', true).click();
        } else {
            jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
        }
    });

    jQuery(".payment_method").load('index.php?route=checkout/checkout/payment_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function () {
        jQuery("#payment_method").removeAttr("selected").prop('selected', false);
    });
});


$(document).delegate('#delivery_checkout', 'click', function () {
    jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
});


$('body').delegate('#payment_method', 'change', function () {
    var text = jQuery('#payment_method :selected').data('about'),
        code = jQuery('#payment_method :selected').val();
    jQuery('.paydescr_text').html(text);
    if ( code === 'liqpay' ){
        jQuery('#button-register').val('Оплатить через LIQPAY')
    } else {
        jQuery('#button-register').val('Сохранить')
    }
    jQuery('#button-register').removeClass('hidden');
});

$('body').delegate('#payment_method', 'click', function () {

    var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
    data += '&_shipping_method=' + jQuery('#delivery_checkout :selected').prop('title') + '&_payment_method=' + jQuery('#payment_method :selected').prop('title');

    if (!error)
        $.ajax({
            url: 'index.php?route=checkout/checkout/confirm',
            type: 'post',
            data: data,
            success: function (html) {
                jQuery(".pay").html(html);

                jQuery("#button-confirm").click();
                jQuery("#button-confirm_liqpay").click();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
});

$('select[name=\'country_id\']').trigger('change');
jQuery(document).ready(function () {
    jQuery('input:radio[name=\'payment_method\']:first').attr('checked', true).prop('checked', true);
});