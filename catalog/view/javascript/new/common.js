//QUANTITY-SLIDER---------------------------------------------------------------------------

$(document).ready(function () {
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('.quantity input');

    (function (d) {
        var $inputs = d.querySelectorAll('.quantity');

        (function (inputs) {
            for (var i = 0; i < inputs.length; i++) {
                inputs[i] && inputs[i].addEventListener('click', function (e) {
                    e.preventDefault();
                    var target = e.target,
                        input = this.getElementsByTagName('INPUT')[0],
                        min = input.getAttribute('min'),
                        max = input.getAttribute('max');
                        if ( max === ''){
                            max = 99;
                        }
                    if (target.classList.contains('quantity-up')) {
                        var oldValue = parseInt(input.value);
                        if (oldValue >= max) {
                            var newVal = oldValue;
                        } else {
                            var newVal = oldValue + 1;
                        }
                        input.value = newVal;
                        if (this.classList.contains('cart')) {
                            var cartId = input.getAttribute('data-cart-id');
                            cart.update(cartId, newVal);
                        }
                    }
                    if (target.classList.contains('quantity-down')) {
                        var oldValue = parseInt(input.value);
                        if (oldValue <= min) {
                            var newVal = oldValue;
                        } else {
                            var newVal = oldValue - 1;
                        }
                        input.value = newVal;
                        if (this.classList.contains('cart')) {
                            var cartId = input.getAttribute('data-cart-id');
                            cart.update(cartId, newVal);
                        }
                    }
                    return false;
                });
                inputs[i] && inputs[i].addEventListener('keyup', function ( e ) {
                    var target = e.target,
                        input = this.getElementsByTagName('INPUT')[0];
                    if (target.value !== '') {
                        var cartId = input.getAttribute('data-cart-id');
                        cart.update(cartId, target.value);
                    }
                });
            }
        })($inputs);
    })(document)
});

//added product to cart

(function (d) {
    var $elements = document.querySelectorAll('.product-layout, .proditem, #product, .relatedproducts, .ads');

    for (var i = 0; i < $elements.length; i++) {
        $elements[i] && $elements[i].addEventListener('click', function (e) {
            var target = e.target;
            if (target.classList.contains('item_tocart') || target.classList.contains('oneitem_tocart') || target.classList.contains('relatedproducts_tocart')) {
                e.preventDefault();
                if (this.querySelector('.quantity input:first-child')) {
                    var id = this.getAttribute('data-id'),
                        quantity = this.querySelector('.quantity input:first-child').value;
                } else {
                    var id = this.getAttribute('data-id'),
                        quantity = 1;
                }
                $.ajax({
                    url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: {
                        quantity: quantity,
                        product_id: id
                    },
                    dataType: 'json',
                    success: function (json) {
                        $('.alert, .text-danger').remove();
                        $('.form-group').removeClass('has-error');

                        $('#cart > .cart_header').addClass('in_cart');

                        if (json['error']) {
                            if (json['error']['option']) {
                                for (i in json['error']['option']) {
                                    var element = $('#input-option' + i.replace('_', '-'));

                                    if (element.parent().hasClass('input-group')) {
                                        element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                    } else {
                                        element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                    }
                                }
                            }
                            if (json['error']['recurring']) {
                                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                            }

                            // Highlight any found errors
                            $('.text-danger').parent().addClass('has-error');
                        }
                        if (json['redirect']) {
                            location = json['redirect'];
                        }
                        if (json['success']) {
                            $('#top').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $('#cart > .cart_header').html('<span class="on_cart">' + json['total'] + '</span>' + ' ' + json['text_cart']);
                            //$('#cart > .cart_reload').load('index.php?route=common/cart/info .cart_header');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
                return false;
            }
        });
    }

})(document);

$(document).ready(function () {

    $("#cbackform").submit(function (e) { //устанавливаем событие отправки для формы с id=form
        e.preventDefault();
        var form_data = $(this).serialize(); //собераем все данные из формы
        $.ajax({
            type: "POST", //Метод отправки
            url: "/mail.php", //путь до php фаила отправителя
            data: form_data,
            beforeSend: function () {
                //console.log('jnghfdrf');
            },
            success: function (res) {
                $('#myModal').modal('toggle');
                $('#top').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> Ваше сообщение отправлено! <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                //код в этом блоке выполняется при успешной отправке сообщения
                /*alert("Ваше сообщение отправлено!");
                 console.log('res', res);*/
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    //  });
});

//added / removed wishlist items

(function (d) {
    var $elements = document.querySelectorAll('.item_infavorites, .item_tofavorites');
    for (var i = 0; i < $elements.length; i++) {
        $elements[i] && $elements[i].addEventListener('click', _wishlistButtonsHandler);
    }

    function _wishlistButtonsHandler(e) {
        var id = this.getAttribute('data-id');
        if (id) {
            e.preventDefault();
            if (this.classList.contains('item_infavorites')) {
                this.classList.remove('item_infavorites');
                this.classList.add('item_tofavorites');
                wishlist.remove(id);
            }
            else {
                this.classList.remove('item_tofavorites');
                this.classList.add('item_infavorites');
                wishlist.add(id);
            }
        }
    }
})(document);

(function (k) {
    var menu = k.querySelector('.dropdown'),
        $li = menu.querySelectorAll('li li.dropdown-submenu'),
        target = menu.querySelector('.ads');

    for (var i = 0; i < $li.length; i++) {
        $li[i] && $li[i].addEventListener('mouseenter', _thirdLiEnterHandler);
        $li[i] && $li[i].addEventListener('mouseleave', _thirdLiLeaveHandler);
    }

    function _thirdLiEnterHandler(e) {
        if (target && !target.classList.contains('third-li')) {
            target.classList.add('third-li')
        }
    }

    function _thirdLiLeaveHandler(e) {
        if (target && target.classList.contains('third-li')) {
            target.classList.remove('third-li')
        }
    }
})(document);