$(document).ready(function(){
    function isCheck(name) {
        return document.querySelector('input[name="' + name + '"]:checked');
    }
    $('.radio-toolbar input:radio').change(function(){
        starcount = isCheck('rating').value;
        console.log(starcount);
        allLabels = $('.radio-toolbar label');
        for (var i = 0; i < allLabels.length; i++) {
            if ( i < starcount ) {
                allLabels[i].style = "background: url(/catalog/view/theme/default/stylesheet/img/star-select.svg)";
            }
            else {
                allLabels[i].style = "background: url(/catalog/view/theme/default/stylesheet/img/star-unselect.svg)";
            }
        }
    });
});