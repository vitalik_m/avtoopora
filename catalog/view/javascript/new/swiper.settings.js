$(document).ready(function () {

    var mediagridSliderCarSearch = {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 4,
            spaceBetween: 30
        }
    };

    var mediagridSliderCarNoSlide = {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 4,
            spaceBetween: 30
        }
    };

    var mediagridSlider = {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 2,
            spaceBetween: 30
        }
    };


    var mediagridCartSlider = {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 30
        },
        870: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 30
        },
        981: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 30
        },

        1100: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            spaceBetween: 30
        },
        1200: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            spaceBetween: 30
        }
    };


//TOP-SLIDER-INIT
    var swiper = new Swiper('.top_slider', {
        pagination: '.topslide-swiper-pagination',
        paginationClickable: true
    });

    var swiper2 = new Swiper('.search_slider', {
        //pagination: '.swiper-pagination2',
        //  paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 6,
        breakpoints: mediagridSliderCarSearch

    });

    var blockSlider = new Swiper('.specialorder_slider', {
        pagination: '.specialorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.specialorder_blockslider-next',
        prevButton: '.specialorder_blockslider-prev',
        breakpoints: mediagridSlider
        // autoHeight: true
    });

    var viewedSlider = new Swiper('.viewed_slider', {
        pagination: '.viewed_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.viewed_blockslider-next',
        prevButton: '.viewed_blockslider-prev',
        breakpoints: mediagridSlider
        // autoHeight: true
    });




    var popularorderSlider = new Swiper('.popularorder_slider', {
        pagination: '.popularorder_swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerView: 4,
        nextButton: '.popularorder_blockslider-next',
        prevButton: '.popularorder_blockslider-prev',
        breakpoints: mediagridSlider
    });

    var recommendedCartSlider = new Swiper('#no_slide', {
        paginationClickable: false,
        spaceBetween: 30,
        slidesPerView: 4,
        breakpoints: mediagridSliderCarNoSlide,
        simulateTouch: false,
        noSwiping: true,
        breakpoints: mediagridCartSlider
    });

});