<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="error"></div>
    <div class="row">
        <div id="checkout_progressbar">
            <p id="checkout_delivery" class="step step_active">
                <span class="step_text">Доставка</span>
            </p>
            <hr class="checkout_line">
            <p id="checkout_payment" class="step">
                <span class="step_text">Оплата</span>
            </p>
            <hr class="checkout_line">
            <p id="checkout_processing" class="step">
                <span class="step_text">Заказ в обработке</span>
            </p>
        </div>
    </div>
</div>
<section id="delivery_section" class="checkout_form">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
                <?php /* if (!isset($address)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <p class="well">
                            <?php echo $text_returning_customer; ?>
                            &nbsp;<a href="#" onclick="jQuery('.login-form').toggle();return false;">
                                <?php echo $text_i_am_returning_customer; ?>
                            </a>
                        </p>
                        <div class="login-form registerbox clearfix" style="display:none">
                            <div class="row">
                                <div class="col-md-12 message"></div>
                                <form class="form-inline" role="form">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label"
                                                   for="input-email"><?php echo $entry_email; ?></label>
                                            <input type="text" name="email" value=""
                                                   placeholder="<?php echo str_replace(':','',$entry_email); ?>"
                                                   id="input-email" class="form-control"/>
                                        </div>&nbsp;&nbsp;
                                        <div class="form-group">
                                            <label class="control-label"
                                                   for="input-password"><?php echo $entry_password; ?></label>
                                            <input type="password" name="password" value=""
                                                   placeholder="<?php echo str_replace(':','',$entry_password); ?>"
                                                   id="input-password" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="button" value="<?php echo $button_login; ?>" id="button-login"
                                               data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"
                                               class="btn_checkout btn_profileenter"/>
                                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } */ // Если зарегистрирован ?>
                <?php if ($addresses) { //Если адрес уже был введен (зарегистрированный пользователь) ?>
                <?php if (isset($customer_id)) { ?>
                <div class="radio option-group">
                    <input type="radio" name="payment_address" value="existing" checked="checked"
                           onclick="jQuery('#payment-address-new').hide(); jQuery('#shipping-method-old').show();$('#shipping-method-old').find('select').attr('name', 'shipping_method'); $('#shipping-method-old').find('select').attr('id', 'delivery_checkout'); $('#shipping-method-new').find('select').removeAttr('name'); $('#shipping-method-new').find('select').attr('id', 'delivery_checkout_1');"/>
                    <label class="radio-inline"><?php echo $text_address_existing; ?></label>
                </div>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required" id="payment-existing">
                    <label class="not-request">Выберите адрес доставки</label>
                    <select name="payment_address_id" class="selectpicker custom-select">
                        <?php foreach ($addresses as $address) { ?>
                        <?php if (isset($payment_address_id) && $address['address_id'] == $payment_address_id) { ?>
                        <option value="<?php echo $address['address_id']; ?>"
                                selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>
                            , <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
                            , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>
                            , <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
                            , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </fieldset>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required" id="shipping-method-old">
                    <?php if ($error_warning) { ?>
                    <div class="alert alert-warning"><i
                                class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                    <?php } ?>
                    <?php if ($shipping_methods) { ?>
                    <label class="control-label" for="delivery_checkout">
                        <?php echo $text_shipping_method; ?>
                    </label>
                    <select id="delivery_checkout" class="selectpicker custom-select" name="shipping_method">
                        <option value="" selected disabled>Выберите способ доставки</option>
                        <?php foreach ($shipping_methods as $shipping_method): ?>
                        <?php if (!$shipping_method['error']) { ?>
                        <?php foreach ($shipping_method['quote'] as $quote): ?>
                        <?php if ($quote['code'] == $code || !$code) { ?>
                        <?php $code = $quote['code']; ?>
                        <option value="<?php echo $quote['code']; ?>" title="<?php echo $quote['title']; ?>">
                            <?php echo $quote['title']; ?>
                        </option>
                        <?php } else { ?>
                        <option value="<?php echo $quote['code']; ?>" title="<?php echo $quote['title']; ?>">
                            <?php echo $quote['title']; ?>
                        </option>
                        <?php } ?>
                        <?php /* echo $quote['title']; ?> - <?php echo $quote['text']; */ // название - стоимость ?>
                        <?php endforeach; ?>
                        <?php } else { ?>
                        <div class="alert alert-danger">
                            <?php echo $shipping_method['error']; ?>
                        </div>
                        <?php } ?>
                        <?php endforeach; ?>
                    </select>
                    <?php } ?>
                    <input id="checkout-input" type="hidden">
                </fieldset>
                <?php } ?>
                <?php if (isset($customer_id)) { ?>
                <div class="radio option-group">
                    <input type="radio" name="payment_address" value="new"
                           onclick="jQuery('#payment-address-new').show(); jQuery('#shipping-method-old').hide(); $('#shipping-method-old').find('select').removeAttr('name'); $('#shipping-method-old').find('select').attr('id', 'delivery_checkout_1'); $('#shipping-method-new').find('select').attr('name', 'shipping_method'); $('#shipping-method-new').find('select').attr('id', 'delivery_checkout')"/>
                    <label class="radio-inline"><?php echo $text_address_new; ?></label>
                </div>
                <?php } ?>
                <?php }  ?>
                <div id="payment-address-new" style="<?php if (isset($customer_id)) { echo 'display: none;' ;} ?>">
                    <input type="hidden" name="customer_group_id" value="1"/>
                    <input type="hidden" name="company" value=""/>
                    <input type="hidden" name="company_id" value=""/>
                    <input type="hidden" name="tax_id" value=""/>
                    <input type="hidden" name="address_2" value=""/>
                    <input type="hidden" name="postcode" value=""/>
                    <input type="hidden" name="comment" value=""/>
                    <input type="hidden" name="fax" value=""/>
                    <input type="hidden" name="shipping_firstname" value=""/>
                    <input type="hidden" name="shipping_lastname" value=""/>
                    <input type="hidden" name="shipping_company" value=""/>
                    <input type="hidden" name="shipping_address_1" value=""/>
                    <input type="hidden" name="shipping_address_2" value=""/>
                    <input type="hidden" name="shipping_city" value=""/>
                    <input type="hidden" name="shipping_postcode" value=""/>
                    <input type="hidden" name="shipping_country_id" value=""/>
                    <fieldset class="checkout_required required form-group">
                        <div>
                            <label class="control-label" for="input-payment-firstname">
                                <?php echo $entry_firstname; ?>
                            </label>
                            <input type="text" name="firstname"
                                   value="<?php if (isset($firstname)): echo $firstname; elseif (isset($address['firstname'])): echo $address['firstname']; endif; ?>"
                                   placeholder="<?php echo str_replace(':','',$entry_firstname); ?>"
                                   id="input-payment-firstname"
                                   class="form-control" <?php /* if (isset($customer_id)) { ?> readonly <?php } */ ?>/>
                        </div>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <div>
                            <label class="control-label" for="input-payment-lastname">
                                <?php echo $entry_lastname; ?>
                            </label>
                            <input type="text" name="lastname" value="<?php if (isset($lastname)) echo $lastname;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_lastname); ?>"
                                   id="input-payment-lastname"
                                   class="form-control" <?php /*if (isset($customer_id)) { ?> readonly<?php }*/?>/>
                        </div>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <div>
                            <label class="control-label" for="input-payment-email"><?php echo $entry_email; ?></label>
                            <input type="text" name="email" value="<?php if (isset($email)) echo $email;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_email); ?>"
                                   id="input-payment-email"
                                   class="form-control" <?php /*if (isset($customer_id)) { ?> readonly<?php }*/?>/>
                        </div>
                    </fieldset>

                    <h2 class="section_subheader"><?php echo $text_your_address; ?></h2>

                    <fieldset class="select_checkoutsort select_arr_checkout checkout_required"
                              id="shipping-method-new">
                        <?php if ($error_warning) { ?>
                        <div class="alert alert-warning">
                            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                        </div>
                        <?php } ?>
                        <?php if ($shipping_methods) { ?>

                        <label class="control-label" for="delivery_checkout">
                            <?php echo $text_shipping_method; ?>
                        </label>
                        <select class="selectpicker custom-select" <?php if (!isset($customer_id)) { ?>
                        id="delivery_checkout"
                        name="shipping_method" <?php } ?> >
                        <option value="" selected disabled>Выберите способ доставки</option>
                        <?php foreach ($shipping_methods as $shipping_method): ?>
                        <?php if (!$shipping_method['error']) { ?>
                        <?php foreach ($shipping_method['quote'] as $quote): ?>
                        <?php if ($quote['code'] == $code || !$code) { ?>
                        <?php $code = $quote['code']; ?>
                        <option value="<?php echo $quote['code']; ?>"
                                title="<?php echo $quote['title']; ?>">
                            <?php echo $quote['title']; ?>
                        </option>
                        <?php } else { ?>
                        <option value="<?php echo $quote['code']; ?>"
                                title="<?php echo $quote['title']; ?>">
                            <?php echo $quote['title']; ?>
                        </option>
                        <?php } ?>
                        <?php /* echo $quote['title']; ?> - <?php echo $quote['text']; */ // название - стоимость ?>
                        <?php endforeach; ?>
                        <?php } else { ?>
                        <div class="alert alert-danger">
                            <?php echo $shipping_method['error']; ?>
                        </div>
                        <?php } ?>
                        <?php endforeach; ?>
                        </select>
                        <?php } ?>
                        <input id="checkout-input" type="hidden">
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <div>
                            <label class="control-label"
                                   for="input-payment-country"><?php echo $entry_country; ?></label>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == 220) { ?>
                            <input type="text" name="country_id" id="input-payment-country" class="form-control"
                                   value="<?php echo $country['name']; ?>"
                                   data-value="<?php echo $country['country_id']; ?>" readonly>
                            <?php } ?>
                            <?php } /* ?>
                            <select name="country_id" id="input-payment-country"
                                    class="selectpicker custom-select">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($countries as $country) { ?>
                                <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>"
                                        selected="selected"><?php echo $country['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                            <?php */ ?>
                        </div>
                    </fieldset>

                    <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                        <label class="control-label" for="input-payment-zone"><?php echo $entry_zone;; ?></label>
                        <select name="zone_id" id="input-payment-zone"
                                class="selectpicker custom-select">
                        </select>
                    </fieldset>

                    <fieldset class="checkout_required required form-group">
                        <div>
                            <label class="control-label" for="input-payment-city"><?php echo $entry_city; ?></label>
                            <input type="text" name="city" value="<?php if (isset($city)) echo $city;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_city); ?>" id="input-payment-city"
                                   class="form-control"/>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div>
                            <label class="control-label"
                                   for="input-payment-address-1"><?php echo $entry_address_1; ?></label>
                            <input type="text" name="address_1" value="<?php if (isset($address_1)) echo $address_1;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_address_1); ?>"
                                   id="input-payment-address-1"
                                   class="form-control"/>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div>
                            <label class="control-label"
                                   for="input-payment-address-2"><?php echo $entry_address_2; ?></label>
                            <input type="text" name="address_2" value="<?php if (isset($address_2)) echo $address_2;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_address_2); ?>"
                                   id="input-payment-address-2"
                                   class="form-control"/>
                        </div>
                    </fieldset>

                    <fieldset class="checkout_required form-group required">
                        <div>
                            <label class="control-label"
                                   for="input-payment-telephone"><?php echo $entry_telephone; ?></label>
                            <input type="tel" name="telephone" value="<?php if (isset($telephone)) echo $telephone;?>"
                                   placeholder="<?php echo str_replace(':','',$entry_telephone); ?>"
                                   id="input-payment-telephone"
                                   class="form-control" <?php /*if (isset($customer_id)) { ?> readonly<?php }*/ ?>/>
                        </div>
                    </fieldset>
                    <fieldset class="checkout_unrequired form-group placeholder_black">
                        <div>
                            <label class="control-label">
                                <span class="pull-left"><?php echo $text_comments; ?></span>
                                <span class="pull-right">(по желанию)</span>
                            </label>
                            <textarea name="comment" rows="3" class="form-control"
                                      placeholder="<?php echo $text_comment; ?>"><?php echo $comment; ?></textarea>
                        </div>
                    </fieldset>
                </div>
                <?php if (!isset($customer_id)) { // Регистрировать при покупке или нет ?>
                <div class="form-group col-md-12">
                    <label>
                        <input type="checkbox" name="register"
                               onclick="jQuery('.register-form').toggle()">&nbsp;<?php echo $text_register; ?>
                    </label>
                </div>
                <?php } ?>
                <div class="register-form" style="display:none">
                    <div class="form-group required col-md-6">
                        <label class="control-label" for="input-payment-password"><?php echo $entry_password; ?></label>
                        <input type="password" name="password" value=""
                               placeholder="<?php echo str_replace(':','',$entry_password); ?>"
                               id="input-payment-password"
                               class="form-control"/>
                    </div>
                    <div class="form-group required col-md-6">
                        <label class="control-label" for="input-payment-confirm"><?php echo $entry_confirm; ?></label>
                        <input type="password" name="confirm" value=""
                               placeholder="<?php echo str_replace(':','',$entry_confirm); ?>"
                               id="input-payment-confirm"
                               class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <span>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <span class="divider pull-right"></span>
                    <p class="checkout_cartinfolink">
                        <span><a class="checkout_cartinfolink" href="<?= $cart ?>"><?= $edit_cart; ?></a></span>
                        <span>
                            <?php foreach ($totals as $total): if ($total['title'] == 'Всего'): echo $total['title'] . ': ' . $total['text']; endif; endforeach; ?>
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-md-6">
                    <input id="delivery_submit" class="pull-right btn_checkout" type="submit"
                           value="сохранить и перейти к оплате">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="payment_section" class="ishidden_checkout checkout_form">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h2 class="section_header">Оформление заказа</h2>
                <h2 class="section_subheader"><?=$text_payment_method; ?></h2>
                <?php if ($error_warning) { ?>
                <div class="alert alert-warning">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                </div>
                <?php } ?>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                    <div>
                        <label class="control-label" for="payment_method">Способ оплаты</label>
                        <?php if ($payment_methods): ?>
                        <select id="payment_method" class="selectpicker custom-select" name="payment_method">
                            <option value="" selected disabled>Выберите способ оплаты</option>
                            <?php foreach ($payment_methods as $payment_method): ?>
                            <?php if ($payment_method['code'] == $code || !$code): ?>
                            <?php $code = $payment_method['code']; ?>
                            <option value="<?php echo $payment_method['code']; ?>"
                                    title="<?php echo $payment_method['title']; ?>"
                                    data-about="<?=isset($payment_method['text'])?$payment_method['text']:'' ?>">
                                <?php echo $payment_method['title']; ?>
                            </option>
                            <?php else: ?>
                            <option value="<?php echo $payment_method['code']; ?>"
                                    title="<?php echo $payment_method['title']; ?>"
                                    data-about="<?=isset($payment_method['text'])?$payment_method['text']:'' ?>">
                                <?php echo $payment_method['title']; ?>
                            </option>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                        <?php endif; ?>
                        <input type="hidden" id="payment-input">
                    </div>
                </fieldset>
                <p class="paydescr_text">

                </p>
                <?php if ($text_agree) { ?>
                <div class="option-group">
                    <?php if ($agree) { ?>
                    <input type="checkbox" name="agree" value="1" checked="checked"/>
                    <?php } else { ?>
                    <input type="checkbox" name="agree" value="1"/>
                    <?php } ?>
                    <label for=""><?php echo $text_agree; ?></label>
                </div>
                <?php } else { ?>
                <div class="buttons">
                    <div class="pull-right">
                        <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method"
                               data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"
                               class="btn_checkout btn_profileenter"/>
                    </div>
                </div>
                <?php } ?>
                <input type="submit" class="pull-right btn_checkout hidden"
                       data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"
                       id="button-register" value="<?php echo $text_cart;?>">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <span>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <span class="divider pull-right"></span>
                    <p class="checkout_cartinfolink">
                        <span><a class="checkout_cartinfolink" href="<?= $cart ?>"><?= $edit_cart; ?></a></span>
                        <span>
                            <?php foreach ($totals as $total): if ($total['title'] == 'Всего'): echo $total['title'] . ': ' . $total['text']; endif; endforeach; ?>
                        </span>
                    </p>
                </div>
                <div class="checkout_cartinfo">
                    <h3 class="delivery_headblock delivery"></h3>
                    <p class="delivery_block name"></p>
                    <p class="delivery_block city"></p>
                    <p class="delivery_block street"></p>
                    <p class="delivery_block checkout_cartinfolink"><a id="delivery_edit" href="#">
                            Редактировать доставку
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="checkout_section" class="ishidden_checkout">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="checkout_text"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="checkout_cartinfo">
                    <?php foreach ($products as $product): ?>
                    <p>
                        <span>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            (<?php echo $product['quantity']; ?> шт.)
                        </span>
                        <?php foreach ($product['option'] as $option): ?>
                        <small> - <?php echo $option['name']; ?>
                            : <?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?>
                        </small>
                        <?php endforeach; ?>
                        <span><?php echo $product['total']; ?></span>
                    </p>
                    <?php endforeach; ?>
                    <p class="checkout_cartinfolink">
                         <span class="already_price">
                             <?php foreach ($totals as $total): if ($total['title'] == 'Всего'): echo $total['title'] . ': ' . $total['text']; endif; endforeach; ?>
                        </span>
                    </p>
                </div>
                <div class="checkout_cartinfo"><h3 class="delivery_headblock delivery"></h3>
                    <p class="delivery_block name"></p>
                    <p class="delivery_block city"></p>
                    <p class="delivery_block street"></p></div>
                <div class="checkout_cartinfo pay"></div>
            </div>
        </div>
    </div>
</section>
<?php echo $column_right; ?>
<div class="container">
    <?php echo $content_bottom; ?>
</div>

<?php echo $footer;?>
