<h3 class="delivery_headblock"><?php echo $text_title ?></h3>
<p><?php echo $cod; ?></p>
<div class="buttons">
    <div class="pull-right hidden">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn_checkout btn_profileenter"
               data-loading-text="<?php echo $text_loading; ?>"/>
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-confirm').on('click', function () {
        $.ajax({
            type: 'get',
            url: 'index.php?route=payment/cod/confirm',
            cache: false,
            beforeSend: function () {
                $('#button-confirm').button('loading');
            },
            complete: function () {
                $('#button-confirm').button('reset');
            },
            success: function () {
                //$('#cart > ul').load('index.php?route=common/cart/info ul li');
                location = '<?php echo $continue; ?>';
                <!--$('#checkout_text').load('<?php echo $continue; ?>');-->
            }
        });
    });
    //--></script>
