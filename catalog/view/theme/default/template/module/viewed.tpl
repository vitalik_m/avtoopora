<section class="viewed_item">
    <div class="container">
        <h2 class="sectionheader main_sectionheader"><?php echo $heading_title; ?></h2>
        <div class="block-wrapper">
            <div class="blockslider-next viewed_blockslider-next"></div>
            <div class="blockslider-prev viewed_blockslider-prev"></div>
            <div class="swiper-container block_slider viewed_slider">
                <div class="swiper-wrapper blockslider_flex">
                    <?php foreach ($products as $product) : ?>
                    <div class="swiper-slide blockslider-height proditem" data-id="<?=$product['product_id'];?>">
                        <div class="col-md-12 blockslider_vieweditems">
                            <div class="blockslider_imgwrapper">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <h4 class="part_header">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <?php if ($product['price']) : ?>
                            <p class="partprice partprice_available">
                                <?php if (!$product['special']) : ?>
                                <?php echo $product['price']; ?>
                                <?php else : ?>
                                <span class="oldprice"><?php echo $product['price']; ?></span>
                                <span class="current_price"><?php echo $product['special']; ?></span>
                                <?php endif; ?>
                            </p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination viewed_swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>