<?php if (isset($manufacturers)): ?>
<section class="model_search">
    <div class="container">
        <h2 class="sectionheader main_sectionheader"><?php echo $heading_title; ?></h2>
        <div class="swiper-container search_slider">
            <div class="swiper-wrapper">
                <?php foreach ($manufacturers as $manufacturer) { ?>
                <div class="swiper-slide">
                    <div class="col-md-12 carsearch_bordered">
                        <?php if ($manufacturer['href']) { ?>
                        <a href="<?php echo $manufacturer['href']; ?>">
                            <img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>"
                                 class="img-responsive center-block">
                            <span class="carmark_descr"><?php echo $manufacturer['name']; ?></span>
                        </a>
                        <?php } else { ?>
                        <img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>"
                             class="img-responsive center-block">
                        <span class="carmark_descr"><?php echo $manufacturer['name']; ?></span>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>