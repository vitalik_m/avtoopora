<div class="container">
    <div class="row main_categories imgcategory">
        <?php foreach ($categories as $category) { ?>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <a href="<?php echo $category['href']; ?>" class="maincat">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>"
                         alt="<?php echo $category['name']; ?>"/>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 maincat_header">
                    <h3>
                        <?php echo $category['name']; ?>
                    </h3>
                </div>
            </a>
        </div>
        <?php } ?>
    </div>
</div>