<?php if (isset($products)): ?>
<section class="special_order">
    <div class="container">
        <h2 class="sectionheader main_sectionheader"><?php echo $heading_title; ?></h2>
        <div class="block-wrapper">
            <div class=" block_slider specialorder_slider swiper-no-swiping" id="no_slide">
                <div class="swiper-wrapper blockslider_flex ">
                    <?php foreach ($products as $product): ?>
                    <div class="swiper-slide blockslider-height proditem" data-id="<?=$product['product_id'];?>">
                        <div class="col-md-12 blockslider_vieweditems">
                            <div class="blockslider_imgwrapper">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <p class="part_manufacturer"><?php echo $product['manufacturer']; ?></p>
                            <h4 class="part_header">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <?php if ($product['price']) : ?>
                            <p class="partprice partprice_available">
                                <?php if (!$product['special']) : ?>
                                    <?php echo $product['price']; ?>
                                <?php else : ?>
                                    <span class="oldprice"><?php echo $product['price']; ?></span>
                                    <span class="current_price"><?php echo $product['special']; ?></span>
                                <?php endif; ?>
                            </p>
                            <?php endif; ?>
                            <?php if ( $product['quantity'] > 0 ) { ?>
                            <div class="quantity">
                                <input type="number" min="<?=$product['minimum'];?>" max="<?php //echo $product['quantity'];?>"
                                       step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
                            </div>
                            <button class="item_tocart pull-right" type="button"><?php echo $button_cart; ?></button>
                            <?php } else { ?>
                            <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal"
                                    data-target="#order"><?php echo $button_cart_zero; ?></button>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>