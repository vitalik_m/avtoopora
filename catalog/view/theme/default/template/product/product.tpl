<?php echo $header;?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row"><?php echo $content_top; ?></div>
</div>
<div class="container" id="product" data-id="<?=$product_id;?>">
    <div class="row">
        <?php echo $column_left; ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php // echo $class; ?>">
            <h1 class="header_singlepage"><?php echo $heading_title; ?></h1>
            <div class="product-rating">
                <?php if ($review_status) { ?>
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($rating < $i) { ?>
                <span class="star_empty"></span>
                <?php } else { ?>
                <span class="star_full"></span>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </div>
            <span class="vote_count">&mdash; <?php echo $reviews; ?></span>
        </div>
        <div id="image" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="bigimg-wrapper">
                <?php if ($thumb) { ?>
                <a class="images min-h" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>" class="img-responsive main_image" id="zoom_01"
                         data-zoom-image="<?php echo $popup; ?>"/>
                </a>
                <?php } ?>

                <?php if (isset($sticker)): ?>
                <span class="oneitem_action">Хит продаж</span>
                <?php endif; ?>

                <?php if ($special) { ?>
                <span class="oneitem_salecount">-<?php echo $saving; ?>%</span>
                <?php } ?>

                <?php if(isset($AllWishlist)) { ?>
                <a type="button" data-toggle="tooltip" class="item_infavorites" title="<?php echo $button_wishlist; ?>"
                   data-id="<?php echo $product_id; ?>"></a>
                <?php } else {  ?>
                <a type="button" data-toggle="tooltip" class="item_tofavorites" title="<?php echo $button_wishlist; ?>"
                   data-id="<?php echo $product_id; ?>"></a>
                <?php } ?>
            </div>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <a class="img-responsive smallthumbs images <?=isset($image['class'])?$image['class']: '' ?>" href="<?php echo $image['popup']; ?>"
               title="<?php echo $heading_title; ?>">

                <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>"
                     alt="<?php echo $heading_title; ?>" name="<?php echo $image['swap']; ?>"
                     id="<?php echo $image['popup']; ?>" class="thumb_image1"
                     data-zoom-image="<?php echo $image['thumb']; ?>"/>

            </a>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?php if ($price) { ?>
            <div class="col-md-12 nopadding wrapper-oldprice">
                <?php if (!$special) { ?>
                <h2 class="oneitem-detailprice"><?php echo $price; ?></h2>
                <?php } else { ?>
                <span class="oldprice"><?php echo $price; ?></span>
                <h2 class="oneitem-detailprice"><?php echo $special; ?></h2>
                <?php } ?>
            </div>

            <?php if ($tax) { ?>
            <?php echo $text_tax; ?> <?php echo $tax; ?>
            <?php } ?>
            <?php if ($points) { ?>
            <?php echo $text_points; ?> <?php echo $points; ?>
            <?php } ?>
            <?php if ($discounts) { ?>
            <hr>
            <?php foreach ($discounts as $discount) { ?>
            <?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <div class="col-md-12 nopadding singleitem_topcol">
                <?php if ( $quantity > 0 ) { ?>
                <div class="col-md-2 nopadding">
                    <div class="quantity quantity_single">
                        <input type="number" min="<?php echo $minimum; ?>" max="<?php // echo $quantity; ?>"
                               step="<?php echo $minimum; ?>"
                               value="<?php echo $minimum; ?>">
                    </div>
                </div>
                <div class="col-md-10">
                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"
                            class="oneitem_tocart"><?php echo $button_cart; ?></button>
                </div>
                <?php } else { ?>
                <div class="col-md-12 nopadding">
                    <button class="item_tocart_notavailable" type="button" data-toggle="modal"
                            data-target="#order"><?php echo $button_cart_zero; ?></button>
                </div>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
            <table class="detail_paramethers" width="100%" border="1">
                <thead>
                <span class="paramtable_head">Характеристики</span>
                </thead>
                <tbody>
                <tr>
                    <?php if (!empty($model)): ?>
                    <td>Код товара</td>
                    <td><?php echo $model; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($sku)): ?>
                    <td>Каталожный номер</td>
                    <td><?php echo $sku; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($upc)): ?>
                    <td>Заводской номер</td>
                    <td><?php echo $upc; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($ean)): ?>
                    <td>??</td>
                    <td><?php echo $ean; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($jan)): ?>
                    <td>??</td>
                    <td><?php echo $jan; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($mpn)): ?>
                    <td>??</td>
                    <td><?php echo $mpn; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($reward)): ?>
                    <td><?php echo $text_reward; ?></td>
                    <td><?php echo $reward; ?></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (!empty($manufacturer)): ?>
                    <td><?php echo $text_manufacturer; ?></td>
                    <td>
                        <a href="<?php echo $manufacturers; ?>">
                            <?php echo $manufacturer; ?>
                        </a>
                    </td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <td><?php echo $text_stock; ?></td>
                    <td><?php echo $stock; ?></
                    </td>
                </tr>
                <tr>
                    <td>Краткое описание</td>
                    <td><?php echo substr(strip_tags($description), 0, 50 ) . "..."; ?></td>
                </tr>
                </tbody>
            </table>
            <?php if ( isset($reviews_new) ) { ?>
                <?php foreach ( $reviews_new as $review_new ) { ?>
                    <?php if ( $review_new['customer_id'] == $customer_id ) { ?>
                        <div class="radio-toolbar">
                            <style>.nomargin{margin:auto}</style>
                            <div class="product-rating nomargin">
                                <?php if ( $review_new['rating'] ) { ?>
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ( $review_new['rating'] < $i ) { ?>
                                <span class="star_empty"></span>
                                <?php } else { ?>
                                <span class="star_full"></span>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <span class="vote_count_click_added">&mdash; ваш голос</span>
                        </div>
                        <?php $added_review = 1; ?>
                    <?php } elseif ( ( $review_new['customer_id'] != $customer_id ) && ( $review_new['customer_id'] != 0 ) ) { ?>
                        <div class="radio-toolbar">
                            <input type="radio" id="one_star" name="rating" value="1">
                            <label for="one_star" class="tablinks"></label>
                            <input type="radio" id="two_star" name="rating" value="2">
                            <label for="two_star" class="tablinks"></label>
                            <input type="radio" id="three_star" name="rating" value="3">
                            <label for="three_star"></label>
                            <input type="radio" id="four_star" name="rating" value="4">
                            <label for="four_star"></label>
                            <input type="radio" id="five_star" name="rating" value="5">
                            <label for="five_star"></label>
                        </div>
                        <span class="vote_count_click">&mdash; вы еще не голосовали</span>
                    <?php } ?>
                <?php } ?>
            <?php } elseif ( !isset( $reviews_new ) ) { ?>
            <div class="radio-toolbar">
                <input type="radio" id="one_star" name="rating" value="1">
                <label for="one_star" class="tablinks"></label>
                <input type="radio" id="two_star" name="rating" value="2">
                <label for="two_star" class="tablinks"></label>
                <input type="radio" id="three_star" name="rating" value="3">
                <label for="three_star"></label>
                <input type="radio" id="four_star" name="rating" value="4">
                <label for="four_star"></label>
                <input type="radio" id="five_star" name="rating" value="5">
                <label for="five_star"></label>
            </div>
            <span class="vote_count_click">&mdash; вы еще не голосовали</span>
            <?php } ?>
        </div>
    </div>
</div>
<?php if ($options) { ?>
<hr>
<h3><?php echo $text_option; ?></h3>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'select') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <select name="option[<?php echo $option['product_option_id']; ?>]"
            id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
        </option>
        <?php } ?>
    </select>
</div>
<?php } ?>
<?php if ($option['type'] == 'radio') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"><?php echo $option['name']; ?></label>
    <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
            <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"
                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
            </label>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php if ($option['type'] == 'checkbox') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"><?php echo $option['name']; ?></label>
    <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]"
                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
            </label>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php if ($option['type'] == 'image') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"><?php echo $option['name']; ?></label>
    <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
            <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"
                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                <img src="<?php echo $option_value['image']; ?>"
                     alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                     class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
            </label>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php if ($option['type'] == 'text') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
           value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
           id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
</div>
<?php } ?>
<?php if ($option['type'] == 'textarea') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
              placeholder="<?php echo $option['name']; ?>"
              id="input-option<?php echo $option['product_option_id']; ?>"
              class="form-control"><?php echo $option['value']; ?></textarea>
</div>
<?php } ?>
<?php if ($option['type'] == 'file') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"><?php echo $option['name']; ?></label>
    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>"
            data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i
                class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value=""
           id="input-option<?php echo $option['product_option_id']; ?>"/>
</div>
<?php } ?>
<?php if ($option['type'] == 'date') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <div class="input-group date">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
               value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
        <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
</div>
<?php } ?>
<?php if ($option['type'] == 'datetime') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <div class="input-group datetime">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
               value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
</div>
<?php } ?>
<?php if ($option['type'] == 'time') { ?>
<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
    <label class="control-label"
           for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
    <div class="input-group time">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
               value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
</div>
<?php } ?>
<?php } ?>
<?php } ?>
<?php if ($recurrings) { ?>
<hr>
<h3><?php echo $text_payment_recurring ?></h3>
<div class="form-group required">
    <select name="recurring_id" class="form-control">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($recurrings as $recurring) { ?>
        <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
        <?php } ?>
    </select>
    <div class="help-block" id="recurring-description"></div>
</div>
<?php } ?>
<?php if ($minimum > 1) { ?>
<div class="alert alert-info">
    <i class="fa fa-info-circle"></i>
    <?php echo $text_minimum; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="container">
    <ul class="nav nav-tabs">
        <?php if ($products): ?>
        <li class="active">
            <a href="#tab-transition" data-toggle="tab">
                <?php echo $text_related; ?>
            </a>
        </li>
        <?php endif; ?>
        <?php if (!$products): ?>
        <li class="active">
            <a href="#tab-description" data-toggle="tab">
                <?php echo $tab_description; ?>
            </a>
        </li>
        <?php else: ?>
        <li>
            <a href="#tab-description" data-toggle="tab">
                <?php echo $tab_description; ?>
            </a>
        </li>
        <?php endif; ?>
        <?php /* if ($attribute_groups) { ?>
        <li>
            <a href="#tab-specification" data-toggle="tab">
                <?php echo $tab_attribute; ?>
            </a>
        </li>
        <?php } */ ?>
        <?php foreach($product_tabs as $key => $tab){ ?>
        <li>
            <a href="#tab-<?php echo $product_id ?>-<?php echo $tab['product_tab_id']; ?>"
               data-toggle="tab">
                <?php echo $tab['title']; ?>
            </a>
        </li>
        <?php } ?>
        <?php if ($review_status) { ?>
        <li>
            <a href="#tab-review" data-toggle="tab">
                <?php echo $tab_review; ?>
            </a>
        </li>
        <?php } ?>
    </ul>
    <div class="tab-content">
        <?php if ($products): ?>
        <div id="tab-transition" class="tab-pane fade in active">
            <?php foreach ($products as $product): ?>
            <div class="col-md-12 relatedproducts" data-id="<?=$product['product_id'];?>">
                <div class="col-md-2">
                    <a href="<?=$product['href']; ?>">
                        <img src="<?=$product['thumb']; ?>" alt="<?=$product['name']; ?>"
                             title="<?=$product['name']; ?>" class="img-responsive relatedproducts_img"/>
                    </a>
                </div>
                <div class="col-md-5">
                    <p class="relatedproducts_manufacturer"><?php echo $product['manufacturer']; ?></p>
                    <h3 class="relatedproducts_header"><?=$product['name']; ?></h3>
                    <p class="item_partnumber">
                        <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                    </p>
                    <p class="item_partnumber">
                        <?php //if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                    </p>
                    <!--<p class="item_partnumber">
                        <?php /*if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif; */?>
                    </p>-->
                    <?php if ($product['rating']): ?>
                    <div class="product-rating">
                        <?php for ($i = 1; $i <= 5; $i++) : ?>
                        <?php if ($product['rating'] < $i): ?>
                        <span class="star_empty"></span>
                        <?php else: ?>
                        <span class="star_full"></span>
                        <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-5 proditem_col">
                    <div class="actionbadge_wrapper">
                        <?php if ($product['special']) { ?>
                        <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                        <?php } ?>
                        <?php if (!empty($product['sticker'])): ?>
                        <span class="oneitem_action">Хит продаж</span>
                        <?php endif; ?>
                        <?php if($product['inWish'] == true) { ?>
                        <a class="item_infavorites" type="button" data-toggle="tooltip"
                           title="<?php echo $button_wishlist; ?>"
                           data-id="<?php echo $product['product_id']; ?>"></a>
                        <?php } else { ?>
                        <a class="item_tofavorites" type="button" data-toggle="tooltip"
                           title="<?php echo $button_wishlist; ?>"
                           data-id="<?php echo $product['product_id']; ?>"></a>
                        <?php } ?>
                    </div>
                    <?php if ($product['price']) { ?>
                    <?php if (!$product['special']): ?>
                    <span class="relatedproducts_itemprice"><?=$product['price']; ?></span>
                    <?php else: ?>
                    <p class="partprice partprice_notavailable">
                        <span class="oldprice"><?=$product['price']; ?></span>
                        <span class="current_price"><?=$product['special']; ?></span>
                    </p>
                    <?php endif; ?>
                    <?php if ($product['tax']): ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php endif; ?>
                    <?php } ?>
                    <?php if ( $product['quantity'] > 0 ) { ?>
                    <div class="quantity quantity_single">
                        <input type="number" min="<?php echo $product['minimum']; ?>"
                               max="<?php // echo $product['quantity']; ?>" step="<?php echo $product['minimum']; ?>"
                               value="<?php echo $product['minimum']; ?>">
                    </div>
                    <button type="button" class="relatedproducts_tocart">
                            <?php echo $button_cart; ?>
                    </button>
                    <?php } else { ?>
                    <button class="item_tocart_notavailable recomented_product" type="button" data-toggle="modal"
                            data-target="#order"><?php echo $button_cart_zero; ?></button>
                    <?php } ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane fade oneitem_tabdescription" id="tab-description">
            <?php else: ?>
            <div class="tab-pane fade in active oneitem_tabdescription" id="tab-description">
                <?php endif; ?>
                <h3 class="oneitem_tabdescription_header"><?php echo $heading_title;?></h3>
                <?php echo $description; ?>
            </div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane fade oneitem_tabdescription" id="tab-specification">
                <table class="table table-bordered">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <thead>
                    <tr>
                        <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                    <tr>
                        <td><?php echo $attribute['name']; ?></td>
                        <td><?php echo $attribute['text']; ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
            <?php } ?>
            <?php foreach($product_tabs as $key => $tab){ ?>
            <div class="tab-pane fade oneitem_tabdescription"
                 id="tab-<?php echo $product_id ?>-<?php echo $tab['product_tab_id']; ?>">
                <?php echo $tab['description']; ?>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane fade item_feedback" id="tab-review">
                <form class="form-horizontal" id="form-review">
                    <div id="review"></div>
                    <?php if ( !isset($added_review) ){ ?>
                    <?php if (!$review_guest) { ?>
                    <div class="form-group required" class="comment_form">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-name"><?php // echo $entry_name; ?></label>
                            <input type="text" name="name" value="" id="input-name" class="form-control"
                                   placeholder="<?php echo $entry_name; ?>"/>
                        </div>
                    </div>
                    <?php } else { ?>
                    <input type="hidden" name="name" value="<?php echo $customer_name; ?>" id="input-name"
                           class="form-control"
                           placeholder="<?php echo $entry_name; ?>"/>
                    <?php } ?>
                    <div class="comment_form">
                        <label class="control-label" for="comment_text"><?php // echo $entry_review; ?></label>
                        <textarea name="text" id="comment_text" placeholder="Оставьте ваш отзыв о товаре"></textarea>
                    </div>
                    <?php echo $captcha?$captcha:''; ?>
                    <button type="button" id="comment_submit"
                            data-loading-text="<?php echo $text_loading; ?>"
                            class="btn_checkout btn_profileenter">
                        <?php echo $text_write; ?>
                    </button>
                    <?php } ?>
                </form>
            </div>
            <?php } ?>
        </div>
        <?php /* if ($tags) { ?>
        <p><?php echo $text_tags; ?>
            <?php for ($i = 0; $i < count($tags); $i++) { ?>
            <?php if ($i < (count($tags) - 1)) { ?>
            <a class="savecart" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
            <?php } else { ?>
            <a class="savecart" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
            <?php } ?>
            <?php } ?>
        </p>
        <?php } */ ?>
        <?php echo $column_right; ?>
    </div>
    <section class="viewed_item">
        <div class="container">
            <?php echo $content_bottom; ?>
        </div>
    </section>
    <script type="text/javascript"><!--
        document.addEventListener("DOMContentLoaded", function (event) {
            $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
                $.ajax({
                    url: 'index.php?route=product/product/getRecurringDescription',
                    type: 'post',
                    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                    dataType: 'json',
                    beforeSend: function () {
                        $('#recurring-description').html('');
                    },
                    success: function (json) {
                        $('.alert, .text-danger').remove();

                        if (json['success']) {
                            $('#recurring-description').html(json['success']);
                        }
                    }
                });
            });

            $('.date').datetimepicker({
                pickTime: false
            });

            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });

            $('.time').datetimepicker({
                pickDate: false
            });

            $('button[id^=\'button-upload\']').on('click', function () {
                var node = this;

                $('#form-upload').remove();

                $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

                $('#form-upload input[name=\'file\']').trigger('click');

                if (typeof timer != 'undefined') {
                    clearInterval(timer);
                }

                timer = setInterval(function () {
                    if ($('#form-upload input[name=\'file\']').val() != '') {
                        clearInterval(timer);

                        $.ajax({
                            url: 'index.php?route=tool/upload',
                            type: 'post',
                            dataType: 'json',
                            data: new FormData($('#form-upload')[0]),
                            cache: false,
                            contentType: false,
                            processData: false,
                            beforeSend: function () {
                                $(node).button('loading');
                            },
                            complete: function () {
                                $(node).button('reset');
                            },
                            success: function (json) {
                                $('.text-danger').remove();

                                if (json['error']) {
                                    $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                }

                                if (json['success']) {
                                    alert(json['success']);

                                    $(node).parent().find('input').attr('value', json['code']);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                }, 500);
            });

            $('#review').delegate('.pagination a', 'click', function (e) {
                e.preventDefault();

                $('#review').fadeOut('slow');

                $('#review').load(this.href);

                $('#review').fadeIn('slow');
            });

            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

            $('#comment_submit').on('click', function () {
                $.ajax({
                    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
                    type: 'post',
                    dataType: 'json',
                    data: $('#form-review, .radio-toolbar input[type="radio"]:checked').serialize(),
                    beforeSend: function () {
                        $('#button-review').button('loading');
                    },
                    complete: function () {
                        $('#button-review').button('reset');
                    },
                    success: function (json) {
                        $('.alert-success, .alert-danger').remove();
                        if (json['error']) {
                            $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                        if (json['success']) {
                            $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $('input[name=\'name\']').val('');
                            $('textarea[name=\'text\']').val('');
                            $('input[name=\'rating\']:checked').prop('checked', false);
                        }
                    },
                    error: function () {
                        console.log('error', arguments);
                    }
                });
            });


            $('.radio-toolbar').on('click', function () {
                $('a[href="#tab-review"]').click();
            });

            $(document).ready(function () {
                $('.bigimg-wrapper').magnificPopup({
                    type: 'image',
                    delegate: '.images',
                    gallery: {
                        enabled: true
                    }
                });
            });
            $(document).ready(function () {
                var isMobile = {
                    Android: function () {
                        return navigator.userAgent.match(/Android/i);
                    },
                    BlackBerry: function () {
                        return navigator.userAgent.match(/BlackBerry/i);
                    },
                    iOS: function () {
                        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                    },
                    Opera: function () {
                        return navigator.userAgent.match(/Opera Mini/i);
                    },
                    Windows: function () {
                        return navigator.userAgent.match(/IEMobile/i);
                    },
                    any: function () {
                        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                    }
                };


                if (isMobile.any()) {

                    $(".zoomContainer").remove();
                    $(".your-zoom").removeData("elevateZoom");
                    $('#zoom_01').removeData("elevateZoom");

                } else {
                    $("#zoom_01").elevateZoom({tint:true, tintColour:'#eef2f5', tintOpacity:1.5});
                    $('.thumb_image1').click(function (d) {
                        d.preventDefault();
                        $('#image > a').removeClass('active');
                        $(this).parent().addClass('active');
                        small_image = $(this).attr('name');
                        large_image = $(this).attr('id');
                        $('.main_image').attr('src', small_image);
                        $('#zoom_01').attr('src', small_image);
                        $('#zoom_01').attr('data-zoom-image', large_image);
                        $('.bigimg-wrapper .min-h').attr('href', large_image);
                        smallImage = small_image;
                        largeImage = large_image;
                        var ez = $('#zoom_01').data('elevateZoom');
                        ez.swaptheimage(smallImage, largeImage);
                    });
                }
            });
        });
        //--></script>
    <?php echo $footer; ?>
