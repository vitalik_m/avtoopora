<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<section class="item_subcat">
    <div class="container">
        <div class="row">
            <?php echo $content_top; /* ?>
            <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
            <?php */ if ($categories) { /*?>
            <h3><?php echo $text_refine; ?></h3>
            <?php */ if (count($categories) <= 5) { ?>
            <div class="row">
                <?php foreach ($categories as $category) { ?>
                <div class="col-md-2">
                    <div class="carsearch_bordered">
                        <a href="<?php echo $category['href']; ?>">
                            <?php if ( isset($category['thumb']) ) { ?>
                            <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"
                                 class="center-block img-responsive">
                            <?php } else { ?>
                            <img src="image/cache/no_image.png" alt="<?php echo $category['name']; ?>"
                                 class="center-block img-responsive">
                            <?php } ?>
                            <span class="carmark_descr"><?php echo $category['name']; ?></span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php } /*  else { ?>
            <div class="row">
                <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                <div class="col-sm-3">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <?php } */ ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php if ($products) { ?>
<section>
    <div id="content" class="container">
        <h2 class="sectionheader sectionheader-cat"><?php echo $heading_title; ?></h2>
        <div class="row">
            <div class="col-md-5 ocitems-leftside">
                <p class="search_ocitems pull-left">Найдено <?php echo $count; ?> товара в категории</p>
            </div>
            <!--<p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
            <div class="col-md-7 col-xs-12 ocitems-rightside">
                <div class="group_custom">
                    <div class="btn-group btn-group-sm pull-right grid_switch">
                        <button type="button" id="grid-view" class="btn btn-default grid-active" data-toggle="tooltip"
                                title="<?php echo $button_grid; ?>" data-original-title="Сетка"></button>
                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip"
                                title="<?php echo $button_list; ?>" data-original-title="Список"></button>
                    </div>
                    <div class="select_arr select_pricesort">
                        <select id="input-sort" class="form-control select_styled" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>"
                                    selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row"><div class="col-md-1 text-right"><label class="control-label" for="input-limit"><?php echo $text_limit; ?></label></div><div class="col-md-2 text-right"><select id="input-limit" class="form-control" onchange="location = this.value;"><?php foreach ($limits as $limits) { ?><?php if ($limits['value'] == $limit) { ?><option value="<?php echo $limits['href']; ?>"selected="selected"><?php echo $limits['text']; ?></option><?php } else { ?><option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option><?php } ?><?php } ?></select></div></div><br/> -->
        <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="product-layout product-grid col-lg-3 col-md-4 col-sm-6 col-xs-12"
                 data-id="<?php echo $product['product_id']; ?>">
                <div class="blockslider_bordered">
                    <div class="blockgrid_badges">
                        <?php if ($product['special']) { ?>
                        <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                        <?php } ?>
                        <?php if (!empty($product['sticker'])): ?>
                        <span class="oneitem_action">Хит продаж</span>
                        <?php endif; ?>
                        <?php if($product['inWish'] == true) { ?>
                        <a class="item_infavorites" type="button" data-toggle="tooltip"
                           title="<?php echo $button_wishlist; ?>" data-id="<?php echo $product['product_id']; ?>"></a>
                        <?php } else { ?>
                        <a class="item_tofavorites" type="button" data-toggle="tooltip"
                           title="<?php echo $button_wishlist; ?>"
                           data-id="<?php echo $product['product_id']; ?>"></a>
                        <?php } ?>
                    </div>
                    <div class="blockslider_imgwrapper">
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                 title="<?php echo $product['name']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="product_layout_listheader">
                        <p class="part_manufacturer"><?php echo $product['manufacturer']; ?></p>
                        <h4 class="part_header">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        </h4>
                    </div>
                    <div class="product_layout_listblock">
                        <p class="item_partnumber">
                            <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                        </p>
                        <p class="item_partnumber">
                            <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
                        </p>
                    </div>
                    <?php if ($product['rating']): //проверка на наличие рейтинга. Уже стилизовано. ?>
                    <div class="starcont">
                        <div class="product-rating">
                            <?php for ($i = 1; $i <= 5; $i++) : ?>
                            <?php if ($product['rating'] < $i): ?>
                            <span class="star_empty"></span>
                            <?php else: ?>
                            <span class="star_full"></span>
                            <?php endif; ?>
                            <?php endfor; ?>
                        </div>
                    </div>

                    <?php endif;  ?>
                    <div class="product_layout_listtocart">
                        <?php if ($product['price']) { ?>
                        <?php if (!$product['special']) { ?>
                        <p class="partprice"><?php echo $product['price']; ?></p>
                        <?php } else { ?>
                        <p class="partprice partprice_notavailable">
                            <span class="oldprice"><?php echo $product['price']; ?></span>
                            <span class="current_price"><?php echo $product['special']; ?></span>
                        </p>
                        <?php } ?>
                        <?php /* if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                        <?php } */ ?>
                        <?php } ?>
                        <?php if ( $product['quantity'] > 0 ) { ?>
                        <div class="quantity">
                            <input type="number" min="<?=$product['minimum'];?>" max="<?=$product['quantity'];?>"
                                   step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
                        </div>
                        <button class="item_tocart pull-right" type="button"><?php echo $button_cart; ?></button>
                        <?php } else { ?>
                        <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal" data-target="#order"><?php echo $button_cart_zero; ?></button>
                        <?php } ?>
                    </div>
                </div>
                <?php /* if need added compare ?>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                        onclick="compare.add('<?php echo $product['product_id']; ?>');">
                    <i class="fa fa-exchange"></i>
                </button>
                <?php */ ?>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<div class="pagination_wrapper">
    <div class="text-center">
        <?php echo $pagination; ?>
        <!--<div class="col-sm-6 text-right"><?php echo $results; ?></div>-->
    </div>
</div>
</div>
<?php } ?>
<?php echo $column_right; ?>
<?php if (!$categories && !$products) { ?>
<p><?php echo $text_empty; ?></p>
<div class="buttons">
    <div class="pull-right">
        <a href="<?php echo $continue; ?>" class="btn_checkout btn_profileenter">
            <?php echo $button_continue; ?>
        </a>
    </div>
</div>
<?php } ?>
<?php if ($thumb && $description) { ?>
<section class="grids_textblock">
    <div class="container">
        <div class="row">
            <div class="col-md-9  gridtest-a"><?php echo $description; ?></div>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo $thumb_inner; ?>" alt="<?php echo $heading_title; ?>"
                     title="<?php echo $heading_title; ?>" class="gridsbanner_testclass img-responsive"/>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<div class="container">
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
