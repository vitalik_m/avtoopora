<?php echo $header; ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ( isset($breadcrumb['href']) ) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
      <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
<section class="item_subcat">
  <div class="container">
    <div class="row">
      <?php echo $content_top; ?>
    </div>
  </div>
</section>
<?php if ($products) { ?>
<section>
  <div id="content" class="container">
    <h2 class="sectionheader"><?php echo $manufacturer_name; ?></h2>
    <div class="row">
      <div class="col-md-5">
        <p class="search_ocitems pull-left">Найдено <?php echo $count; ?> товара в категории</p>
      </div>
      <div class="col-md-7 col-xs-6">
        <div class="group_custom">
          <div class="btn-group btn-group-sm pull-right grid_switch">
            <button type="button" id="grid-view" class="btn btn-default grid-active" data-toggle="tooltip"
                    title="<?php echo $button_grid; ?>" data-original-title="Сетка"></button>
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip"
                    title="<?php echo $button_list; ?>" data-original-title="Список"></button>
          </div>
          <div class="select_arr select_pricesort manufacturer">
            <select id="input-sort" class="form-control select_styled" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>"
                      selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <?php foreach ($products as $product) { ?>
      <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"
           data-id="<?php echo $product['product_id']; ?>">
        <div class="blockslider_bordered">
          <?php if($product['inWish'] == true) { ?>
          <a class="item_infavorites" type="button" data-toggle="tooltip"
             title="<?php echo $button_wishlist; ?>" data-id="<?php echo $product['product_id']; ?>"></a>
          <?php } else { ?>
          <a class="item_tofavorites" type="button" data-toggle="tooltip"
             title="<?php echo $button_wishlist; ?>"
             data-id="<?php echo $product['product_id']; ?>"></a>
          <?php } ?>
          <div class="blockslider_imgwrapper">
            <div class="blockgrid_badges">
              <?php if (!empty($product['sticker'])): ?>
              <span class="oneitem_action">Хит продаж</span>
              <?php endif; ?>
              <?php if ($product['special']) { ?>
              <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
              <?php } ?>
            </div>
            <a href="<?php echo $product['href']; ?>">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                   title="<?php echo $product['name']; ?>" class="img-responsive"/>
            </a>
          </div>
          <div class="product_layout_listheader">
            <h4 class="part_header">
              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            </h4>
          </div>
          <div class="product_layout_listblock">
            <p class="item_partnumber">
              <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
            </p>
            <p class="item_partnumber">
              <?php //if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
            </p>
            <p class="item_partnumber">
              <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
            </p>
          </div>
          <?php /* if ($product['rating']): //проверка на наличие рейтинга. Уже стилизовано. ?>
          <div class="product-rating">
            <?php for ($i = 1; $i <= 5; $i++) : ?>
            <?php if ($product['rating'] < $i): ?>
            <span class="star_empty"></span>
            <?php else: ?>
            <span class="star_full"></span>
            <?php endif; ?>
            <?php endfor; ?>
          </div>
          <?php endif; */ ?>
          <div class="product_layout_listtocart">
            <?php if ($product['price']) { ?>
            <?php if (!$product['special']) { ?>
            <p class="partprice"><?php echo $product['price']; ?></p>
            <?php } else { ?>
            <p class="partprice partprice_notavailable">
              <span class="oldprice"><?php echo $product['price']; ?></span>
              <span class="current_price"><?php echo $product['special']; ?></span>
            </p>
            <?php } ?>
            <?php } ?>
            <?php if ( $product['quantity'] > 0 ) { ?>
            <div class="quantity">
              <input type="number" min="<?=$product['minimum'];?>" max="<?=$product['quantity'];?>"
                     step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
            </div>
            <button class="item_tocart pull-right" type="button"><?php echo $button_cart; ?></button>
            <?php } else { ?>
            <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal" data-target="#order"><?php echo $button_cart_zero; ?></button>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<div class="pagination_wrapper">
  <div class="text-center">
    <?php echo $pagination; ?>
  </div>
</div>
<?php } else { ?>
<p><?php echo $text_empty; ?></p>
<div class="buttons">
  <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
</div>
<?php } ?>
<?php echo $column_right; ?>
<?php if ($thumb && $description) { ?>
<section class="grids_textblock">
  <div class="container">
    <div class="row">
      <div class="col-md-9"><?php echo $description; ?></div>
      <div class="col-md-3">
        <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"
             title="<?php echo $heading_title; ?>" class="gridsbanner_testclass"/>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<div class="container">
  <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>