<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>">
                    <?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li>
                <?php echo $breadcrumb['text']; ?>
            </li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<section class="item_subcat">
    <div class="container">
        <div class="row item_subcat-wrap ">
            <?php echo $content_top; /* ?>
            <h2 class="sectionheader">
                <?php echo $heading_title; ?>
            </h2>
            <?php */ if ($categories) { /*?>
            <h3>
                <?php echo $text_refine; ?>
            </h3>
            <?php */ if (count($categories) > 0) { ?>

            <?php foreach ($categories as $category) { ?>
            <div class="">
                <div class="carsearch_bordered">
                    <a href="<?php echo $category['href']; ?>">
                        <?php if ( isset($category['thumb']) ) { ?>
                        <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" class="center-block img-responsive">
                        <?php } else {
                                if(file_exists(DIR_IMAGE_PRODUCTS . 'no_image.png')) {
                                    $noImg = DIR_IMAGE_PRODUCTS . 'no_image.png';
                                }else{
                                    $noImg = 'image/' . 'no_image.png';
                                }
                                ?>
                        <img src="<?= $noImg?>" alt="<?php echo $category['name']; ?>" class="center-block img-responsive">
                        <?php } ?>
                        <span class="carmark_descr">
                            <?php echo $category['name']; ?></span>
                    </a>
                </div>
            </div>
            <?php } ?>

            <?php } /*  else { ?>
            <div class="row">
                <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                <div class="col-sm-3">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        <li><a href="<?php echo $category['href']; ?>">
                                <?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <?php } */ ?>
            <?php } ?>
        </div>
    </div>
</section>

<!--<section>
    <div class="container">
        <div class="filters-top">
            <div class="select_arr select_pricesort pull-left"> <select class="form-control select_styled"    name='marka' onchange="location = this.value;"><?php echo $marka_list; ?></select></div>
            <div class="select_arr select_pricesort pull-left" ><select class="form-control select_styled"   name='uzel' onchange="location = this.value;"><?php echo $uzel_list; ?></select></div>
        </div>-->


<!-- <p class="paramtext-empty"><?php //echo $text_empty; ?></p>
         <div class="buttons">
                 <a href="<?php //echo $continue; ?>" class="btn_checkout btn_profileenter">
                     <?php // echo $button_continue; ?>
                 </a>
         </div>-->
<!--</div>
  </section>-->


<section>
    <div id="content" class="container">
        <h2 class="sectionheader sectionheader-cat">
            <?php echo $heading_title; ?>
        </h2>
        <div class="row">
            <div class="col-md-5 ocitems-leftside">
                <?php if(empty($filter)):?>
                <p class="search_ocitems pull-left">Найдено <?php echo $count; ?> товара в категории</p>
                <?php else:?>
                <p class="search_ocitems pull-left">Подобрано <?= $product_total; ?> из <?= $count; ?> товаров</p>
                <?php endif;?>
            </div>
            <!--<p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
            <div class="col-md-7 col-xs-12 ocitems-rightside">

                <div class="group_custom">

                    <div class="btn-group btn-group-sm pull-right grid_switch">
                        <button type="button" id="grid-view" class="btn btn-default grid-active" data-toggle="tooltip"
                            title="<?php echo $button_grid; ?>" data-original-title="Сетка"></button>
                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"
                            data-original-title="Список"></button>
                    </div>

                    <div class="select_arr select_pricesort">

                        <select id="input-sort" class="form-control select_styled" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>" selected="selected">
                                <?php echo $sorts['text']; ?>
                            </option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>">
                                <?php echo $sorts['text']; ?>
                            </option>
                            <?php } ?>
                            <?php } ?>
                        </select>

                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row"><div class="col-md-1 text-right"><label class="control-label" for="input-limit"><?php echo $text_limit; ?></label></div><div class="col-md-2 text-right"><select id="input-limit" class="form-control" onchange="location = this.value;"><?php foreach ($limits as $limits) { ?><?php if ($limits['value'] == $limit) { ?><option value="<?php echo $limits['href']; ?>"selected="selected"><?php echo $limits['text']; ?></option><?php } else { ?><option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option><?php } ?><?php } ?></select></div></div><br/> -->
        <div class='row'>

            <!-- filters-->
            <?php if(!empty($data_for_filter)):?>
            <div class='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
                <form action="<?= $self_link?>" name="filter" id="filter_form" method="get">
                    <input type="hidden" id="filters_ids" name="filter" value="">
                    <?= !empty($_GET['route'])? '<input type="hidden" id="get_route" name="route" value="'.$_GET['route'].'">': '';?>
                    <?= !empty($_GET['path'])? '<input type="hidden" id="get_path" name="path" value="'.$_GET['path'].'">': '';?>

                    <?php foreach($data_for_filter as $filterGroup_name => $filterGroup_arr):?>
                    <div class="panel-group" id="accordion_filter_<?= $filterGroup_arr['group_id']?>" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a  class='collapsed' role="button" data-toggle="collapse" href="#collapse<?= '_'. $filterGroup_arr['group_id'] ?>" aria-expanded="true"
                                        aria-controls="collapse<?= '_'. $filterGroup_arr['group_id'] ?>">
                                        <?= $filterGroup_name?><div class='filter-plus'></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?= '_'. $filterGroup_arr['group_id'] ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php foreach($filterGroup_arr['filters'] as $filter_id => $filter_name):?>
                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox"
                                                       value="<?= $filter_id ?>"
                                                       id="check<?= '_'. $filterGroup_arr['group_id'] .'_'. $filter_id ?>"
                                                       class="filter_checkbox"
                                                       <?= !is_array($filter_name) || !$filter_name[1] ? '' : 'checked';  ?>>
                                                <label for='check<?= '_'. $filterGroup_arr['group_id'] .'_'. $filter_id ?>'>
                                                    <?= !is_array($filter_name) ? $filter_name : $filter_name[0] ?>
                                                </label>
                                            </div>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </form>
                <div class='filter-link'>
                    <a href="<?= $self_link ?>" class="enter_link">Сбросить фильтр</a>
                </div>             
            </div>
            <?php endif;?>
            <!-- END filters-->

            <!-- products list-->
            <?php if(!empty($data_for_filter)):?>
            <div class='col-lg-9 col-md-9 col-sm-12 col-xs-12'>
            <?php else:?>
            <div class='col-xs-12'>
            <?php endif;?>
                <?php if ($products): ?>
                <div class="row">
                    <?php foreach ($products as $product) { ?>

                    <div class="product-layout product-grid <?= !empty($data_for_filter)? 'col-lg-4 col-md-6 col-sm-12': 'col-lg-3 col-md-4 col-sm-6' ;?> col-xs-12"
                         data-id="<?php echo $product['product_id']; ?>">



                        <div class="blockslider_bordered">
                            <div class="blockgrid_badges">
                                <?php if ($product['special']) { ?>
                                <span class="item_tosale">-
                                    <?php echo $product['saving']; ?>%</span>
                                <?php } ?>
                                <?php if (!empty($product['sticker'])): ?>
                                <span class="oneitem_action">Хит продаж</span>
                                <?php endif; ?>
                                <?php if($product['inWish'] == true) { ?>
                                <a class="item_infavorites" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>"
                                    data-id="<?php echo $product['product_id']; ?>"></a>
                                <?php } else { ?>
                                <a class="item_tofavorites" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>"
                                    data-id="<?php echo $product['product_id']; ?>"></a>
                                <?php } ?>
                            </div>
                            <a href="<?php echo $product['href']; ?>">
                                <div class="blockslider_imgwrapper">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                        title="<?php echo $product['name']; ?>" class="img-responsive" />
                                </div>
                            </a>
                                <div class="product_layout_listheader">
                                    <p class="part_manufacturer">
                                        <?php echo $product['manufacturer']; ?>
                                    </p>
                                    <h4 class="part_header">
                                       <a href="<?php echo $product['href']; ?>"> <?php echo $product['name']; ?></a>
                                    </h4>
                                </div>
                                <div class="product_layout_listblock">
                                    <p class="item_partnumber">
                                        <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                                    </p>
                                    <p class="item_partnumber">
                                        <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                                    </p>
<!--                                    <p class="item_partnumber">-->
<!--                                        --><?php //if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
<!--                                    </p>-->
                                </div>
                                <?php if ($product['rating']): //проверка на наличие рейтинга. Уже стилизовано. ?>
                                <div class="starcont">
                                    <div class="product-rating">
                                        <?php for ($i = 1; $i <= 5; $i++) : ?>
                                        <?php if ($product['rating'] < $i): ?>
                                        <span class="star_empty"></span>
                                        <?php else: ?>
                                        <span class="star_full"></span>
                                        <?php endif; ?>
                                        <?php endfor; ?>
                                    </div>
                                </div>

                                <?php endif;  ?>
                                <div class="product_layout_listtocart">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <p class="partprice">
                                        <?php echo $product['price']; ?>
                                    </p>
                                    <?php } else { ?>
                                    <p class="partprice partprice_notavailable">
                                        <span class="oldprice">
                                            <?php echo $product['price']; ?></span>
                                        <span class="current_price">
                                            <?php echo $product['special']; ?></span>
                                    </p>
                                    <?php } ?>
                                    <?php /* if ($product['tax']) { ?>
                                    <span class="price-tax">
                                        <?php echo $text_tax; ?>
                                        <?php echo $product['tax']; ?></span>
                                    <?php } */ ?>
                                    <?php } ?>
                                    <?php if ( $product['quantity'] > 0 ) { ?>
                                    <div class="quantity">
                                        <input type="number" min="<?=$product['minimum'];?>" max="<?=$product['quantity'];?>"
                                            step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
                                    </div>
                                    <button class="item_tocart pull-right" type="button">
                                        <?php echo $button_cart; ?></button>
                                    <?php } else { ?>
                                    <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal"
                                        data-target="#order">
                                        <?php echo $button_cart_zero; ?></button>
                                    <?php } ?>
                                </div>
                 
                        </div>
                        <?php /* if need added compare ?>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                            <i class="fa fa-exchange"></i>
                        </button>
                        <?php */ ?>
                    </div>
                    <?php } ?>
                </div>
                <?php else: ?>
                <div class="row">
                    <p class="search_ocitems pull-left">Товары не найдены</p>
                </div>
                <?php endif; ?>
            </div>
            <!-- END products list-->

        </div>

    </div>
</section>
<div class="pagination_wrapper">
    <div class="text-center">
        <?php echo $pagination; ?>
        <!--<div class="col-sm-6 text-right"><?php echo $results; ?></div>-->
    </div>
</div>
</div>


<?php echo $column_right; ?>

<?php if (!$categories && !$products) { ?>

<?php } ?>
<?php if ($thumb && $description) { ?>
<section class="grids_textblock">
    <div class="container">
        <div class="row">
            <div class="col-md-9  gridtest-a">
                <?php echo $description; ?>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo $thumb_inner; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"
                    class="gridsbanner_testclass img-responsive" />
            </div>
        </div>
    </div>
</section>
<?php } ?>
<div class="container">
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>