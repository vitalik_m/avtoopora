<?php echo $header; ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ( isset($breadcrumb['href']) ) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
      <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>

<section class="searchpage">
  <div class="container">
    <?php echo $column_left; ?>
    <div id="content">
      <?php echo $content_top; ?>
      <?php echo $heading_title; ?>
      <?php if (isset($product_total) && $product_total != '0 результата поиска'): ?>
      <div class="row">
        <div class="col-md-5">
          <p class="search_ocitems pull-left"><?=$product_total; ?></p>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <?php if ($products): ?>
        <?php foreach ($products as $product): ?>
        <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"
             data-id="<?php echo $product['product_id']; ?>">
          <div class="blockslider_bordered">
            <?php if($product['inWish'] == true) { ?>
            <a class="item_infavorites" type="button" data-toggle="tooltip"
               title="<?php echo $button_wishlist; ?>" data-id="<?php echo $product['product_id']; ?>"></a>
            <?php } else { ?>
            <a class="item_tofavorites" type="button" data-toggle="tooltip"
               title="<?php echo $button_wishlist; ?>"
               data-id="<?php echo $product['product_id']; ?>"></a>
            <?php } ?>
            <div class="blockslider_imgwrapper">
              <div class="blockgrid_badges">
                <?php if ($product['special']) { ?>
                <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                <?php } ?>
                <?php if (!empty($product['sticker'])): ?>
                <span class="oneitem_action">Хит продаж</span>
                <?php endif; ?>
              </div>
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                     title="<?php echo $product['name']; ?>" class="img-responsive"/>
              </a>
            </div>
            <div class="product_layout_listheader">
              <p class="part_manufacturer"><?php echo $product['manufacturer']; ?></p>
              <h4 class="part_header">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              </h4>
            </div>
            <div class="product_layout_listblock">
              <p class="item_partnumber">
                <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
              </p>
              <p class="item_partnumber">
                <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
              </p>
<!--              <p class="item_partnumber">-->
<!--                --><?php //if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
<!--              </p>-->
            </div>
            <?php /* if ($product['rating']): //проверка на наличие рейтинга. Уже стилизовано. ?>
            <div class="product-rating">
              <?php for ($i = 1; $i <= 5; $i++) : ?>
              <?php if ($product['rating'] < $i): ?>
              <span class="star_empty"></span>
              <?php else: ?>
              <span class="star_full"></span>
              <?php endif; ?>
              <?php endfor; ?>
            </div>
            <?php endif; */ ?>
            <div class="product_layout_listtocart">
              <?php if ($product['price']) { ?>
              <?php if (!$product['special']) { ?>
              <p class="partprice"><?php echo $product['price']; ?></p>
              <?php } else { ?>
              <p class="partprice partprice_notavailable">
                <span class="oldprice"><?php echo $product['price']; ?></span>
                <span class="current_price"><?php echo $product['special']; ?></span>
              </p>
              <?php } ?>
              <?php /* if ($product['tax']) { ?>
              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } */ ?>
              <?php } ?>
              <?php if ( $product['quantity'] > 0 ) { ?>
              <div class="quantity">
                <input type="number" min="<?=$product['minimum'];?>" max="<?=$product['quantity'];?>"
                       step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
              </div>
              <button class="item_tocart pull-right" type="button"><?php echo $button_cart; ?></button>
              <?php } else { ?>
              <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal" data-target="#order"><?php echo $button_cart_zero; ?></button>
              <?php } ?>
            </div>
          </div>
          <?php /* if need added compare ?>
          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                  onclick="compare.add('<?php echo $product['product_id']; ?>');">
            <i class="fa fa-exchange"></i>
          </button>
          <?php */ ?>
        </div>
        <?php endforeach; ?>
        <?php else: ?>
        <h4 class="notfound_result"><?php echo $text_empty; ?></h4>
        <?php endif; ?>
      </div>
    </div>
    <?php echo $column_right; ?>
  </div>
</section>
<?php if (isset($pagination)): ?>
<div class="pagination_wrapper">
  <div class="text-center">
    <?php echo $pagination; ?>
  </div>
</div>
<?php endif; ?>
<div class="container">
<?php echo $content_bottom; ?>
</div>
<script type="text/javascript"><!--
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#button-search').bind('click', function () {
            url = 'index.php?route=product/search';

            var search = $('#content input[name=\'search\']').prop('value');
            if (search) {
                url += '&search=' + encodeURIComponent(search) + '&sub_category=true' + '&description=true';
            }
            location = url;
        });
        $('input[name=\'search\']').bind('keydown', function (e) {
            if (e.keyCode == 13) {
                $('#button-search').trigger('click');
            }
        });
    });
--></script>
<?php echo $footer; ?>