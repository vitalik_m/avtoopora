<?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="register" onsubmit="return false;">
    <h2 class="sectionheader">Регистрация</h2>
    <div class="form-group required" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
        <label class="col-sm-2 control-label"><?php echo $entry_customer_group; ?></label>
        <div class="col-sm-10">
            <?php foreach ($customer_groups as $customer_group) { ?>
            <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
            <div class="radio">
                <label>
                    <input type="radio" name="customer_group_id"
                           value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked"/>
                    <?php echo $customer_group['name']; ?></label>
            </div>
            <?php } else { ?>
            <div class="radio">
                <label>
                    <input type="radio" name="customer_group_id"
                           value="<?php echo $customer_group['customer_group_id']; ?>"/>
                    <?php echo $customer_group['name']; ?></label>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
    <fieldset class="checkout_required placeholder_gray required">
        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
        <input type="email" name="email" value="<?php echo $email; ?>"
               placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control"/>
        <?php if ($error_email) { ?>
        <div class="text-danger"><?php echo $error_email; ?></div>
        <?php } ?>
    </fieldset>
    <fieldset class="checkout_required placeholder_gray required">
        <label class="control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>"
               placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control"/>
        <?php if ($error_firstname) { ?>
        <div class="text-danger"><?php echo $error_firstname; ?></div>
        <?php } ?>
    </fieldset>

    <fieldset class="checkout_required placeholder_gray required">
        <label class="control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
        <input type="text" name="lastname" value="<?php echo $lastname; ?>"
               placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control"/>
        <?php if ($error_lastname) { ?>
        <div class="text-danger"><?php echo $error_lastname; ?></div>
        <?php } ?>
    </fieldset>

    <fieldset class="checkout_required placeholder_gray">
        <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
        <input type="tel" name="telephone" value="<?php echo $telephone; ?>"
               placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control"/>
        <?php if ($error_telephone) { ?>
        <div class="text-danger"><?php echo $error_telephone; ?></div>
        <?php } ?>
    </fieldset>
    <fieldset class="checkout_required placeholder_gray">
        <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
        <input type="password" name="password" value="<?php echo $password; ?>"
               placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control"/>
        <?php if ($error_password) { ?>
        <div class="text-danger"><?php echo $error_password; ?></div>
        <?php } ?>

        <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
        <input type="password" name="confirm" value="<?php echo $confirm; ?>"
               placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control"/>
        <?php if ($error_confirm) { ?>
        <div class="text-danger"><?php echo $error_confirm; ?></div>
        <?php } ?>
    </fieldset>
    <?php if ($text_agree) { ?>
    <div class="col-md-6">
        <div class="option-group">
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked"/>
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1"/>
            <?php } ?>
            <label for=""><?php echo $text_agree; ?></label>
        </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
        <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn_checkout btn_profileenter"/>
        </div>
    </div>
    <?php } ?>
    <div class="col-md-6">
        <input id="register_submit" class="pull-right btn_checkout btn_profileregister" type="submit"
               value="<?php echo $button_continue; ?>">
    </div>
    <input type="hidden" name="country_id" value="220"/>
    <input type="hidden" name="zone_id" value="3491"/>
    <input type="hidden" name="newsletter" value="1"/>
    <input type="hidden" name="fax" value="<?php echo $fax; ?>" />
    <input type="hidden" name="company" value="<?php echo $company; ?>" />
    <input type="hidden" name="address_1" value="адрес" />
    <input type="hidden" name="address_2" value="<?php echo $address_2; ?>" />
    <input type="hidden" name="city" value="Город" />
    <input type="hidden" name="postcode" value="<?php echo $postcode; ?>" />
</form>
<script type="text/javascript"><!--
    // Sort the custom fields
    document.addEventListener("DOMContentLoaded", function (event) {

        $('input[name=\'customer_group_id\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
                dataType: 'json',
                success: function (json) {
                    $('.custom-field').hide();
                    $('.custom-field').removeClass('required');

                    for (i = 0; i < json.length; i++) {
                        custom_field = json[i];

                        $('#custom-field' + custom_field['custom_field_id']).show();

                        if (custom_field['required']) {
                            $('#custom-field' + custom_field['custom_field_id']).addClass('required');
                        }
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $('input[name=\'customer_group_id\']:checked').trigger('change');

        /*$('.date').datetimepicker({
            pickTime: false
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });
*/
        $('#register_submit').on('click', function () {
            var data = $('#register input[type=\'text\'], #register input[type=\'tel\'], #register input[type=\'email\'], #register input[type=\'time\'], #register input[type=\'password\'], #register input[type=\'hidden\'], #register input[type=\'checkbox\']:checked, #register input[type=\'radio\']:checked, #register textarea, #register select').serialize();
            $.ajax({
                url: '/index.php?route=account/register_singup',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                complete: function () {
                    $('#button-register').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        //$('#register').html().load(json['success']);
                        location = json['redirect'];

                    } else if (json['error']) {

                        error = true;
                        if (json['error']['warning']) {
                            $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    } else {
                        error = false;

                        jQuery('#payment_method :selected').click();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });


    });

    //--></script>