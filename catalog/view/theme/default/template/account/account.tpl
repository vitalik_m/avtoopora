<?php echo $header; ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ( isset($breadcrumb['href']) ) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
      <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $content_top; ?>
      <h2><?php echo $text_my_account; ?></h2>
      <ul class="list-unstyled">
        <li class="checkout_cartinfolink"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
        <li class="checkout_cartinfolink"><a href="<?php echo $edit; ?>"><?php echo $text_password; ?></a></li>
        <li class="checkout_cartinfolink"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <li class="checkout_cartinfolink"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
      </ul>
      <h2><?php echo $text_my_orders; ?></h2>
      <ul class="list-unstyled">
        <li class="checkout_cartinfolink"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <!--<li class="checkout_cartinfolink"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>-->
        <?php if ($reward) { ?>
        <li class="checkout_cartinfolink"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
        <?php } ?>
        <li class="checkout_cartinfolink"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
        <!--<li class="checkout_cartinfolink"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>-->
        <!--<li class="checkout_cartinfolink"><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>-->
      </ul>
      <h2><?php echo $text_my_newsletter; ?></h2>
      <ul class="list-unstyled">
        <li class="checkout_cartinfolink"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
