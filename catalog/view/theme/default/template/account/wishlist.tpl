<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
    </div>
</div>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?php echo $content_top; ?>
            <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
            <?php if ($products) { ?>
            <section class="yolllololololiloooooooooooooooooooo">
                <div class="row">
                    <?php foreach ($products as $product) { ?>
                    <div class="product-layout product-grid col-lg-6 col-md-6 col-sm-6 col-xs-12" data-id="<?php echo $product['product_id']; ?>">
                        <div class="blockslider_bordered">
                            <a class="item_infavorites" type="button" data-toggle="tooltip"
                               title="<?php echo $button_remove; ?>"
                               href="<?php echo $product['remove']; ?>"></a>
                            <div class="blockslider_imgwrapper">
                                <div class="blockgrid_badges">
                                    <?php if ($product['special']) { ?>
                                    <span class="item_tosale">-<?php echo $product['saving']; ?>%</span>
                                    <?php } ?>
                                    <?php if (!empty($product['sticker'])): ?>
                                    <span class="oneitem_action">Хит продаж</span>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <div class="product_layout_listheader">
                                <p class="part_manufacturer"></p>
                                <h4 class="part_header">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                </h4>
                            </div>
                            <div class="product_layout_listblock">
                                <p class="item_partnumber">
                                    <?php // if (!empty($product['description'])): echo $product['description']; endif; ?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['model'])): echo 'Код товара: ' . $product['model']; endif; ?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['sku'])) : echo 'Кат. номер: '. $product['sku']; endif; ?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['upc'])): echo 'Зав. номер: '. $product['upc']; endif;?>
                                </p>
                                <p class="item_partnumber">
                                    <?php /* if (!empty($product['ean'])): echo $product['ean']; endif;?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['jan'])): echo $product['jan']; endif; ?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['isbn'])): echo $product['isbn']; endif; ?>
                                </p>
                                <p class="item_partnumber">
                                    <?php if (!empty($product['mpn'])): echo $product['mpn']; endif; */ ?>
                                </p>
                            </div>
                            <?php /* if ($product['rating']): //проверка на наличие рейтинга. Уже стилизовано. ?>
                            <div class="product-rating">
                                <?php for ($i = 1; $i <= 5; $i++) : ?>
                                <?php if ($product['rating'] < $i): ?>
                                <span class="star_empty"></span>
                                <?php else: ?>
                                <span class="star_full"></span>
                                <?php endif; ?>
                                <?php endfor; ?>
                            </div>
                            <?php endif; */ ?>
                            <div class="product_layout_listtocart">
                                <?php if ($product['price']) { ?>
                                <?php if (!$product['special']) { ?>
                                <p class="partprice"><?php echo $product['price']; ?></p>
                                <?php } else { ?>
                                <p class="partprice partprice_notavailable">
                                    <span class="oldprice"><?php echo $product['price']; ?></span>
                                    <span class="current_price"><?php echo $product['special']; ?></span>
                                </p>
                                <?php } ?>
                                <?php /* if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } */ ?>
                                <?php } ?>
                                <?php if ( $product['quantity'] > 0 ) { ?>
                                <div class="quantity">
                                    <input type="number" min="<?=$product['minimum'];?>"
                                           max="<?=$product['quantity'] != 0 ? $product['quantity'] : $product['minimum']+1; ?>"
                                           step="<?=$product['minimum'];?>" value="<?=$product['minimum'];?>">
                                </div>
                                <button class="item_tocart pull-right"
                                        type="button"><?php echo $button_cart; ?></button>
                                <?php } else { ?>
                                <button class="item_tocart_notavailable pull-right" type="button" data-toggle="modal"
                                        data-target="#order"><?php echo $button_cart_zero; ?></button>
                                <?php } ?>
                            </div>
                        </div>
                        <?php /* if need added compare ?>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                                onclick="compare.add('<?php echo $product['product_id']; ?>');">
                            <i class="fa fa-exchange"></i>
                        </button>
                        <?php */ ?>
                    </div>
                    <?php } ?>
                </div>
            </section>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
        <?php /* ?>
        <div class="buttons clearfix">
            <div class="pull-right"><a href="<?php echo $continue; ?>"
                                       class="btn_checkout btn_profileenter"><?php echo $button_continue; ?></a>
            </div>
        </div>
        <?php */ ?>
    </div>
</div>

<?php echo $content_bottom; ?>
<?php echo $footer; ?>