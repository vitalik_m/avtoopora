<?php echo $header; ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ( isset($breadcrumb['href']) ) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
      <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
      <?php } ?>
    </ul>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
  </div>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_email; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn_checkout btn_profileenter" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>