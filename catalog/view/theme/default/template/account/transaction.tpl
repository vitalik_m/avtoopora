<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <p><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <td class="text-left"><?php echo $column_date_added; ?></td>
                        <td class="text-left"><?php echo $column_description; ?></td>
                        <td class="text-right"><?php echo $column_amount; ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($transactions) { ?>
                    <?php foreach ($transactions  as $transaction) { ?>
                    <tr>
                        <td class="text-left"><?php echo $transaction['date_added']; ?></td>
                        <td class="text-left"><?php echo $transaction['description']; ?></td>
                        <td class="text-right"><?php echo $transaction['amount']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                        <td class="text-center" colspan="5"><?php echo $text_empty; ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            <?php /* ?>
            <div class="buttons clearfix">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn_checkout btn_profileenter"><?php echo $button_continue; ?></a></div>
            </div>
            <?php */ ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>