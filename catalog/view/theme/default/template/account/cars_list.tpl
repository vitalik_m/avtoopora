<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<section id="deliverydetails_section">
    <div class="container">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">×</button></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">×</button></div>
        <?php } ?>
        <div class="row">
            <?php echo $column_left; ?>
            <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php echo $content_top; ?>
                <h2 class="sectionheader"><?php echo $text_cars_book; ?></h2>
                <p><?php echo $cars_all; ?></p>
                <?php if ($carses) { ?>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                    <label class="control-label" for="user_delivery"><?php echo $text_cars_book; ?></label>
                    <select id="user_delivery" class="selectpicker custom-select">
                        <?php foreach ($carses as $key => $result) { ?>
                        <option value="" data-update="<?=$result['update']; ?>"><?php echo $result['cars']; ?></option>
                        <?php } ?>
                    </select>
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo $add; ?>" class="pull-left btn_checkout btn_profileregister">
                                <?php echo $button_new_cars; ?>
                            </a>
                            <button id="update" type="button" class="pull-right btn_checkout btn_profileregister">
                                Редактировать адрес доставки
                            </button>
                        </div>
                        <style>
                            .btn_checkout.btn_profileregister{
                                margin-bottom: 24px;
                            }
                        </style>
                    </div>
                    <script type="text/javascript">
                        document.addEventListener("DOMContentLoaded", function (event) {
                            $("#update").click(function (e) {
                                e.preventDefault();
                                location = $('#user_delivery :selected').attr('data-update');
                            });
                        });
                    </script>
                </fieldset>
                <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons clearfix">
                    <div class="pull-left">
                        <a href="<?php echo $back; ?>" class="btn_checkout btn_profileenter btn_back btn-mapback">
                            <?php echo $button_back; ?>
                        </a>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo $add; ?>" class="btn_checkout btn_profileenter">
                            <?php echo $button_new_cars; ?>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
        </div>

    </div>
</section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>