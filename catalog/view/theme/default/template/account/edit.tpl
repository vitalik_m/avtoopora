<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        <?php } ?>
    </div>
</div>
<section id="userprofile_section">
    <div class="container">
        <div class="row">
            <?php echo $column_left; ?>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php echo $content_top; ?>
                <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
                <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                <?php } ?>
                <?php if ($error_warning) { ?>
                <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                <?php } ?>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <fieldset
                            class="checkout_required placeholder_black <?php if ($error_firstname){ ?>err_alert<?php } ?>">
                        <label class="control-label" for="input-firstname"><?php echo $entry_firstname; ?> </label>
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>"
                               placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control"/>
                        <?php if ($error_firstname) { ?>
                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset
                            class="checkout_required placeholder_black <?php if ($error_lastname){ ?>err_alert<?php } ?>">
                        <label class="control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>"
                               placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control"/>
                        <?php if ($error_lastname) { ?>
                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset
                            class="checkout_required placeholder_black <?php if ($error_email){ ?>err_alert<?php } ?>">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="email" name="email" value="<?php echo $email; ?>"
                               placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control"/>
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset
                            class="checkout_unrequired placeholder_black <?php if ($error_telephone){ ?>err_alert<?php } ?>">
                        <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                        <input type="tel" name="telephone" value="<?php echo $telephone; ?>"
                               placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control"/>
                        <?php if ($error_telephone) { ?>
                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                        <?php } ?>
                    </fieldset>
                    <input type="hidden" name="fax" value="<?php echo $fax; ?>empty"
                           placeholder="<?php echo $entry_fax; ?>"
                           id="input-fax" class="form-control"/>
                    <fieldset class="checkout_unrequired placeholder_black">
                        <?php foreach ($custom_fields as $custom_field) { ?>
                        <?php if ($custom_field['location'] == 'account') { ?>
                        <?php if ($custom_field['type'] == 'select') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                        id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                        class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                            selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'radio') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <div class="radio">
                                        <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                        <label>
                                            <input type="radio"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                   checked="checked"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } else { ?>
                                        <label>
                                            <input type="radio"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'checkbox') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <div class="checkbox">
                                        <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $account_custom_field[$custom_field['custom_field_id']])) { ?>
                                        <label>
                                            <input type="checkbox"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                   checked="checked"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } else { ?>
                                        <label>
                                            <input type="checkbox"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'text') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                       value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                       placeholder="<?php echo $custom_field['name']; ?>"
                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                       class="form-control"/>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'textarea') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                    <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5"
                              placeholder="<?php echo $custom_field['name']; ?>"
                              id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                              class="form-control"><?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'file') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <button type="button"
                                        id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                        data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i
                                            class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden"
                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                       value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : ''); ?>"/>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'date') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>"
                                           data-date-format="YYYY-MM-DD"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                    <i class="fa fa-calendar"></i>
                </button>
                </span>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'time') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group time">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'datetime') { ?>
                        <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field"
                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group datetime">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>"
                                           data-date-format="YYYY-MM-DD HH:mm"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                    </fieldset>
                    <input id="savechanges_submit" class="pull-right btn_checkout btn_profileregister" type="submit"
                           value="Сохранить изменения">
                </form>
                <div id="new-pass">
                    <h3 class="section_subheader">Смена пароля</h3>

                    <fieldset class="checkout_required placeholder_gray required pass-view">
                        <label class="control-label" for="input-password"><?php echo $entry_old_password; ?></label>
                        <input type="password" name="old_password" value="<?php echo $old_password; ?>"
                               placeholder="<?php echo $entry_old_password; ?>" id="input-password"
                               class="form-control"/>
                        <?php if ($error_old_password) { ?>
                        <div class="text-danger"><?php echo $error_old_password; ?></div>
                        <?php } ?>
                        <div class="passwordview_oldpass"><input id="password_old" type="checkbox"> <label
                                    class="pass_switcher" for="password_old"><span></span></label></div>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_gray required pass-view">
                        <label class="control-label" for="input-confirm"><?php echo $entry_password; ?></label>
                        <input type="password" name="password" value="<?php echo $password; ?>"
                               placeholder="<?php echo $entry_password; ?>" id="input-confirm" class="form-control"/>
                        <?php if ($error_password) { ?>
                        <div class="text-danger"><?php echo $error_password; ?></div>
                        <?php } ?>
                        <div class="passwordview_oldpass"><input id="password_old" type="checkbox"> <label
                                    class="pass_switcher" for="password_old"><span></span></label></div>
                        <script type="text/javascript">
                            var $passes = document.querySelectorAll('.pass-view');
                            (function () {
                                for (var i = 0; i < $passes.length; i++) {
                                    $passes[i] && $passes[i].addEventListener('click', _passesHandler);
                                }
                            })();

                            function _passesHandler(e) {
                                e.preventDefault();
                                var target = e.target,
                                    focus;
                                if (target.classList.contains('pass_switcher')) {
                                    focus = this.querySelector('[type="password"], [type="text"]');
                                    if ('text' === focus.getAttribute('type')) {
                                        inputToPassword(focus);
                                    }
                                    else if ('password' === focus.getAttribute('type')) {
                                        inputToText(focus);
                                    }
                                }
                                return false;
                            }

                            function inputToText(input) {
                                if (!input) {
                                    return false;
                                }
                                var type = input.getAttribute('type'),
                                    checkbox = input.parentNode.querySelector('[type="checkbox"]');
                                if ('text' !== type) {
                                    input.setAttribute('type', 'text');
                                    checkbox && checkbox.setAttribute('checked', '');
                                }
                            }

                            function inputToPassword(input) {
                                if (!input) {
                                    return false;
                                }
                                var type = input.getAttribute('type'),
                                    checkbox = input.parentNode.querySelector('[type="checkbox"]');
                                if ('password' !== type) {
                                    input.setAttribute('type', 'password');
                                    checkbox && checkbox.removeAttribute('checked');
                                }
                            }
                        </script>
                    </fieldset>
                    <input type="submit" value="Сохранить новый пароль"
                           class="pull-right btn_checkout btn_profileenter" id="change_passwd"/>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $content_bottom; ?>
<script type="text/javascript"><!--
    // Sort the custom fields
    document.addEventListener("DOMContentLoaded", function (event) {
        $('.form-group[data-sort]').detach().each(function () {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
                $('.form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('.form-group').length) {
                $('.form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('.form-group').length) {
                $('.form-group:first').before(this);
            }
        });

        $('button[id^=\'button-custom-field\']').on('click', function () {
            var node = this;

            $('#form-upload').remove();

            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
                clearInterval(timer);
            }

            timer = setInterval(function () {
                if ($('#form-upload input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $(node).button('loading');
                        },
                        complete: function () {
                            $(node).button('reset');
                        },
                        success: function (json) {
                            $(node).parent().find('.text-danger').remove();

                            if (json['error']) {
                                $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">×</button></div>');
                            }

                            if (json['success']) {
                                alert(json['success']);

                                $(node).parent().find('input').attr('value', json['code']);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });

        $('#change_passwd').on('click', function (e) {
            e.preventDefault();
            //$('#new-pass').find('.err_alert').removeClass('err_alert');
            var data = $('input[name="password"],input[name="old_password"], input[name="email"]').serialize();
            $.ajax({
                url: '/index.php?route=account/password',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                complete: function () {
                    $('#button-register').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('#new-pass').find('.err_alert').removeClass('err_alert');
                    if (json['error']) {
                        error = true;
                        for (i in json['error']) {
                            if ( json['error'][i] !== '') {
                                $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                                $('[name="' + i + '"]').parent().addClass('err_alert');
                            }
                        }

                    } else {
                        error = false;
                    }
                    if (json['success']) {
                        $('#userprofile_section').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" id="redirect" data-dismiss="alert">&times;</button></div>');
                    }
                    if (json['redirect']) {
                        $('#redirect').on('click', function () {
                            location = json['redirect'];
                        });
                        //$('#register').html().load(json['success']);
                        //console.log(json['redirect']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });
    });
    //--></script>
<?php echo $footer; ?>