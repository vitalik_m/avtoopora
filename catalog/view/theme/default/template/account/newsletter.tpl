<?php echo $header; ?>
<div class="header-breadcrumbs">
  <div class="container">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ( isset($breadcrumb['href']) ) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
      <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <?php echo $content_top; ?>
      <h1 class="subscribepage_header"><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset class="subscribepage_fieldset">
          <div class="form-group">
            <label class="control-label"><?php echo $entry_newsletter; ?></label>
            <div class="option-group">
              <?php if ($newsletter) { ?>
              <input type="radio" name="newsletter" value="1" checked="checked" />
              <label class="radio-inline"><?php echo $text_yes; ?> </label>
              <input type="radio" name="newsletter" value="0" />
              <label class="radio-inline"><?php echo $text_no; ?></label>
              <?php } else { ?>
              <input type="radio" name="newsletter" value="1" />
              <label class="radio-inline"><?php echo $text_yes; ?> </label>
              <input type="radio" name="newsletter" value="0" checked="checked" />
              <label class="radio-inline"><?php echo $text_no; ?></label>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <!--<div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>-->
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_save; ?>" class="btn_checkout btn_profileenter" />
          </div>
        </div>
      </form>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>