<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <?php echo $content_top; ?>
        <section id="usercarryout_section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h2 class="sectionheader"><?php echo $heading_title; ?></h2>
                        <?php if ($orders): ?>
                        <?php foreach ($orders as $order): ?>
                        <div class="checkout_cartinfo">
                            <?php if ($order['status'] == 'Сделка завершена'): ?>
                            <span class="order_status order_done"><?php echo $order['status']; ?></span>
                            <?php elseif (($order['status'] == 'Отменено') || ($order['status'] == 'Отмена и аннулирование') || ( $order['status'] ==  'Анулированный' )): ?>
                            <span class="order_status order_cancel"><?php echo $order['status']; ?></span>
                            <?php else: ?>
                            <span class="order_status order_processing"><?php echo $order['status']; ?></span>
                            <?php endif; ?>
                            <span class="order_date_added"><?php echo $order['order_id']; echo $order['date_added']; ?></span>
                            <?php if ( $order['products'] < 3 ): ?>
                            <?php foreach ($order['all_products']['new'] as $product): ?>
                            <p class="checkout_cartinfo_item">
                                <span>
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    (<?php echo $product['quantity']; ?> шт.)
                                </span>
                                <span><?php echo $product['total']; ?></span>
                            </p>
                            <?php endforeach; ?>
                            <p class="checkout_cartinfolink divider">
                                <!--<span class="pull-left">
                                    <a href="<?php echo $order['href']; ?>" data-toggle="tooltip"
                                       title="<?php echo $button_view; ?>">
                                        <?php echo $text_view; ?>
                                    </a>
                                </span>-->
                                <span class="pull-right">
                                    <?php echo $column_total; ?> <?php echo $order['total']; ?>
                                </span>
                            </p>
                            <?php else: ?>
                            <?php for ($i = 0; $i < 2; $i++): ?>
                            <p class="checkout_cartinfo_item">
                                <span><?php echo $order['all_products']['new'][$i]['name']; ?>
                                    (<?php echo$order['all_products'][$i]['quantity']; ?> шт.)</span>
                                <span><?php echo $order['all_products']['new'][$i]['total']; ?></span>
                            </p>
                            <?php endfor; ?>
                            <div class="checkout_cartinfo_item_wrapper hidden">
                                <?php for ($i = 2; $i < count($order['all_products']['new']); $i++): ?>
                                <p class="checkout_cartinfo_item">
                                    <span><?php echo $order['all_products']['new'][$i]['name']; ?>
                                        (<?php echo$order['all_products'][$i]['quantity']; ?> шт.)</span>
                                    <span><?php echo $order['all_products']['new'][$i]['total']; ?></span>
                                </p>
                                <?php endfor; ?>
                            </div>
                            <p class="checkout_cartinfolink divider">
                                 <span class="pull-left">
                                     <a class="order_seemore" href="#" data-action="close"><?php echo $order['text_view_all']; ?></a>
                                 </span>
                                <!--<span class="pull-left">
                                    <a href="<?php echo $order['href']; ?>" data-toggle="tooltip"
                                       title="<?php echo $button_view; ?>">
                                        <?php echo $order['text_view_all']; ?>
                                    </a>
                                </span>-->
                                <span class="pull-right">
                                    <?php echo $column_total; ?> <?php echo $order['total']; ?>
                                </span>
                            </p>
                            <?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                        <?php else: ?>
                        <p><?php echo $text_empty; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
                </div>
                <?php /* if ($orders) { ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-right"><?php echo $column_order_id; ?></td>
                            <td class="text-left"><?php echo $column_status; ?></td>
                            <td class="text-left"><?php echo $column_date_added; ?></td>
                            <td class="text-right"><?php echo $column_product; ?></td>
                            <td class="text-left"><?php echo $column_customer; ?></td>
                            <td class="text-right"><?php echo $column_total; ?></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $order) { ?>
                        <tr>
                            <td class="text-right">#<?php echo $order['order_id']; ?></td>
                            <td class="text-left"><?php echo $order['status']; ?></td>
                            <td class="text-left"><?php echo $order['date_added']; ?></td>
                            <td class="text-right"><?php echo $order['products']; ?></td>
                            <td class="text-left"><?php echo $order['name']; ?></td>
                            <td class="text-right"><?php echo $order['total']; ?></td>
                            <td class="text-right">
                                <a href="<?php echo $order['href']; ?>" data-toggle="tooltip"
                                   title="<?php echo $button_view; ?>" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="text-right"><?php echo $pagination; ?></div>
                <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <?php }  ?>
                <div class="buttons clearfix">
                    <div class="pull-right"><a href="<?php echo $continue; ?>"
                                               class="btn_checkout btn_profileenter"><?php echo $button_continue; ?></a>
                    </div>
                </div>
                <?php */ echo $content_bottom; ?>
            </div>
        </section>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        (function (d) {
            var $elements = document.querySelectorAll('.checkout_cartinfo');
            for (var i = 0; i < $elements.length; i++) {
                $elements[i] && $elements[i].addEventListener('click', _checkoutCartInfo_click_handler);
            }

            function _checkoutCartInfo_click_handler ( e ) {
                var target = e.target,
                    grandparent = target.parentNode.parentNode.parentNode.querySelector('.checkout_cartinfo_item_wrapper');

                if (target.classList.contains('order_seemore')) {
                    e.preventDefault();
                    if (target.getAttribute('data-action') === 'close') {
                        target.innerHTML = 'Скрыть товары';
                        target.setAttribute('data-action','open');
                        grandparent.classList.remove('hidden')
                    }
                    else {
                        target.setAttribute('data-action','close');
                        target.innerHTML = 'Показать все товары';
                        grandparent.classList.add('hidden')
                    }
                }
            }
        })(document);
        /*(function (d) {
            var $elements = document.querySelectorAll('.checkout_cartinfo');
            for (var i = 0; i < $elements.length; i++) {
                var text_seemore = $('.order_seemore').html(),
                    text_close = 'Скрыть товары';
                $elements[i] && $elements[i].addEventListener('click', function (e) {
                    var target = e.target;
                    if (target.classList.contains('order_seemore')) {
                        e.preventDefault();
                        if (target.getAttribute('data-action') === 'close') {
                            target.innerHTML = text_close;
                            target.setAttribute('data-action','open');
                            target.parentNode.parentNode.parentNode.querySelector('.checkout_cartinfo_item_wrapper').classList.remove('hidden')
                        }
                        else {
                            target.setAttribute('data-action','close');
                            target.innerHTML = text_seemore;
                            target.parentNode.parentNode.parentNode.querySelector('.checkout_cartinfo_item_wrapper').classList.add('hidden')
                        }
                    }
                });
            }
        })(document);*/
    });
</script>
<?php echo $footer; ?>