<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<section id="deliverydetails_section">
    <div class="container">
        <div class="row">
            <?php echo $column_left; ?>
            <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php echo $content_top; ?>
                <h2 class="sectionheader"><?php echo $text_edit_cars; ?></h2>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="form-horizontal">
                    <fieldset class="checkout_required placeholder_black">
                        <label class="control-label" for="input-brand"><?php echo $entry_brand; ?></label>
                        <input type="text" name="brand" value="<?php echo $brand; ?>"
                               placeholder="<?php echo $entry_brand; ?>" id="input-brand" class="form-control"/>
                        <?php if ($error_brand) { ?>
                        <div class="text-danger"><?php echo $error_brand; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_black">
                        <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                        <input type="text" name="model" value="<?php echo $model; ?>"
                               placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control"/>
                        <?php if ($error_model) { ?>
                        <div class="text-danger"><?php echo $error_model; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_black">
                        <label class="control-label" for="input-year"><?php echo $entry_year; ?></label>
                        <input type="text" name="year" value="<?php echo $year; ?>"
                               placeholder="<?php echo $entry_year; ?>" id="input-year" class="form-control"/>
                        <?php if ($error_year) { ?>
                        <div class="text-danger"><?php echo $error_year; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_black">
                        <label class="control-label" for="input-engine"><?php echo $entry_engine; ?></label>
                        <input type="text" name="engine" value="<?php echo $engine; ?>"
                               placeholder="<?php echo $entry_engine; ?>" id="input-engine" class="form-control"/>
                        <?php if ($error_engine) { ?>
                        <div class="text-danger"><?php echo $error_engine; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_black">
                        <label class="control-label" for="input-vin_code"><?php echo $entry_vin_code; ?></label>
                        <input type="text" name="vin_code" value="<?php echo $vin_code; ?>"
                               placeholder="<?php echo $entry_vin_code; ?>" id="input-vin_code" class="form-control"/>
                        <?php if ($error_vin_code) { ?>
                        <div class="text-danger"><?php echo $error_vin_code; ?></div>
                        <?php } ?>
                    </fieldset>
                    <div class="buttons customadress-btn">
                        <a href="<?php echo $back; ?>" class="btn_checkout btn_profileenter btn_back btn-mapback">
                            <?php echo $button_back; ?>
                        </a>
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn_checkout btn_profileenter btn-mapsubmit"/>
                    </div>

                </form>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
        </div>
    </div>
</section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
