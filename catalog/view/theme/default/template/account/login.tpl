<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
        <?php } ?>
    </div>
</div>
<section id="register_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <form action="" method="post" enctype="multipart/form-data" id="register"
                      onsubmit="return false;">
                    <h2 class="sectionheader">Регистрация</h2>
                    <?php /* ?>
                    <div class="form-group required"
                         style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
                        <label class="col-sm-2 control-label"><?php echo $entry_customer_group; ?></label>
                        <div class="col-sm-10">
                            <?php foreach ($customer_groups as $customer_group) { ?>
                            <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="customer_group_id"
                                           value="<?php echo $customer_group['customer_group_id']; ?>"
                                           checked="checked"/>
                                    <?php echo $customer_group['name']; ?></label>
                            </div>
                            <?php } else { ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="customer_group_id"
                                           value="<?php echo $customer_group['customer_group_id']; ?>"/>
                                    <?php echo $customer_group['name']; ?></label>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <?php */ ?>
                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="email" name="email" value="<?php echo $email; ?>"
                               placeholder="<?php echo $entry_email_reg; ?>" id="input-email" class="form-control"/>
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>"
                               placeholder="<?php echo $entry_firstname_reg; ?>" id="input-firstname" class="form-control"/>
                        <?php if ($error_firstname) { ?>
                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                    </fieldset>

                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>"
                               placeholder="<?php echo $entry_lastname_reg; ?>" id="input-lastname" class="form-control"/>
                        <?php if ($error_lastname) { ?>
                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                    </fieldset>

                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                        <input type="tel" name="telephone" value="<?php echo $telephone; ?>"
                               placeholder="<?php echo $entry_telephone_reg; ?>" id="input-telephone" class="form-control"/>
                        <?php if ($error_telephone) { ?>
                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                        <?php } ?>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_gray pass-view">
                            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                            <input type="password" name="password" value="<?php echo $password; ?>"
                                   placeholder="<?php echo $entry_password_reg; ?>" id="input-password"
                                   class="form-control"/>
                            <?php if ($error_password) { ?>
                            <div class="text-danger"><?php echo $error_password; ?></div>
                            <?php } ?>
                            <div class="passwordview_register">
                                <input type="checkbox" id="password_register_ch">
                                <label class="pass_switcher" for="password_register_ch"><span></span></label>
                            </div>
                        <?php /* ?>
                        <div class="pass-view">
                            <label class="control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                            <input type="password" name="confirm" value="<?php echo $confirm; ?>"
                                   placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control"/>
                            <?php if ($error_confirm) { ?>
                            <div class="text-danger"><?php echo $error_confirm; ?></div>
                            <?php } ?>
                            <div class="passwordview_register">
                                <input type="checkbox" id="password_register_confirm">
                                <label class="pass_switcher" for="password_register_confirm"><span></span></label>
                            </div>
                        </div>
                        <?php */ ?>

                    </fieldset>
                    <?php if ($text_agree) { ?>
                    <div class="col-md-6">
                        <div class="option-group">
                            <?php if ($agree) { ?>
                            <input type="checkbox" name="agree" value="1" checked="checked"/>
                            <?php } else { ?>
                            <input type="checkbox" name="agree" value="1"/>
                            <?php } ?>
                            <label for=""><?php echo $text_agree; ?></label>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="buttons">
                        <div class="pull-right">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn_checkout btn_profileenter"/>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-6">
                        <input id="register_submit" class="pull-right btn_checkout btn_profileregister" type="submit"
                               value="<?php echo $text_registered; ?>">
                    </div>
                    <input type="hidden" name="country_id" value="220"/>
                    <input type="hidden" name="zone_id" value="3491"/>
                    <input type="hidden" name="newsletter" value="1"/>
                    <input type="hidden" name="fax" value="<?php echo $fax; ?>"/>
                    <input type="hidden" name="company" value="<?php echo $company; ?>"/>
                    <input type="hidden" name="address_1" value="адрес"/>
                    <input type="hidden" name="address_2" value="<?php echo $address_2; ?>"/>
                    <input type="hidden" name="city" value="Город"/>
                    <input type="hidden" name="postcode" value="<?php echo $postcode; ?>"/>
                </form>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2 class="sectionheader"><?php echo $text_returning_customer; ?></h2>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      id="authorization">
                    <?php if ($error_warning) { ?>
                    <fieldset class="checkout_required placeholder_gray err_alert">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="email" name="email_log" value="<?php echo $email_log; ?>"
                               placeholder="<?php echo $entry_email_log; ?>" id="input-email" class="form-control"/>
                        <div class="text-danger"><?php echo $error_warning; ?></div>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_gray err_alert">
                        <div class="pass-view">
                            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                            <input type="password" name="password_log" value="<?php echo $password_log; ?>"
                                   placeholder="<?php echo $entry_password_log; ?>" id="input-password"
                                   class="form-control"/>
                            <div class="passwordview_enter"><input type="checkbox" id="password_enter_ch">
                                <label class="pass_switcher" for="password_enter_ch">
                                    <span></span>
                                </label>
                            </div>
                            <div class="text-danger"><?php echo $error_warning; ?></div>
                        </div>
                        <?php if ($forgotten_pass) { ?>
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                        <?php } ?>
                    </fieldset>
                    <?php } else { ?>
                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="email" name="email_log" value="<?php echo $email_log; ?>"
                               placeholder="<?php echo $entry_email_log; ?>" id="input-email" class="form-control"/>
                    </fieldset>
                    <fieldset class="checkout_required placeholder_gray">
                        <div class="pass-view">
                            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                            <input type="password" name="password_log" value="<?php echo $password_log; ?>"
                                   placeholder="<?php echo $entry_password_log; ?>" id="input-password"
                                   class="form-control"/>
                            <div class="passwordview_enter"><input type="checkbox" id="password_enter_ch">
                                <label class="pass_switcher" for="password_enter_ch">
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <?php if ($forgotten_pass) { ?>
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                        <?php } ?>
                    </fieldset>
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="option-group">
                            <input type="checkbox" name="autologin" value="1" checked="checked"/>
                            <label for="remember_me"><?php echo $text_remember_me; ?></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <input type="submit" value="<?php echo $button_login; ?>"
                               class="pull-right btn_checkout btn_profileenter" id="logIn"/>
                    </div>
                    <?php if ($redirect) { ?>
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</section>
<?php echo $content_bottom; ?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        $('input[name=\'customer_group_id\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
                dataType: 'json',
                success: function (json) {
                    $('.custom-field').hide();
                    $('.custom-field').removeClass('required');

                    for (i = 0; i < json.length; i++) {
                        custom_field = json[i];

                        $('#custom-field' + custom_field['custom_field_id']).show();

                        if (custom_field['required']) {
                            $('#custom-field' + custom_field['custom_field_id']).addClass('required');
                        }
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        (function (d) {
            var $elements = document.querySelectorAll('.checkout_required');

            for (var i = 0; i < $elements.length; i++) {
                $elements[i] && $elements[i].addEventListener('click', function ( k ) {
                    var target = k.target;
                    if ( target.classList.contains('form-control') ) {
                        $(this).find('.text-danger').remove();
                    }
                    if (this.classList.contains('err_alert')) {
                        this.classList.remove('err_alert');
                    }
                });
            }

        })(document);

        $('input[name=\'customer_group_id\']:checked').trigger('change');

        /*$('.date').datetimepicker({
         pickTime: false
         });

         $('.time').datetimepicker({
         pickDate: false
         });

         $('.datetime').datetimepicker({
         pickDate: true,
         pickTime: true
         });
         */

        $('#register_submit').on('click', function () {
            var data,
                $passes = document.querySelectorAll('.pass-view [type="text"]');

            if ( 0 < $passes.length ) {
                for (var i = 0; i < $passes.length; i++) {
                    inputToPassword( $passes[i] );
                }
            }
            data = $('#register input[type=\'text\'], #register input[type=\'tel\'], #register input[type=\'email\'], #register input[type=\'time\'], #register input[type=\'password\'], #register input[type=\'hidden\'], #register input[type=\'checkbox\']:checked, #register input[type=\'radio\']:checked, #register textarea, #register select').serialize();
            $.ajax({
                url: '/index.php?route=account/register_singup',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                complete: function () {
                    $('#button-register').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        //$('#register').html().load(json['success']);
                        location = json['redirect'];

                    } else if (json['error']) {

                        error = true;
                        if (json['error']['warning']) {
                            $('#register').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                            $('[name="' + i + '"]').parent().addClass('err_alert');
                        }
                    } else {
                        error = false;

                        jQuery('#payment_method :selected').click();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });


        var $passes = document.querySelectorAll('.pass-view');
        (function(){
            for (var i = 0; i < $passes.length; i++) {
                $passes[i] && $passes[i].addEventListener('click', _passesHandler);
            }
        })();

        function _passesHandler ( e ) {
            e.preventDefault();
            var target = e.target,
                focus;
            if ( target.classList.contains('pass_switcher') ) {
                focus = this.querySelector('[type="password"], [type="text"]');
                if ( 'text' === focus.getAttribute('type') ) {
                    inputToPassword ( focus );
                }
                else if ( 'password' === focus.getAttribute('type') ) {
                    inputToText ( focus );
                }
            }
            return false;
        }

        function inputToText ( input ) {
            if ( !input ) {
                return false;
            }
            var type = input.getAttribute('type'),
                checkbox = input.parentNode.querySelector('[type="checkbox"]');
            if ( 'text' !== type ) {
                input.setAttribute('type', 'text');
                checkbox && checkbox.setAttribute('checked', '');
            }
        }

        function inputToPassword ( input ) {
            if ( !input ) {
                return false;
            }
            var type = input.getAttribute('type'),
                checkbox = input.parentNode.querySelector('[type="checkbox"]');
            if ( 'password' !== type ) {
                input.setAttribute('type', 'password');
                checkbox && checkbox.removeAttribute('checked');
            }
        }
    });
</script>
<?php echo $footer; ?>