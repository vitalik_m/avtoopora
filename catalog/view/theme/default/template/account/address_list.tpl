<?php echo $header; ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<section id="deliverydetails_section">
    <div class="container">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">×</button></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">×</button></div>
        <?php } ?>
        <div class="row">
            <?php echo $column_left; ?>
            <div id="content" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php echo $content_top; ?>
                <h2 class="sectionheader"><?php echo $text_address_book; ?></h2>
                <p><?php echo $address_default; ?></p>
                <?php if ($addresses) { ?>
                <fieldset class="select_checkoutsort select_arr_checkout checkout_required">
                    <label class="control-label" for="user_delivery"><?php echo $text_address_book; ?></label>
                    <select id="user_delivery" class="selectpicker custom-select">
                        <?php foreach ($addresses as $key => $result) { ?>
                        <option value="" data-update="<?=$result['update']; ?>"><?php echo $result['address']; ?></option>
                        <?php } ?>
                    </select>
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo $add; ?>" class="pull-left btn_checkout btn_profileregister">
                                <?php echo $button_new_address; ?>
                            </a>
                            <button id="update" type="button" class="pull-right btn_checkout btn_profileregister">
                                Редактировать адрес доставки
                            </button>
                        </div>
                        <style>
                            .btn_checkout.btn_profileregister{
                                margin-bottom: 24px;
                            }
                        </style>
                    </div>
                    <script type="text/javascript">
                        document.addEventListener("DOMContentLoaded", function (event) {
                            $("#update").click(function (e) {
                                e.preventDefault();
                                location = $('#user_delivery :selected').attr('data-update');
                            });
                        });
                    </script>
                </fieldset>
                <fieldset>
                    <div class="pull-right">

                    </div>
                </fieldset>

                <p>Свой заказ вы сможете забрать из магазина в будние дни с 08:00 – 19:00, в субботу и воскресенье с
                    09:00 – 16:00</p>

                <p>Магазин находится по адресу: г. Запорожье, ул. Седова, 7</p>

                <div id="map"></div>
                <?php if ($geocode) { ?>
                <?php $vowels = array('lat: ','lng: ');?>
                <a href="https://www.google.com/maps/dir//<?=urlencode(str_replace($vowels, '', $geocode));?>/"
                   target="_blank" class="roadmap">
                    <?php echo $button_map; ?>
                </a>
                <a href="https://maps.google.com/maps?q=<?php echo urlencode(str_replace($vowels, '', $geocode)); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                   target="_blank" class="roadmap">
                    <?php echo $button_maps; ?>
                </a>
                <script type="text/javascript">
                    function initMap() {
                        var sedova = {<?=(!empty($geocode))? $geocode : 'lat: 47.850815, lng: 35.141889' ?>};

                        var styles = [
                            {'featureType':'administrative', 'elementType':'labels.text.fill', 'stylers':[{'color':'#444444'}]},
                            {'featureType':'landscape',      'elementType':'all',              'stylers':[{'color':'#dfdfdf'}]},
                            {'featureType':'poi',            'elementType':'all',              'stylers':[{'visibility':'off'}]},
                            {'featureType':'road',           'elementType':'all',              'stylers':[{'saturation':-100},{'lightness':45}]},
                            {'featureType':'road.highway',   'elementType':'all',              'stylers':[{'visibility':'simplified'}]},
                            {'featureType':'road.arterial',  'elementType':'labels.icon',      'stylers':[{'visibility':'off'}]},
                            {'featureType':'transit',        'elementType':'all',              'stylers':[{'visibility':'off'}]},
                            {'featureType':'water',          'elementType':'all',              'stylers':[{'color':'#29b6f6'},{'visibility':'on'}]}
                        ];

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: sedova,
                            styles: styles
                        });
                        var marker = new google.maps.Marker({
                            position: sedova,
                            map: map,
                            icon: 'catalog/view/theme/default/image/pick.svg'
                        });
                    }
                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVmLGbPtGTdQd154lfyzm045YP3XkDy8c&callback=initMap"
                        type="text/javascript"></script>
                <?php } ?>
                <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <?php } /* ?>
                <div class="buttons clearfix">
                    <div class="pull-left">
                        <a href="<?php echo $back; ?>" class="btn btn-default">
                            <?php echo $button_back; ?>
                        </a>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo $add; ?>" class="btn_checkout btn_profileenter">
                            <?php echo $button_new_address; ?>
                        </a>
                    </div>
                </div>
                <?php */ ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><?php echo $column_right; ?></div>
        </div>

    </div>
</section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>