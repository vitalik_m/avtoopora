<h1 class="sectionheader"><?php echo $heading_title; ?></h1>
<div class="paydescr_text">
    <?php echo $text_message; ?>
</div>

<div class="buttons">
    <div class="pull-left">
        <a href="<?php echo $continue; ?>" class="btn_checkout btn_profileenter">
            <?php echo $button_continue; ?>
        </a>
    </div>
</div>