<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <?php if ($robots) { ?>
    <meta name="robots" content="<?php echo $robots; ?>"/>
    <?php } ?>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } /* ?>

    <!--<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/new/fonts.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/new/main.min.css" rel="stylesheet">
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
    <?php */ foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>

    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript" defer></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <!-- <script src="catalog/view/javascript/jquery/jquery-3.3.1.min.js"></script> -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127966202-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127966202-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '201117750756977');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=201117750756977&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
    <div class="container">
        <?php echo $currency; ?>
        <?php echo $language; ?>
        <?php // echo $menu; ?>
        <nav class="navbar customnav_marg">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#top_nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="top_nav">
                    <ul class="nav navbar-nav top_m">
                        <?php foreach ($informations as $information): ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</nav>
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div id="logo">
                    <?php if ($logo) { ?>
                    <a href="<?php echo $home; ?>">
                        <img src="<?php echo $logo; ?>" alt="logotype" class="img-responsive header_logo"></a>
                    <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 col-sm-9 col-xs-8 fullwidth_resp">
                <div class="open_phones">
                    <p class="shop_open"><?php echo $config_open; ?></p>
                    <ul class="shop_phones">
                        <li><a href="tel:<?=$telephone?>"><?php echo $telephone; ?></a></li>
                        <li><a href="tel:<?=$telephone2?>"><?php echo $telephone2; ?></a></li>
                        <li><a href="tel:<?=$fax?>"><?php echo $fax; ?></a></li>
                    </ul>
                </div>
                <a href="#openmodal_callback" class="callme" data-toggle="modal" data-target="#myModal">перезвоните
                    мне</a>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-3 col-xs-4 fullwidth_resp">
                <?php if ($logged) { ?>
                <a class="enter_link pull-right" href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
                <?php } else { ?>
                <a class="enter_link pull-right" href="<?php echo $login; ?>"><?php echo $text_login; ?></a>
                <!--<a class="reg_link" href="<?php echo $register; ?>"> / <?php echo $text_register; ?></a>-->
                <?php } ?>
            </div>
        </div>
    </div>
    <section class="filtermenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <?php if ($categories) { ?>
                    <div class="dropdown">
                        <a href="#" id="dLabel" class="cat_dropdown" role="button" data-toggle="dropdown"
                           data-target="#">Каталог товаров</a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                            <?php foreach ($categories as $category) : ?>
                            <?php if ($category['children']) : ?>
                            <li class="dropdown-submenu">
                                <a href="<?php echo $category['href']; ?>"
                                   tabindex="-1"><?php echo $category['name']; ?></a>
                                <ul class="dropdown-menu">
                                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                    <?php foreach ($children as $child) : ?>
                                    <?php if (isset($child['children_1'])) : ?>
                                    <li class="dropdown-submenu">
                                        <a href="<?php echo $child['href']; ?>"
                                           tabindex="-1"><?php echo $child['name']; ?></a>
                                        <ul class="dropdown-menu">
                                            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                            <?php foreach ($child['children_1'] as $children_1): ?>
                                            <li>
                                                <a tabindex="-1" href="<?php echo $children_1['href']; ?>">
                                                    <?php echo $children_1['name']; ?>
                                                </a>
                                            </li>
                                            <?php endforeach; ?>
                                            <li>
                                                <a href="<?php echo $child['href']; ?>" class="see-all">
                                                    <?php echo $text_all; ?> <?php echo $child['name']; ?>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php else: ?>
                                    <li>
                                        <a tabindex="-1" href="<?php echo $child['href']; ?>">
                                            <?php echo $child['name']; ?>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                    <li><a href="<?php echo $category['href']; ?>"
                                           class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php else : ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if (isset($shared)): ?>
                            <?php $rand_keys = array_rand($shared, 1); $product = $shared[$rand_keys]; ?>
                            <li class="ads" data-id="<?=$product['product_id'];?>">
                                <div class="col-md-12 blockads_menu">
                                    <div class="menuadsblock_imgwrapper">
                                        <div class="badgeswrapper">
                                            <span class="item_tosale">-<?php echo $product['saving']?>%</span>
                                        </div>
                                        <a href="<?php echo $product['href']?>"><img src="<?php echo $product['thumb']?>" alt="<?php echo $product['name']; ?>"></a>
                                    </div>
                                    <p class="part_manufacturer"><?php echo $product['manufacturer']; ?></p>
                                    <h4 class="part_header">
                                        <a href="<?php echo $product['href']; ?>">
                                            <?php echo mb_substr($product['name'],0,24,'UTF-8').' ...' ; ?>
                                        </a>
                                    </h4>
                                    <p class="partprice partprice_available"><?php echo $product['special']; ?></p>
                                    <?php if ( $product['quantity'] > 0 ) : ?>
                                    <button class="item_tocart" type="button">В корзину</button>
                                    <?php else: ?>
                                    <button class="item_tocart_notavailable" type="button" data-toggle="modal" data-target="#order">Уведомить о поступлении</button>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12 grid_headsearchclass"> <?php echo $search; ?> </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 grid_headbuttons">
                    <div id="cart" class="btn-group btn-block">
                        <?php if ($true == true): ?>
                        <a class="favorite_header in_wish" href="/index.php?route=account/wishlist">
                              <span>
                                <?php if ($text_wishlist != 0) { echo $text_wishlist; } ?>
                              </span>
                            <?php echo $text_wish; ?>
                        </a>
                        <?php else: ?>
                        <a class="favorite_header" href="/index.php?route=account/wishlist">
                            <?php echo $text_wish; ?>
                        </a>
                        <?php endif; ?>
                        <?php echo $cart; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>


