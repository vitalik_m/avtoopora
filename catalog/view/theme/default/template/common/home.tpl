<?php echo $header; ?>
<section class="bannersection">
    <div class="container">
        <div class="row topsect_marg">
            <?php echo $column_left; ?>
            <div id="content" class="col-lg-9 col-md-9">
                <div class="block-wrapper-greenslider">
                    <?php echo $content_top; ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php /* ?>
<!--<div class="row">
    <?php $class = 'col-sm-12'; ?>
    <?php if ( $column_right ) { $class = 'col-sm-9'; } ?>
    <div id="content_bottom" class="<?php echo $class; ?>">
        <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
</div> -->
<?php */ ?>
<?php echo $content_bottom; ?>
<?php echo $column_right; ?>
<?php echo $footer; ?>