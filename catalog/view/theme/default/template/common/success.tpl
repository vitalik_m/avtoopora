<?= $data['header'];?>
<section >
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <h1 class="sectionheader"><?php echo $heading_title; ?></h1>
                <div class="paydescr_text">
                    <?php echo $text_message; ?>
                </div>

                <div class="buttons success-content">
                    <div class="pull-left">
                        <a href="<?php echo $continue; ?>" class="btn_checkout btn_profileenter">
                            <?php echo $button_continue; ?>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?= $data['footer'];?>