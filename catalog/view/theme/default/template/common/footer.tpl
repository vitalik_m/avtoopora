<div class="modal modalDialog" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close_modal" data-dismiss="modal" aria-label="Close"></button>
            <h2 class="modal_header">Заказать обратный звонок</h2>
            <form action="" id="cbackform">
                <label for="callback_name">Имя</label>
                <input id="callback_name" name="name" type="text" placeholder="Укажите ваше имя" required>
                <label for="callback_phone">Телефон</label>
                <input id="callback_phone" name="phone" type="tel" placeholder="Укажите номер вашего телефона" pattern="[0-9]{3,20}" required>
                <div class="modal-footer">
                    <input id="callback_submit" type="submit" value="Отправить" class="call_back_request pull-right">
                </div>
            </form>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <?php if ($logo) { ?>
                <div class="footer-logo">
                    <a href="<?php echo $home; ?>">
                        <?php /* <img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" class="img-responsive header_logo"> */ ?>
                        <img src="/catalog/view/theme/default/stylesheet/img/logo-footer.png" alt="<?php echo $name; ?>" class="img-responsive header_logo">
                    </a>
                </div>
                <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>

                <div class="payment-methods">
                    <p class="service_top_pay">Мы принимаем к оплате</p>
                    <img src="image/catalog/demo_avtoopora/liqpay-logo.png" alt="liqpay" class="wtf_mzf img-responsive">
                    <img src="image/catalog/demo_avtoopora/footer-logogroup.png" alt="visa-mcard" class="wtf_mzf img-responsive footer-group">
                </div>

                <div class="clearfix"></div>
                <?php /* ?>
                <img src="image/catalog/demo_avtoopora/visa-logo.png" alt="visa" class="wtf_mzf img-responsive">
                <img src="image/catalog/demo_avtoopora/mc-logo.png" alt="visa" class="wtf_mzf img-responsive">
                <?php */ ?>

                <div class="footer-socials">
                    <a href="https://www.facebook.com/OPORAvAVTOSFERE/" class="facebook">
                    </a>

                    <a href="http://clc.to/9A0Avw" class="google-plus">
                    </a>

                    <a href="https://www.instagram.com/autoopora/?hl=ru" class="instagram">
                    </a>

                    <a href="https://www.youtube.com/watch?v=woNa0q9LsSQ&list=PL4cW3CYakXsFz5eruNKd7miCz-senk7a5" class="youtube">
                    </a>
                </div>

            </div>
            <?php if ($informations): ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2><?php echo $name; ?></h2>
                <ul>
                    <?php foreach ($informations as $information): ?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php if ($categories) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2 class="footer-list-title">Каталог</h2>
                <ul class="footer-list">
                    <?php foreach ($categories as $category) : ?>
                    <li>
                        <a href="<?php echo $category['href']; ?>">
                            <?php echo $category['name']; ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo $contact; ?>"><?php echo $help_online; ?></a></li>
                    <li>
                        <a href="<?php echo $contact; ?>">
                            <?php echo $questions; ?>
                        </a>
                    </li>
                </ul>
            </div>
            <?php } ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h2><?php echo $text_extra; ?></h2>
                <ul>
                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone2; ?></a></li>
                    <li><a href="tel:<?php echo $fax; ?>"><?php echo $fax; ?></a></li>
                </ul>
                <p class="work_graphic"><?php echo $config_open; ?></p>
                <p class="footer_adress"><?php echo $address; ?></p>
                <?php if ($geocode): ?>
                <?php $vowels = array('lat: ','lng: ');?>
                <a href="https://maps.google.com/maps?q=<?php echo urlencode(str_replace($vowels, '', $geocode)); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                   target="_blank" class="see_map">
                    <?php echo $button_maps; ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
        <?php if (isset($isLogged)) : ?>
        <script>
            window.isLogged = <?php echo $isLogged ?>;
        </script>
        <?php else: ?>
        <script>
            window.isLogged = 0;
        </script>
        <?php endif; ?>
        <div class="scripts">
            <?php foreach ($scripts as $script) { ?>
            <script src="<?php echo $script; ?>" type="text/javascript" defer></script>
            <?php } ?>
        </div>
    </div>
<!--    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>-->
<!--    <script src="catalog/view/javascript/common.js"></script>-->

    <script src="catalog/view/javascript/jquery/jquery-3.3.1.min.js"></script>
</footer>
</body>
</html>