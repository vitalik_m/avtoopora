<?php echo $header; ?>
<div class="header-breadcrumbs">
	<div class="container">
		<ul>
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php if ( isset($breadcrumb['href']) ) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } else { ?>
			<li><?php echo $breadcrumb['text']; ?></li>
			<?php } ?>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="container">
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			<h1 class="sectionheader"><?php echo $heading_title; ?></h1>
			<div class="text-paragraph"><?php echo $description; ?></div>
		</div>
		<?php echo $column_right; ?>
	</div>
	<?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>