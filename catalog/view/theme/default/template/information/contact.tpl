<?php echo $header; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-sm-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<div class="header-breadcrumbs">
    <div class="container">
        <ul>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ( isset($breadcrumb['href']) ) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } else { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contactpage">
                <?php if (!isset($success)) { ?>
                <h2 class="sectionheader"><?=$heading_title?></h2>
                <p><?=$text_contact_us?></p>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="form-horizontal">
                    <?php if ($error_name) { ?>
                    <fieldset class="checkout_required placeholder_gray err_alert">
                        <?php } else { ?>
                        <fieldset class="checkout_required placeholder_gray">
                            <?php } ?>
                    <fieldset class="checkout_required placeholder_gray">
                        <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"
                               class="form-control" placeholder="<?php echo $entry_name_please; ?>"/>
                        <?php if ($error_name) { ?>
                        <div class="text-danger"><?php echo $error_name; ?></div>
                        <?php } ?>
                    </fieldset>
                    <?php if ($error_telephone) { ?>
                    <fieldset class="checkout_required placeholder_gray err_alert">
                    <?php } else { ?>
                    <fieldset class="checkout_required placeholder_gray">
                        <?php } ?>
                        <label class="control-label" for="input-telephone"><?php echo $entry_telephone?></label>
                        <input name="telephone" value="" placeholder="<?php echo $entry_telephone_please ?>" id="input-telephone"
                               class="form-control" type="tel">
                        <?php if ($error_telephone) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </fieldset>
                        <?php if ($error_email) { ?>
                        <fieldset class="checkout_required placeholder_gray err_alert">
                            <?php } else { ?>
                            <fieldset class="checkout_required placeholder_gray">
                                <?php } ?>
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <input type="email" name="email" value="<?php echo $email; ?>" id="input-email"
                               class="form-control" placeholder="<?php echo $entry_email_please; ?>"/>
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </fieldset>
                            <?php if ($error_enquiry) { ?>
                            <fieldset class="checkout_required placeholder_gray err_alert">
                                <?php } else { ?>
                                <fieldset class="checkout_required placeholder_gray">
                                    <?php } ?>
                        <label class="control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                        <textarea name="enquiry" id="input-enquiry" placeholder="<?php echo $entry_enquiry_please; ?>"
                                  class="form-control"><?php echo $enquiry; ?></textarea>
                        <?php if ($error_enquiry) { ?>
                        <div class="text-danger"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </fieldset></fieldset>
                    <?php echo $captcha; ?>
                    <input class="btn_checkout btn_profileregister pull-right" type="submit"
                           value="<?php echo $text_send; ?>"/>
                </form>
                <?php } else { ?>
                    <p id="success"></p>
                <script>
                    document.addEventListener("DOMContentLoaded", function (event) {
                    $('#success').html('<h1 class="sectionheader">Форма обратной связи</h1><div class="paydescr_text"><p>Ваше сообщение было успешно отправлено администрации магазина!</p></div><div class="buttons"><div class="pull-right"><a href="/index.php?route=common/home" class="btn_checkout btn_profileenter">Продолжить</a></div></div>');
                        });
                </script>
                <?php } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contactpage">
                <?php if ($open) { ?>
                <p><?php echo $open; ?></p>
                <?php } ?>
                <?php if ($comment) { ?>
                <p><?php echo $comment; ?></p>
                <?php } ?>
                <p>Магазин находится по адресу: <?php echo $address; ?></p>
                <div id="map"></div>
                <?php if ($geocode) { ?>
                <?php $vowels = array('lat: ','lng: ');?>
                <a href="https://www.google.com/maps/dir//<?=urlencode(str_replace($vowels, '', $geocode));?>/"
                   target="_blank" class="roadmap">
                    <?php echo $button_map; ?>
                </a>
                <?php /* ?>
                <a href="https://maps.google.com/maps?q=<?php echo urlencode(str_replace($vowels, '', $geocode)); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                   target="_blank" class="roadmap">
                    <?php echo $button_maps; ?>
                </a>
                <?php */ //посмотреть карту ?>
                <script type="text/javascript">
                    function initMap() {
                        var sedova = {<?=(!empty($geocode))? $geocode : 'lat: 47.850815, lng: 35.141889' ?>};

                        var styles = [
                            {'featureType':'administrative', 'elementType':'labels.text.fill', 'stylers':[{'color':'#444444'}]},
                            {'featureType':'landscape',      'elementType':'all',              'stylers':[{'color':'#dfdfdf'}]},
                            {'featureType':'poi',            'elementType':'all',              'stylers':[{'visibility':'off'}]},
                            {'featureType':'road',           'elementType':'all',              'stylers':[{'saturation':-100},{'lightness':45}]},
                            {'featureType':'road.highway',   'elementType':'all',              'stylers':[{'visibility':'simplified'}]},
                            {'featureType':'road.arterial',  'elementType':'labels.icon',      'stylers':[{'visibility':'off'}]},
                            {'featureType':'transit',        'elementType':'all',              'stylers':[{'visibility':'off'}]},
                            {'featureType':'water',          'elementType':'all',              'stylers':[{'color':'#29b6f6'},{'visibility':'on'}]}
                        ];

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: sedova,
                            styles: styles
                        });
                        var marker = new google.maps.Marker({
                            position: sedova,
                            map: map,
                            icon: 'catalog/view/theme/default/image/pick.svg'
                        });
                    }
                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVmLGbPtGTdQd154lfyzm045YP3XkDy8c&callback=initMap"
                        type="text/javascript"></script>
                <?php } ?>
            </div>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<div class="container">
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
