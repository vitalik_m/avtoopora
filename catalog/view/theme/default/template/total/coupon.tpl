<input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control" />
<a id="cart_promocode_confirm" class="adft" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_coupon; ?></a>
<script type="text/javascript"><!--
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#cart_promocode_confirm').on('click', function () {
            $.ajax({
                url: 'index.php?route=total/coupon/coupon',
                type: 'post',
                data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
                dataType: 'json',
                beforeSend: function () {
                    $('#cart_promocode_confirm').button('loading');
                },
                complete: function () {
                    $('#cart_promocode_confirm').button('reset');
                },
                success: function (json) {
                    $('.alert').remove();

                    if (json['error']) {
                        $('.header-breadcrumbs').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        //$('html, body').animate({scrollTop: 0}, 'slow');
                    }
                    if (json['success']){
                        $( "#cart_salecount" ).toggleClass( " hidden" );
                        $( "#cart_entercode" ).toggleClass( " hidden" );
                    }
                    if (json['redirect']) {
                        location = json['redirect'];
                    }

                }
            });
        });
    });
    //--></script>