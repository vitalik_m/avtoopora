<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Личный кабинет';

// Text
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Вход';
$_['text_logout']      = 'Выход';
$_['text_forgotten']   = 'Забыли пароль?';
$_['text_account']     = 'Моя информация';
$_['text_edit']        = 'Контактная информация';
$_['text_password']    = 'Пароль';
$_['text_address']     = 'Адрес доставки';
$_['text_cars']        = 'Мои автомобили';
$_['text_wishlist']    = 'Закладки';
$_['text_order']       = 'История заказов';
$_['text_download']    = 'Файлы для скачивания';
$_['text_reward']      = 'Бонусные баллы';
$_['text_return']      = 'Возвраты';
$_['text_transaction'] = 'Платёжная информация';
$_['text_newsletter']  = 'Подписка на рассылку';
$_['text_recurring']   = 'Регулярные платежи';