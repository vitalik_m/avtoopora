<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']  = 'Форма обратной связи';

// Text
$_['text_location']  = 'Мы находимся по адресу:';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Написать нам';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Режим работы';
$_['text_comment']   = 'Дополнительная информация';
$_['text_success']   = '<p>Ваше сообщение было успешно отправлено администрации магазина!</p>';
$_['text_send']      = 'Отправить';

// Entry
$_['entry_name']     = 'Имя';
$_['entry_email']    = 'Электронная почта';
$_['entry_enquiry']  = 'Вопрос или комментарий';
$_['entry_telephone']= 'Телефон';
$_['entry_name_please']     = 'Укажите ваше имя';
$_['entry_email_please']    = 'Укажите вашу почту';
$_['entry_enquiry_please']  = 'Напишите ваш вопрос либо укажите, какую запчасть вам необходимо подобрать';
$_['entry_telephone_please']= 'Укажите номер вашего телефона';

// Email
$_['email_subject']  = 'Сообщение от %s';

// Errors
$_['error_name']     = 'Имя должно содержать от 3 до 32 символов!';
$_['error_telephone']= 'Введите, пожалуйста свой номер!';
$_['error_email']    = 'E-Mail адрес введен неверно!';
$_['error_enquiry']  = 'Длина текста должна содержать от 10 до 3000 символов!';


$_['text_contact_us']= 'Вы можете заказать запчасти к вашему автомобилю через эту форму. Напишите код запчати или просто опишите проблему и укажите марку автомобиля и мы поможем вам подобрать нужные запчасти.';