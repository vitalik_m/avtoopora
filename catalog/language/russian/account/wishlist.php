<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Закладки';

// Text
$_['text_account']  = 'Личный кабинет';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = '%s товаров';
$_['text_login']    = 'Товар <a href="%s">%s</a> добавлен в <a href="%s">избранные товары</a>! Необходимо войти в <a href="%s">Личный Кабинет</a> или <a href="%s">создать учетную запись</a>, чтобы просмотреть <a href="%s">избранные товары</a>!';
$_['text_rem_log']  = 'Необходимо войти в <a href="%s">Личный Кабинет</a> или <a href="%s">создать учетную запись</a>, чтобы удалить товар <a href="%s">%s</a> из своих <a href="%s">закладок</a>!';
$_['text_success']  = 'Товар <a href="%s">%s</a> добавлен в <a href="%s">избранные товары</a>!';
$_['text_rem_succ'] = 'Товар <a href="%s">%s</a> удален из <a href="%s">избранных товаров</a>!';
$_['text_remove']   = 'Закладки успешно обновлены!';
$_['text_empty']    = 'Ваши закладки пусты.';
$_['button_cart_zero']= 'Уведомить о поступлении';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Название товара';
$_['column_model']  = 'Код товара';
$_['column_stock']  = 'Наличие';
$_['column_price']  = 'Цена за единицу товара';
$_['column_action'] = 'Действие';