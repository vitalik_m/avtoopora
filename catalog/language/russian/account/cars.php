<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']        = 'Мои Автомобили';

// Text
$_['text_account']         = 'Личный кабинет';
$_['text_cars_book']       = 'Мой автомобиль';
$_['text_edit_cars']       = 'Редактировать данные автомобиля';
$_['text_added_cars']      = 'Добавить автомобиль';
$_['text_add']             = 'Ваш автомобиль был успешно добавлен';
$_['text_edit']            = 'Ваш автомобиль был успешно изменен';
$_['text_delete']          = 'Ваш автомобиль был успешно удален';
$_['text_empty']           = 'В вашей учетной записи нет автомобилей';
$_['cars_all']             = 'На этой странице отображаются все ваши автомобили добавленные на сайт. Здесь вы можете добавить или изменить информацию о вашем авто.';
$_['button_new_cars']      = 'Добавить автомобиль';

// Entry
$_['entry_brand']          = 'Бренд';
$_['entry_model']          = 'Модель';
$_['entry_year']           = 'Год';
$_['entry_engine']         = 'Объем двигателя';
$_['entry_vin_code']       = 'VIN-code';

// Error
$_['error_delete']         = 'У Вас должно быть не менее 1 автомобиля!';
$_['error_brand']          = 'Бренд должно содержать от 1 до 32 символов!';
$_['error_model']          = 'Модель должна содержать от 1 до 32 символов!';
$_['error_year']           = 'Не правильно введен год!';
$_['error_engine']         = 'Адрес должен содержать от 3 до 128 символов!';
$_['error_vin_code']       = 'Не правильно введен VIN код!';
$_['error_custom_field']   = '%s необходим!';