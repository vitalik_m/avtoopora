<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelToolImage extends Model {
	public function resize($filename, $width, $height) {

		if (is_file(PATH_IMAGE_PRODUCTS . $filename)) {
            $curImgPath = PATH_IMAGE_PRODUCTS;
            $dirImg = DIR_IMAGE_PRODUCTS;
		}else if(is_file(DIR_IMAGE . $filename)){
            $curImgPath = DIR_IMAGE;
            $dirImg = 'image/';
        }else{
		    return;
        }

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file($curImgPath . $new_image) || (filectime($curImgPath . $old_image) > filectime($curImgPath . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir($curImgPath . $path)) {
					@mkdir($curImgPath . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize($curImgPath . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image($curImgPath . $old_image);
				$image->resize($width, $height);
				$image->save($curImgPath . $new_image);
			} else {
				copy($curImgPath . $old_image, $curImgPath . $new_image);
			}
		}

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . $dirImg . $new_image;
		} else {
			return $this->config->get('config_url') . $dirImg . $new_image;
		}
	}
}
