<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelAccountCars extends Model {
	public function addCars($data) {
		$this->event->trigger('pre.customer.add.cars', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "cars SET customer_id = '" . (int)$this->customer->getId() . "', brand = '" . $this->db->escape($data['brand']) . "', model = '" . $this->db->escape($data['model']) . "', year = '" . (int)$data['year'] . "', engine = '" . (int)$data['engine'] . "', vin_code = '" . $this->db->escape($data['vin_code']) . "'");

		$cars_id = $this->db->getLastId();

		$this->event->trigger('post.customer.add.cars', $cars_id);

		return $cars_id;
	}

	public function editCars($cars_id, $data) {
		$this->event->trigger('pre.customer.edit.cars', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "cars SET brand = '" . $this->db->escape($data['brand']) . "', model = '" . $this->db->escape($data['model']) . "', year = '" . (int)$data['year'] . "', engine = '" . (int)$data['engine'] . "', vin_code = '" . $this->db->escape($data['vin_code']) . "' WHERE cars_id  = '" . (int)$cars_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.edit.cars', $cars_id);
	}

	public function deleteCars($cars_id) {
		$this->event->trigger('pre.customer.delete.cars', $cars_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "cars WHERE cars_id = '" . (int)$cars_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.delete.cars', $cars_id);
	}

	public function getCars($cars_id) {
		$cars_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cars WHERE cars_id = '" . (int)$cars_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($cars_query->num_rows) {

			$cars_data = array(
				'cars_id'     => $cars_query->row['cars_id'],
				'brand'       => $cars_query->row['brand'],
				'model'       => $cars_query->row['model'],
				'year'        => $cars_query->row['year'],
				'engine'      => $cars_query->row['engine'],
				'vin_code'    => $cars_query->row['vin_code']
			);
			return $cars_data;
		} else {
			return false;
		}
	}

	public function getCarses() {
		$cars_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cars WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		foreach ($query->rows as $result) {

			$cars_data [$result['cars_id']] = array(
				'cars_id'    => $result['cars_id'],
				'brand'      => $result['brand'],
				'model'      => $result['model'],
				'year'       => $result['year'],
				'engine'     => $result['engine'],
				'vin_code'   => $result['vin_code']

			);
		}

		return $cars_data;
	}

	public function getTotalCarses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cars WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}